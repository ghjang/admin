package egovframework.happy.cmmn;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageingUtil {
	protected final static Logger log = LoggerFactory.getLogger(FileUtils.class);
	public static void alertAndForward(HttpServletResponse res, String alertMsg, String forwardUrl) throws IOException {
		StringBuffer sb = new StringBuffer();
		sb.append("<script> alert(\"").append(alertMsg).append("\");</script>")
				.append("<meta http-equiv=\"Refresh\" content=\"0; url=").append(forwardUrl).append("\">");

		callPage(res, sb.toString());
	}

	public static void callPage(HttpServletResponse res, String msg) throws IOException {
		res.setContentType("text/html; charset=" + "UTF-8");
		PrintWriter pw = res.getWriter();
		try {
			pw.println(msg);
		} finally {
			if (pw != null)
				pw.close();
		}
	}
}