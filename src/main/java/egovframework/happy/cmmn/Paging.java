package egovframework.happy.cmmn;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for adding page numbers to the list
 * 
 * @author f1_sb
 * 
 */

public class Paging {

	private static final int NUM_OF_PAGES_TO_DISPLAY = 10; // page numbers at the
															// bottom to view
															// prev and next
															// pages

	/**
	 * Adding list of page numbers to be displayed
	 * 
	 * @param currentPage
	 * @param totalItems
	 * @param numberOfPages
	 * @return List
	 */
	public static List<Integer> addPageNumbers(int currentPage, int totalItems,
			int numberOfPages) {

//		System.out.println("currentPage :" + currentPage + " totalItems :"
//				+ totalItems + " numberofPages :" + numberOfPages);

		List<Integer> pageNumbers = new ArrayList<Integer>();

		// Case 1: numberOfPages < numberOfPagesToDisplay
		if (numberOfPages <= NUM_OF_PAGES_TO_DISPLAY)
			for (int i = 1; i <= numberOfPages; i++) {
				pageNumbers.add(new Integer(i));
			}
		else if (currentPage == 1 && numberOfPages > NUM_OF_PAGES_TO_DISPLAY) {
			// Case 2: current Page = 1 : numberOfPages > numberOfPagesToDisplay
			// add first numOfPagesToDisplay
			for (int i = 1; i <= NUM_OF_PAGES_TO_DISPLAY; i++) {
				pageNumbers.add(new Integer(i));
			}
		} else if (currentPage > 1 && numberOfPages > NUM_OF_PAGES_TO_DISPLAY) {
			// Case 3: currentPage > 1 : numberOfPages > numberOfPagesToDisplay
			// find the beginning page
			int beginPage = currentPage;
			if (currentPage > (numberOfPages - NUM_OF_PAGES_TO_DISPLAY)) {
				beginPage = numberOfPages - NUM_OF_PAGES_TO_DISPLAY + 1;
			}

			for (int i = 0; i < NUM_OF_PAGES_TO_DISPLAY; i++) {
				// pageNumbers.add(new Integer(beginPage + i));
				pageNumbers.add(beginPage + i);
			}
		}
		return pageNumbers;
	}

	/**
	 * Return number of pages according to give data
	 * 
	 * @param totalItems
	 *            - total number of items
	 * @param itemsPerPage
	 *            - number of items to be displayed in one page
	 * @return
	 */
	public static int getNumberOfPages(int totalItems, int itemsPerPage) {
		int numberOfPages = totalItems / itemsPerPage;
		if (totalItems % itemsPerPage != 0) {
			numberOfPages += 1;
		}
		return numberOfPages;
	}

	/**
	 * For fixing currentPage if it is less than 1 or greater than total number
	 * of pages
	 * 
	 * @param currentPage
	 * @param numberOfPages
	 * @return
	 */
	public static int fixCurrentPage(int currentPage, int numberOfPages) {
		int fixedCurrentPage = currentPage < 1 ? 1 : currentPage;
		fixedCurrentPage = fixedCurrentPage > numberOfPages ? numberOfPages
				: fixedCurrentPage;
		return fixedCurrentPage;
	}

	/**
	 * Return starting index in the db
	 * 
	 * @param currentPage
	 *            - Current page number
	 * @param itemsPerPage
	 *            - number of items to be displayed in a page
	 * @return
	 */
	public static int getStartingIndex(int currentPage, int itemsPerPage) {
		int from = 0;
		if (currentPage == 1) {
			from = 0;
		}

		if (currentPage > 1) {
			from = (currentPage - 1) * itemsPerPage;
		}
		return from;
	}
}
