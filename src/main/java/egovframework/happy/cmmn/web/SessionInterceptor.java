package egovframework.happy.cmmn.web;

import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import egovframework.happy.cmmn.PageingUtil;
import egovframework.happy.cmmn.SessionUtil;
import egovframework.happy.service.admin.basic.SessionAdminService;
import egovframework.happy.service.client.vo.AdminInfoVO;

public class SessionInterceptor extends HandlerInterceptorAdapter{
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	@Resource(name = "sessionAdminService")
	protected SessionAdminService sessionAdminService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@ [이전페이지:"+request.getHeader("Referer")+"][현재페이지:"+request.getRequestURL()+"]");
		String returnUrl = URLEncoder.encode(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/admin/Login.do", "UTF-8");
		try {
			String[] phones = new String[]{"iphone","ipod","android","blackberry","windows ce","nokia","webos",
					"opera mini","sonyericsson","opera mobi","iemobile"};
			String userAgent = request.getHeader("User-Agent");
			String device = "pc";
			if(log.isDebugEnabled()) log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ [userAgent:"+userAgent+"]");
			/* pc, mobile 확인 */
			for(int i=0;i<phones.length;i++) {
				if(userAgent.toLowerCase().indexOf(phones[i]) != -1){
					device = "mobile";
					break;
				}
			}
			SessionUtil.setAttribute("access_type", device);
			SessionUtil.setAttribute("user_agent", userAgent);
//			SessionUtil.setAttribute("prev_page", request.getHeader("Referer") == null ? "/" : request.getHeader("Referer"));
			SessionUtil.setAttribute("prev_page", request.getHeader("Referer") == null ? "/" : "/");

			if(null == SessionUtil.getAttribute("ss_info")) { //세션 정보 확인
				String alertMsg = "로그인이 필요한 메뉴입니다.";
	    		PageingUtil.alertAndForward(response, alertMsg, "http://newadmin.hunet.co.kr/Login/HappyCollegeLogin?ReturnUrl="+returnUrl);
				return false;
			} else if (null != (AdminInfoVO) SessionUtil.getAttribute("ss_info")) { // 세션 정보가 정상인지 확인 (사용자 정보 확인 및  세션 시간 늘리기)
				JSONObject data = new JSONObject(sessionAdminService.userLoginCheck("https://newadmin.hunet.co.kr/Api/UserLoginCheck", ((AdminInfoVO) SessionUtil.getAttribute("ss_info")).getUser_Id(), ((AdminInfoVO) SessionUtil.getAttribute("ss_info")).getAccessKey()));
				
				//로그인 성공시 세션 체크
				if(0 != (int) data.get("resultCd")) {
					String alertMsg = "로그인 시간이 만료 되었습니다.";
					PageingUtil.alertAndForward(response, alertMsg, "http://newadmin.hunet.co.kr/Login/HappyCollegeLogin?ReturnUrl="+returnUrl);
					return false;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}
}