package egovframework.happy.cmmn.web;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoggerInterceptor extends HandlerInterceptorAdapter {
	protected final Logger log = LoggerFactory.getLogger(getClass());
     
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("\n");
            log.debug("======================================          START         ======================================");
            log.debug("====== [Request URI:" + request.getRequestURI()+"]");
        }
        return super.preHandle(request, response, handler);
    }
     
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (log.isDebugEnabled()) {
        	log.debug("==================         PARAMETER START         ==================");
        	Enumeration<?> en = request.getParameterNames();
            while(en.hasMoreElements()) {
				String key = (String)en.nextElement();
				String val = request.getParameter(key);
				log.debug("====== parameter ["+key+":"+val+"]");
			}
            log.debug("==================          PARAMETER END          ==================");
        	log.debug("====== [view name:"+modelAndView.getViewName()+"]");
        	log.debug("======================================           END          ======================================\n");
        }
    }
}