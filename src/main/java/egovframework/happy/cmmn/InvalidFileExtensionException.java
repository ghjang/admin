package egovframework.happy.cmmn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.exception.handler.ExceptionHandler;

@SuppressWarnings("serial")
public class InvalidFileExtensionException extends Exception {
	private static final Logger log = LoggerFactory.getLogger(EgovSampleOthersExcepHndlr.class);
 
    public InvalidFileExtensionException(String message) {
        super(message);
    }

/*
	*//**
	* @param exception
	* @param packageName
	* @see 개발프레임웍크 실행환경 개발팀
	*//*
	@Override
	public void occur(Exception exception, String packageName) {
		LOGGER.debug(" InvalidFileExtensionException run...............");
	}
	*/
}
