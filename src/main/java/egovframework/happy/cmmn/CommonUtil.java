package egovframework.happy.cmmn;

import java.net.UnknownHostException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class CommonUtil {
	private final static Logger log = LoggerFactory.getLogger(CommonUtil.class);
	
	/**
	 * 클라이언트 IP 가져오기
	 * @return
	 * @throws UnknownHostException
	 */
	public static String getUserIp() throws UnknownHostException {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String ip = req.getHeader("X-FORWARDED-FOR");
		if (ip == null || ip.length() == 0) ip = req.getHeader("Proxy-Client-IP");
		if (ip == null || ip.length() == 0) ip = req.getHeader("WL-Proxy-Client-IP");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		     ip = req.getHeader("HTTP_CLIENT_IP"); 
		 } 
		 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		     ip = req.getHeader("HTTP_X_FORWARDED_FOR"); 
		 } 
		if (ip == null || ip.length() == 0) ip = req.getRemoteAddr();
		
        return ip;
	}
	
	public static String getPublicIp() {
		String ip="127.0.0.1";
		try (java.util.Scanner s = new java.util.Scanner(new java.net.URL("https://api.ipify.org").openStream(), "UTF-8").useDelimiter("\\A")) {
			ip = s.next();
//			System.out.println("My current IP address is " + ip);
		} catch (java.io.IOException e) {
		    e.printStackTrace();
		}
		return ip;
	}
	
	
	public static String null2str(Object obj) {
		if (obj == null) return "";
		else return (String) obj;
	}
	
    /**
     * @param org
     * @return
     */
    public static boolean isEmpty(Object obj) {
		if (obj == null)
			return true;
		return (obj instanceof String) && "".equals(((String) obj).trim());
	}
    
	public static int[] stringArrTointArr(String[] sArr){
		int[] iArr = new int[sArr.length];
		 
		//이렇게 for문으로 풀어서 변환 작업을 해야 한다.
		for(int i = 0; i<sArr.length; i++){
		     iArr[i] = Integer.parseInt(sArr[i] == null || "".equals(sArr[i]) ? "0" : sArr[i]);
		}

		return iArr;
	}
	
	/**
	 * 로그인 필수 체크
	 * @param request
	 * @return
	 */
	public static boolean checkLoginRequired(HttpServletRequest request) {
		String sUri = request.getRequestURI();
		String t[] = sUri.split("/");
		String aCheckUrl[] = {
				/** professor **/
				"makeCourse.do", "makeCourseBasicInfo.do", "makeCourseIntro.do", "makeCoursePlan.do", "lectureSort"
				, "makeCourseDtlContSetting.do", "sampleLectureChoice.do", "manageStatisticsMng.do", "takeLectureStudentList.do"
				/** student **/
				, "lectureRoomStudyHome.do"
				/** professor, student **/
				, "takeLectureReview.do", "updateTakeLectureReview.do"
				, "qnaList.do", "qnaDtl.do", "insertQnaDtl.do", "updateQnaDtl.do", "qnaDtlAnswer.do", "qnaDtlAnswerReplyUpdate.do"
				};
		log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@ [request.getRequestURI():"+sUri+"]");
		log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@ [t.length:"+t.length+"]");
		return t.length > 0 ? Arrays.toString(aCheckUrl).contains(t[t.length-1]) : false;
	}
}
