package egovframework.happy.cmmn;

import java.io.UnsupportedEncodingException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xml.internal.security.utils.Base64;


/**
 * <pre>
 * 암복호화 유틸
 * </pre>
 * @Class AES256Util.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 22.
 * @version 1.0
 */
@SuppressWarnings("restriction")
public class AES256Util {
	private static final String CipherTransformation = "AES/CBC/PKCS5Padding";
	private static final byte[] ivBytes = new byte[] { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31,
			0x32, 0x33, 0x34, 0x35, 0x36 };
	private static final String secretKey = "happycollege98765432101234567890"; 
	
	/**
	 * <pre>
	 * 핸드폰 번호를 암호화
	 * </pre>
	 * @Class AES256Util.java
	 * @Method encrypt
	 * @param phoneNumber
	 * @return 
	 */
	public static String encrypt(String phoneNumber) {
		byte[] keyBytes;
		try {
			keyBytes = secretKey.getBytes("UTF-8");
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
			Cipher cipher = Cipher.getInstance(CipherTransformation);
			cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
			byte [] in = cipher.doFinal(phoneNumber.getBytes("UTF-8"));
			String result = Base64.encode(in, Base64.BASE64DEFAULTLENGTH); 
			return result;
//			return result.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("[ERROR] UnsupportedEncodingException");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("[ERROR] Exception");
			return null;
		}
	}
	
	/**
	 * <pre>
	 * 복호화
	 * </pre>
	 * @Class AES256Util.java
	 * @Method decrypt
	 * @param encryptPhone
	 * @return 
	 */
	public static String decrypt(byte[] encryptPhone) {
		byte[] cipherData;
		try {
			byte[] keyBytes = secretKey.getBytes("UTF-8");
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
			Cipher cipher = Cipher.getInstance(CipherTransformation);
			cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
			byte[] out = Base64.decode(encryptPhone);
			cipherData = cipher.doFinal(out);
			return new String(cipherData, "UTF-8");
		} catch (Exception e) {
			System.out.println("[ERROR]");
			e.printStackTrace();
			return "";
		}
	}
}
