package egovframework.happy.cmmn;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import egovframework.happy.service.client.vo.AdminInfoVO;

public class CustomizeTag {
	public static boolean getAuthFlag(String id) throws Exception {
		AdminInfoVO vo = (AdminInfoVO) SessionUtil.getAttribute("ss_info");
		return id.equals(vo.getUser_Id());
	}
	/**
	 * <pre>
	 * 날짜 포멧 메소드1 아래 포멧 리턴
	 * 
	 * 초 < 60 = 몇 초전
	 * 분 < 60 = 몇 분전
	 * 시간 < 24 = 몇 시간 전
	 * 24시간 이후 = yyyy.MM.dd a hh:mm
	 * </pre>
	 * @Class CustomizeTag.java
	 * @Method dateFmt1
	 * @param t
	 * @return 
	 */
	public static String dateFmt1(Timestamp t) {
		if(t == null) return "";
		else {
			Timestamp now = new Timestamp(System.currentTimeMillis());
			
			long gap = (now.getTime() - t.getTime()) / 1000;
			//long gap = (now.getTime() - t.getTime()) / 1000 / 60;
			//long gap = (now.getTime() - t.getTime()) / 1000 / (60*60);
			
			//초 단위
			if(gap < 60) {
				return Long.toString(gap) + "초 전";
			//분 단위
			} else if(gap < 60 * 60) {
				return Long.toString(gap / 60) + "분 전";
			//시간 단위
			} else if(gap >= 60 * 60 && gap < 60 * 60 * 24) {
				return Long.toString(gap / (60 * 60)) + "시간 전";
			} else {
				SimpleDateFormat sdfCurrent = new SimpleDateFormat ("yyyy.MM.dd a hh:mm"); 
				String today = sdfCurrent.format(t);
				return today;
			}
		}
	}
	/**
	 * <pre>
	 * 날짜 포멧 메소드2 yyyy.MM.dd a hh:mm 포멧 리턴
	 * </pre>
	 * @Class CustomizeTag.java
	 * @Method dateFmt2
	 * @param t
	 * @return 
	 */
	public static String dateFmt2(Timestamp t) {
		if(t == null) return "";
		else {
			SimpleDateFormat sdfCurrent = new SimpleDateFormat ("yyyy.MM.dd a hh:mm"); 
			String today = sdfCurrent.format(t);
			return today;
		}
	}
	
	/**
	 * <pre>
	 * 날짜 포멧 메소드2 yyyy.MM.dd 포멧 리턴
	 * </pre>
	 * @Class CustomizeTag.java
	 * @Method dateFmt3
	 * @param t
	 * @return 
	 */
	public static String dateFmt3(Timestamp t) {
		if(t == null) return "";
		else {
			SimpleDateFormat sdfCurrent = new SimpleDateFormat ("yyyy-MM-dd"); 
			String today = sdfCurrent.format(t);
			return today;
		}
	}
	
	/**
	 * <pre>
	 * 초 to hh:mm:ss 포멧 리턴
	 * </pre>
	 * @Class CustomizeTag.java
	 * @Method secFmt1
	 * @param s
	 * @return 
	 */
	public static String secFmt1(Integer s) {
		if(s == 0) return "00:00:00";
		else {
			String r;
			if(s > 3600) r = (s / 3600) + ":" + (s % 3600 / 60) + ":" + (s % 3600 % 60 > 9 ? "" : "0") + (s % 3600 % 60);
			else r = (s % 3600 / 60) + ":" + (s % 3600 % 60 > 9 ? "" : "0") + (s % 3600 % 60);
			return r;
		}
	}
}
