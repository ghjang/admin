package egovframework.happy.cmmn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.binary.XSSFBUtils;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.helpers.XSSFFormulaUtils;
import org.springframework.beans.factory.annotation.Autowired;

import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.rte.fdl.property.EgovPropertyService;


public class ExcelUtil {
	
	EgovPropertyService propertiesService;
	
	public ExcelUtil(EgovPropertyService propertiesService) {
		this.propertiesService = propertiesService;
	}
	
	public String excelMemberStatus(List<MemberVO> list) {
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("회원현황");
		CreationHelper createHelper = wb.getCreationHelper();
		
		 // Cell 스타일 생성
        CellStyle style = wb.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());	
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        
    	CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/mm/dd hh:mm:ss"));
        // 줄 바꿈
//        cellStyle.setWrapText(true);
		
		Row row = sheet.createRow(0);	//첫번째 줄 

		Cell cell = row.createCell(0);
		cell.setCellValue("회원현황");
		
		row = sheet.createRow(1);	//두번째 줄
		
		List<Cell> cellList = new ArrayList<>();
		
		for(int i=0; i < 14; i++) {
			cellList.add(row.createCell(i));
			sheet.setColumnWidth(i, 14*256);
		}
		sheet.setColumnWidth(2, 20*256);
		sheet.setColumnWidth(5, 25*256);
		sheet.setColumnWidth(13, 20*256);
		
		row.createCell(0).setCellValue(createHelper.createRichTextString("회원번호"));
		row.createCell(1).setCellValue(createHelper.createRichTextString("회원구분"));
		row.createCell(2).setCellValue(createHelper.createRichTextString("가입일"));
		row.createCell(3).setCellValue(createHelper.createRichTextString("ID"));
		row.createCell(4).setCellValue(createHelper.createRichTextString("닉네임"));
		row.createCell(5).setCellValue(createHelper.createRichTextString("이메일"));
		row.createCell(6).setCellValue(createHelper.createRichTextString("휴대폰 번호"));
		row.createCell(7).setCellValue("수강 진행중 과정");
		row.createCell(8).setCellValue("수강 종료 과정");
		row.createCell(9).setCellValue("제작중 과정");
		row.createCell(10).setCellValue("오픈한 과정");
		row.createCell(11).setCellValue("이메일 수신동의");
		row.createCell(12).setCellValue("SMS 수신동의");
		row.createCell(13).setCellValue("가입 경로");

		for (Cell cell2 : cellList) {
			cell2.setCellStyle(style);
		}
		
		Cell tempCell;
		
		//내용
		if(list == null) {
			
		} else {
			for(int i=0; i<list.size(); i++) {
				MemberVO member = list.get(i);
				row = sheet.createRow(2+i);	//새로운 줄

				String joinPathCode = member.getJoin_Path_Code() == null? "" : member.getJoin_Path_Code();
				
				row.createCell(0).setCellValue(member.getUser_Seq());
				row.createCell(1).setCellValue(createHelper.createRichTextString(joinCode(joinPathCode)));
				tempCell = row.createCell(2); 
				tempCell.setCellValue(member.getRegist_Date());
				tempCell.setCellStyle(dateStyle);
				row.createCell(3).setCellValue(createHelper.createRichTextString(member.getUser_Id()));
				row.createCell(4).setCellValue(createHelper.createRichTextString(member.getNickname()));
				row.createCell(5).setCellValue(createHelper.createRichTextString(member.getEmail()));
				row.createCell(6).setCellValue(createHelper.createRichTextString(member.getMobile()));
				row.createCell(7).setCellValue(member.getTakeLecture_Studying_Count());
				row.createCell(8).setCellValue(member.getTakeLecture_Expire_Count());
				row.createCell(9).setCellValue(member.getMaking_Process_Count());
				row.createCell(10).setCellValue(member.getOpen_Process_Count());
				row.createCell(11).setCellValue(createHelper.createRichTextString(member.getEmail_Yn()));
				row.createCell(12).setCellValue(createHelper.createRichTextString(member.getSms_Yn()));
				row.createCell(13).setCellValue(createHelper.createRichTextString(member.getJoin_Path_Url()));
			}
		}
		
		String path="";
		String excelFileName = "";
		FileOutputStream fileOut;
		try {
			excelFileName = "HappyCollege_회원현황.xlsx";
			path = propertiesService.getString("UPLOAD.FILE.PATH");
			System.out.println("execl path: " + path);
			fileOut = new FileOutputStream(path+excelFileName, false);
			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (FileNotFoundException e) {
			System.out.println("ExcelUtil error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("ExcelUtil error: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("ExcelUtil error: " + e.getMessage());
		}
		
		return excelFileName;
	}
	
	public String excelProcessStatus(List<ProcessDTO> list, HashMap<String, List<TagVO>> tagMap, HashMap<String, List<ProcessBlockVO>> blockMap) {
		System.out.println("ExcelUtil] ProcessStatus");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("과정현황");
		
		CreationHelper createHelper = wb.getCreationHelper();
		
		// Cell 스타일 생성
        CellStyle style = wb.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        
    	CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/mm/dd hh:mm:ss"));
		
		Row row = sheet.createRow(0);	//첫번째 줄 
		Cell cell = row.createCell(0);
		cell.setCellValue("과정현황");
		
		row = sheet.createRow(1);	//두번째 줄
		
		List<Cell> cellList = new ArrayList<>();
		
		for(int i=0; i < 20; i++) {
			cellList.add(row.createCell(i));
			sheet.setColumnWidth(i, 13*256);
		}
		sheet.setColumnWidth(0, 20*256);
		sheet.setColumnWidth(3, 40*256);
		sheet.setColumnWidth(6, 8*256);
		sheet.setColumnWidth(19, 25*256);
		
		row.createCell(0).setCellValue(createHelper.createRichTextString("개설일"));
		row.createCell(1).setCellValue(createHelper.createRichTextString("카테고리"));
		row.createCell(2).setCellValue(createHelper.createRichTextString("과정코드"));
		row.createCell(3).setCellValue(createHelper.createRichTextString("과정명"));
		row.createCell(4).setCellValue(createHelper.createRichTextString("ID"));
		row.createCell(5).setCellValue(createHelper.createRichTextString("닉네임"));
		row.createCell(6).setCellValue("가격");
		row.createCell(7).setCellValue("태그명");
		row.createCell(8).setCellValue("평가등급");
		row.createCell(9).setCellValue("수강 후기수");
		row.createCell(10).setCellValue("Q&A");
		row.createCell(11).setCellValue("전체 수강자수");
		row.createCell(12).setCellValue("학습 중 수강자수");
		row.createCell(13).setCellValue("기간종료 수강자수");
		row.createCell(14).setCellValue("페북 공유수");
		row.createCell(15).setCellValue("카톡 공유수");
		row.createCell(16).setCellValue("문자 공유수");
		row.createCell(17).setCellValue("메일 공유수");
		row.createCell(18).setCellValue("공개설정");
		row.createCell(19).setCellValue("차단사유");
		row.createCell(20); row.createCell(21); row.createCell(22);

		for (Cell cell2 : cellList) {
			cell2.setCellStyle(style);
		}
		
		Cell tempCell;
		
		//내용
		if(null == list) {

		} else {
			for(int i=0; i<list.size(); i++) {
				ProcessDTO process = list.get(i);
				String processCode = process.getProcess_Code();
				List<TagVO> tagList = tagMap.get(processCode);
				String tag ="";
				for (TagVO tagVO : tagList) {
					tag = tag + " #" + tagVO.getTag();
				}
				String blockReason ="";
				List<ProcessBlockVO> blockList = blockMap.get(processCode);
				for (ProcessBlockVO processBlockVO : blockList) {
					blockReason += "[" + processBlockVO.getRegist_Date() +"]"+ processBlockVO.getContent() + " ";
				}
				
				row = sheet.createRow(2+i);	//새로운 줄
				tempCell = row.createCell(0); 
				tempCell.setCellValue(process.getRegist_Date());
				tempCell.setCellStyle(dateStyle);
				row.createCell(1).setCellValue(createHelper.createRichTextString(fieldCode(process.getField_Code())));
				row.createCell(2).setCellValue(createHelper.createRichTextString(processCode));
				row.createCell(3).setCellValue(createHelper.createRichTextString(process.getProcess_Nm()));
				row.createCell(4).setCellValue(createHelper.createRichTextString(process.getRegist_Id()));
				row.createCell(5).setCellValue(createHelper.createRichTextString(process.getNickname()));
				row.createCell(6).setCellValue(price(process.getPrice()));
				row.createCell(7).setCellValue(tag);
				row.createCell(8).setCellValue(process.getGrade() + " ("+process.getProcess_Evalationer_Count()+")");
				row.createCell(9).setCellValue(process.getTakeLecture_Review_Count());
				row.createCell(10).setCellValue(process.getProcess_QnA_Count());
				row.createCell(11).setCellValue(process.getTakeLecture_OnGoing_Count());
				row.createCell(12).setCellValue(process.getTakeLecture_Studying_Count());
				row.createCell(13).setCellValue(process.getTakeLecture_Expire_Count());
				row.createCell(14).setCellValue(process.getFacebook_Share_Count());
				row.createCell(15).setCellValue(process.getKakaotalk_Share_Count());
				row.createCell(16).setCellValue(process.getLetters_Share_Count());
				row.createCell(17).setCellValue(process.getEmail_Share_Count());
				row.createCell(18).setCellValue(createHelper.createRichTextString(openYn(process.getOpen_Yn()) + blockYn(process.getBlock_Yn())));
				row.createCell(19).setCellValue(createHelper.createRichTextString(blockReason));
			}
		}
		
		FileOutputStream fileOut;
		String path="";
		String excelFileName="";
		try {
			excelFileName = "HappyCollege_과정현황.xlsx";
			path = propertiesService.getString("UPLOAD.FILE.PATH");
			System.out.println("execl path: " + path);
			fileOut = new FileOutputStream(path+excelFileName, false);
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			System.out.println("error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("error: " + e.getMessage());
		}
		
		return excelFileName;
	}
	
	private String fieldCode(String code) {
		switch (code) {
		case "01":
			return "비즈니스";
		case "02":
			return "인문/예술";
		case "03":
			return "자기계발";
		case "04":
			return "라이프";
		case "05":
			return "언어";
		case "06":
			return "자격증";
		default:
			return "";
		}
	}
	
	private String joinCode(String code) {
		switch (code) {
		case "41":
			return "신규";
		case "42":
			return "유입";
		default:
			return "";
		}
	}
	
	private String openYn(String code) {
		switch (code) {
		case "Y":
		case "y":
			return "공개";
		case "N":
		case "n":
			return "비공개";
		default:
			return "";
		}
	}
	
	private String blockYn(String code) {
		switch (code) {
		case "Y":
		case "y":
			return " (차단)";
		default:
			return "";
		}
	}
	
	private String price(int code) {
		if(code == 0) {
			return "무료";
		} else if(code > 0) {
			return code+"원";
		}
		return "";
	}
}
