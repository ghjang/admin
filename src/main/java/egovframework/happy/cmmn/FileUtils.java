package egovframework.happy.cmmn;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import egovframework.happy.service.client.vo.FileVO;
import egovframework.rte.fdl.property.EgovPropertyService;

@Component("fileUtils")
public class FileUtils {
	@Autowired
	EgovPropertyService propertiesService;

	// Logger
	protected final static Logger log = LoggerFactory.getLogger(FileUtils.class);

	/**
	 * 멀티 파일 업로드
	 * 
	 * @param files : MultipartHttpServletRequest.getFileMap()
	 * @param fileGrpNm : 파일 그룹명(prefix) 
	 * @param storePath : 특정 파일 업로드 properties key name
	 * @return
	 * @throws Exception
	 */
	public List<FileVO> parseMultiFileInfo(Map<String, MultipartFile> files, String fileGrpNm, String storePath) throws Exception {

		String storePathString;
		String linkPathString = "";
		
		int maxFileSize = Integer.parseInt(propertiesService.getString("UPLOAD.FILE.MAXSIZE"));
		String[] aExt = propertiesService.getString("UPLOAD.FILE.EXTENSION").split("\\|");
		 
		if (CommonUtil.isEmpty(storePath)) {
			storePathString = propertiesService.getString("UPLOAD.FILE.PATH");
			linkPathString = propertiesService.getString("UPLOAD.FILE.LINK");
		} else {
			storePathString = propertiesService.getString(storePath);
		}
		
		// 경로에 FileGrpNm 추가.
		if (!CommonUtil.isEmpty(fileGrpNm)) {
			storePathString += fileGrpNm + File.separator;
			linkPathString  += fileGrpNm + File.separator;
		}
		//log.debug("@@@@ storePathString:"+storePathString+"]");
		//log.debug("@@@@ linkPathString:"+linkPathString+"]");
		
		File saveFolder = new File (filePathBlackList(storePathString));
		//filePathBlackList
		
		if (!saveFolder.exists() || saveFolder.isFile()) {
			saveFolder.mkdirs();
		}
		
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;
		String filePath = "";
		List<FileVO> resultList  = new ArrayList<FileVO>();
		FileVO fileVO;
		int fileSeq = 1;

		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();
			file = entry.getValue();
			String origFileName = file.getOriginalFilename();
			long _size = file.getSize();

			//log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ [_size:"+_size+"][file:"+file+"]");
			if(_size > 0) {
				if(_size > maxFileSize) {
					throw new MaxUploadSizeExceededException(maxFileSize);
				}
				
				int index = origFileName.lastIndexOf(".");
				String fileExt = origFileName.substring(index + 1);
				
				if(! isValidFileExtension(fileExt, aExt)) {
					throw new InvalidFileExtensionException(fileExt+" 파일은 업로드 할 수 없습니다.");
				}
				
				// 원 파일명 없음 (첨부되지 않은 input file type)
				if (CommonUtil.isEmpty(origFileName)) continue;
				
				
				//parseMultiFileInfo 시 getTimeStamp 가 중복되어 뒤에 index 를 추가
				String newName = fileGrpNm + "_" + getTimeStamp() + fileSeq;
				
				filePath = storePathString + File.separator + newName + "." + fileExt;
				file.transferTo(new File(filePathBlackList(filePath)));
				
				fileVO = new FileVO();
				fileVO.setExt_File_Nm(fileExt);
				fileVO.setSave_Path(linkPathString);
				fileVO.setFile_Size(_size);
				fileVO.setOriginal_File_Nm(origFileName);
				fileVO.setSave_File_Nm(newName);
				
				fileVO.setAttach_File_Grp_Nm(fileGrpNm);
				
				//writeFile(file, newName, storePathString);
				resultList.add(fileVO);
				
				fileSeq++;
			}
		}
		
		return resultList;
	}
		
	/**
	 * 응용어플리케이션에서 고유값을 사용하기 위해 시스템에서17자리의TIMESTAMP값을 구하는 기능
	 *
	 * @param
	 * @return Timestamp 값
	 * @exception MyException
	 * @see
	 */
	private static String getTimeStamp() {

		String rtnStr = null;

		// 문자열로 변환하기 위한 패턴 설정(년도-월-일 시:분:초:초(자정이후 초))
		String pattern = "yyyyMMddhhmmssSSS";
		try {
			SimpleDateFormat sdfCurrent = new SimpleDateFormat(pattern, Locale.KOREA);
			Timestamp ts = new Timestamp(System.currentTimeMillis());

			rtnStr = sdfCurrent.format(ts.getTime());
		} catch (Exception e) {
			log.error("IGNORE:", e);
		}

		return rtnStr;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String filePathBlackList(String value) {
		String returnValue = value;
		if (returnValue == null || returnValue.trim().equals("")) {
			return "";
		}
		returnValue = returnValue.replaceAll("\\.\\./", ""); // ../
		returnValue = returnValue.replaceAll("\\.\\.\\\\", ""); // ..\

		return returnValue;
	}
	
	/**
	 * 업로드 가능 확장자 검사
	 * 
	 * @param fileExt
	 * @param aExt
	 * @return
	 * @throws Exception
	 */
	public boolean isValidFileExtension( String fileExt ,String[] aExt) throws Exception{
		 boolean result = false;
	        for(String ext : aExt){
	        	ext = ext.toLowerCase();
	        	fileExt = fileExt.toLowerCase();
	        	if(fileExt.equals(ext)) {
	        		result = true;
	        		break;
	        	}
	        }
	        return result;
    }
}
