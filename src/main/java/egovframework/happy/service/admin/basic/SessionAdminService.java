package egovframework.happy.service.admin.basic;

/**
 * <pre>
 * </pre>
 * @Class SessionAdminService.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 14.
 * @version 1.0
 */
public interface SessionAdminService {

	String userLoginCheck(String url, String userId, String accessKey) throws Exception;

	String userLogOut(String url, String userId, String accessKey) throws Exception;

	String getUserInfo(String url, String userId, String accessKey) throws Exception;
}
