package egovframework.happy.service.admin.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import egovframework.happy.service.client.vo.CurationProcessVO;
import egovframework.happy.service.client.vo.CurationVO;
import egovframework.happy.service.client.vo.ProcessDTO;

/**
 * <pre>
 * 큐레이션 관련된 Service
 * </pre>
 * @Class CurationService.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
public interface CurationService {
	
	/**
	 * <pre>
	 * 큐레이션 조회
	 * </pre>
	 * @Class CurationService.java
	 * @Method selectList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<CurationVO> selectList(HashMap<String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * 큐레이션 등록
	 * </pre>
	 * @Class CurationService.java
	 * @Method insertCuration
	 * @param curationVO
	 * @return
	 * @throws Exception 
	 */
	int insertCuration(CurationVO curationVO) throws Exception;
	
	/**
	 * <pre>
	 * 큐레이션 수정
	 * </pre>
	 * @Class CurationService.java
	 * @Method updateCuration
	 * @param curationVO
	 * @return
	 * @throws Exception 
	 */
	int updateCuration(CurationVO curationVO) throws Exception;

	/**
	 * <pre>
	 * 전체 큐레이션의 수
	 * </pre>
	 * @Class CurationMapper.java
	 * @Method curationTotCnt
	 * @return int
	 * @throws Exception 
	 */
	int curationTotCnt() throws Exception;

	/**
	 * <pre>
	 * 큐레이션의 seq로 큐레이션 전체 내용 조회
	 * </pre>
	 * @Class CurationService.java
	 * @Method selectCurationBySeq
	 * @param curationVO
	 * @return
	 * @throws Exception 
	 */
	CurationVO selectCurationBySeq(CurationVO curationVO) throws Exception;

	/**
	 * <pre>
	 * 과정 검색에 따른 과정 목록 출력
	 * </pre>
	 * @Class CurationService.java
	 * @Method selectProcessByType
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<ProcessDTO> selectProcessByType(Map<String, Object> map) throws Exception;

	/**
	 * <pre>
	 * 과정코드로 과정 검색
	 * </pre>
	 * @Class CurationService.java
	 * @Method selectProcessByCode
	 * @param pCode
	 * @return
	 * @throws Exception 
	 */
	ProcessDTO selectProcessByCode(String pCode) throws Exception;

	/**
	 * <pre>
	 * 가장 최근에 INSERT 된 큐레이션 조회
	 * </pre>
	 * @Class CurationService.java
	 * @Method lastInsertCurationSeq
	 * @return
	 * @throws Exception 
	 */
	int lastInsertCurationSeq() throws Exception;

	/**
	 * <pre>
	 * 큐레이션에 큐레이션과정  추가
	 * </pre>
	 * @Class CurationService.java
	 * @Method insertCurationProcess
	 * @param cpList
	 * @return
	 * @throws Exception 
	 */
	int insertCurationProcess(List<CurationProcessVO> cpList) throws Exception;

	/**
	 * <pre>
	 * 큐레이션의 큐레이션과정 조회 
	 * </pre>
	 * @Class CurationService.java
	 * @Method selectCurationProcessBySeq
	 * @param curationList
	 * @return
	 * @throws Exception 
	 */
	List<CurationProcessVO> selectCurationProcessBySeq(List<CurationVO> curationList) throws Exception;

	/**
	 * <pre>
	 *  큐레이션Seq에 해당하는 큐레이션과정 삭제
	 * </pre>
	 * @Class CurationService.java
	 * @Method deleteCurationProcess
	 * @param seq
	 * @return
	 * @throws Exception 
	 */
	int deleteCurationProcess(int seq) throws Exception;	//큐레이션의 등록된 과정 삭제

	/**
	 * <pre>
	 * 큐레이션 순서 변경
	 * </pre>
	 * @Class CurationService.java
	 * @Method updateCurationOrder
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	int updateCurationOrder(HashMap<String, Object> map) throws Exception;
}
