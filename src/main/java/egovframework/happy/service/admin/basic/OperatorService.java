package egovframework.happy.service.admin.basic;

import java.util.List;
import java.util.Map;

import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.OperatorVO;

/**
 * <pre>
 * [관리자페이지]
 * 운영자 관리 관련 Service
 * </pre>
 * @Class OperatorService.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 19.
 * @version 1.0
 */
public interface OperatorService {

	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 전체 목록을 가져온다 (service)
	 * </pre>
	 * @Class OperatorService.java
	 * @Method selectOperatorList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<OperatorVO> selectOperatorList(Map<String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 전체 목록 갯수를 가져온다.
	 * </pre>
	 * @Class OperatorService.java
	 * @Method getCountOfAllOperator
	 * @return
	 * @throws Exception 
	 */
	int getCountOfAllOperator() throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 운영자를 추가한다.
	 * </pre>
	 * @Class OperatorService.java
	 * @Method insertOperator
	 * @param operatorVO
	 * @throws Exception 
	 */
	void insertOperator(OperatorVO operatorVO) throws Exception;
	
	/**
	 * <pre>
	 * 관라지페이지에서 '운영자관리'의 특정 운영자의 Q&A 게시판 공개상태를 변경한다.
	 * </pre>
	 * @Class OperatorService.java
	 * @Method updateOperatorOpenStatus
	 * @param operatorVO
	 * @return
	 * @throws Exception 
	 */
	int updateOperatorOpenStatus(OperatorVO operatorVO) throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 사용자를 검색한다. (AJAX 데이터 처리)
	 * </pre>
	 * @Class OperatorService.java
	 * @Method searchEmployeeByType
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<MemberVO> searchEmployeeByType(Map<String, Object> map) throws Exception;

	/**
	 * <pre>
	 * 관리자페이지 '운영자관리'의 운영자를 추가하기전, 실제 존재하는 User인지 확인한다.
	 * </pre>
	 * @Class OperatorService.java
	 * @Method selectByUserId
	 * @param suserId
	 * @return
	 * @throws Exception 
	 */
	int selectByUserId(String suserId) throws Exception;
}
