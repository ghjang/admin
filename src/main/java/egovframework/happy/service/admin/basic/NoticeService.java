package egovframework.happy.service.admin.basic;

import java.util.HashMap;
import java.util.List;

import egovframework.happy.service.client.vo.NoticeVO;

public interface NoticeService {

	/**
	 * <pre>
	 * 공지사항 등록
	 * </pre>
	 * @Class NoticeService.java
	 * @Method insertNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int insertNotice(NoticeVO notice) throws Exception;		
	/**
	 * <pre>
	 * 공지사항 삭제
	 * </pre>
	 * @Class NoticeService.java
	 * @Method deleteNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int deleteNotice(NoticeVO notice) throws Exception;	
	/**
	 * <pre>
	 * 공지사항 수정
	 * </pre>
	 * @Class NoticeService.java
	 * @Method updateNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int updateNotice(NoticeVO notice) throws Exception;	
	/**
	 * <pre>
	 * 공지사항 공개여부 수정
	 * </pre>
	 * @Class NoticeService.java
	 * @Method updateOpenInfo
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int updateOpenInfo(NoticeVO notice) throws Exception;
	/**
	 * <pre>
	 * 공지사항 전체 수
	 * </pre>
	 * @Class NoticeService.java
	 * @Method selectNoticeListTotCnt
	 * @return
	 * @throws Exception 
	 */
	int selectNoticeListTotCnt() throws Exception;
	/**
	 * <pre>
	 * 공지사항 자세히 보기
	 * </pre>
	 * @Class NoticeService.java
	 * @Method selectNoticeDetailBySeq
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	NoticeVO selectNoticeDetailBySeq(int id) throws Exception;	
	/**
	 * <pre>
	 * 공지사항 리스트 출력
	 * </pre>
	 * @Class NoticeService.java
	 * @Method list
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<NoticeVO> list(HashMap<String, Integer> map) throws Exception;
}
