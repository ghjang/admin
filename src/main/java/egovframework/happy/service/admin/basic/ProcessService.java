package egovframework.happy.service.admin.basic;

import java.util.HashMap;
import java.util.List;

import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.ProcessVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;

/**
 * <pre>
 * [관리자페이지]
 * 과정현황 관련 Service
 * </pre>
 * @Class ProcessService.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
public interface ProcessService {

	/**
	 * <pre>
	 * 전체 과정 조회
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectProcessList
	 * @return 전체 과정 리스트
	 * @throws Exception 
	 */
	List<ProcessDTO> selectProcessList() throws Exception;
	/**
	 * <pre>
	 * 전체 과정 수 
	 * </pre>
	 * @Class ProcessService.java
	 * @Method processDefaultCnt
	 * @return 전체 과정의 수 
	 * @throws Exception 
	 */
	int processDefaultCnt() throws Exception;
	/**
	 * <pre>
	 * 해당 과정의 태그 전체 조회
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectAllTagList
	 * @param processVO
	 * @return 해당과정의 태그 리스트
	 * @throws Exception 
	 */
	List<TagVO> selectAllTagList(ProcessVO processVO) throws Exception;	
	/**
	 * <pre>
	 * 해당 과정의 태그 일부 조회(5개)
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectTagList
	 * @param processDTO
	 * @return 해당 과정의 태그 리스트 일부
	 * @throws Exception 
	 */
	List<TagVO> selectTagList(ProcessDTO processDTO) throws Exception;
	
	/**
	 * <pre>
	 * 해당 과정의 수강후기 보기
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectReviewList
	 * @param map
	 * @return 해당 과정의 수강후기 리스트
	 * @throws Exception 
	 */
	List<TakeLectureReviewVO> selectReviewList(HashMap<String, Object> map) throws Exception;	
	/**
	 * <pre>
	 * 해당 과정의 수강자 보기
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectTakeLectureList
	 * @param map
	 * @return 해당 과정의 수강자 리스트
	 * @throws Exception 
	 */
	List<TakeLectureVO> selectTakeLectureList(HashMap<String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * 과정의 공개설정 변경
	 * </pre>
	 * @Class ProcessService.java
	 * @Method updateOpenStatus
	 * @param processDTO
	 * @return 업데이트된 로우의 수
	 * @throws Exception 
	 */
	int updateOpenStatus(ProcessDTO processDTO) throws Exception;	
	/**
	 * <pre>
	 * 해당 과정 차단으로 변경 및 차단일, 차단 이유 업데이트
	 * </pre>
	 * @Class ProcessService.java
	 * @Method updateBlockStatus
	 * @param processBlock
	 * @return 업데이트된 로우의 수
	 * @throws Exception 
	 */
	int updateBlockStatus(ProcessBlockVO processBlock) throws Exception;
	
	/**
	 * <pre>
	 * 해당 과정의 전체 수강자 수 조회
	 * </pre>
	 * @Class ProcessService.java
	 * @Method takeLecturePeopleCnt
	 * @param processDTO
	 * @return 해당 과정의 수강자 수
	 * @throws Exception 
	 */
	int takeLecturePeopleCnt(ProcessDTO processDTO) throws Exception;
	/**
	 * <pre>
	 * 과정현황 리스트 출력 
	 * </pre>
	 * @Class ProcessService.java
	 * @Method list
	 * @param pageMap
	 * @return 과정 리스트
	 * @throws Exception 
	 */
	List<ProcessDTO> list(HashMap<String, Object> pageMap) throws Exception;
	/**
	 * <pre>
	 * 검색결과에 따른 과정의 수
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectSerchCnt
	 * @param map
	 * @return 검색결과에 따른 과정의 수
	 * @throws Exception 
	 */
	int selectSerchCnt(HashMap<String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * 과정 수강후기의 댓글보기
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectReviewTailList
	 * @param takeLectureReviewVO
	 * @return 해당 수강후기의 댓글 리스트
	 * @throws Exception 
	 */
	List<TakeLectureReviewVO> selectReviewTailList(TakeLectureReviewVO takeLectureReviewVO) throws Exception;
	
	/**
	 * <pre>
	 * 과정 차단에 대한 내용을 TBL_PROCESS_BLOCK에 기록
	 * </pre>
	 * @Class ProcessService.java
	 * @Method insertProcessBlock
	 * @param processBlock
	 * @return insert된 로우의 수
	 * @throws Exception 
	 */
	int insertProcessBlock(ProcessBlockVO processBlock) throws Exception;	
	/**
	 * <pre>
	 * 차단 사유 출력
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectBlockReason
	 * @param process_Code
	 * @return 해당 과정의 차단 사유 리스트
	 * @throws Exception 
	 */
	List<ProcessBlockVO> selectBlockReason(String process_Code) throws Exception;
	/**
	 * <pre>
	 * 차단 메일내용 자세히보기
	 * </pre>
	 * @Class ProcessService.java
	 * @Method selectBlockReasonDetail
	 * @param processBlockVO
	 * @return 차단 메일 내용
	 * @throws Exception 
	 */
	ProcessBlockVO selectBlockReasonDetail(ProcessBlockVO processBlockVO) throws Exception;
	
	/**
	 * <pre>
	 * 태그명 검색
	 * </pre>
	 * @Class ProcessService.java
	 * @Method searchTagList
	 * @param tempMap
	 * @return 과정코드 리스트
	 * @throws Exception 
	 */
	List<String> searchTagList(HashMap<String, String> tempMap) throws Exception;
}
