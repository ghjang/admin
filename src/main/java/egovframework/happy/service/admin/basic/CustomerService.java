package egovframework.happy.service.admin.basic;

import java.util.List;
import java.util.Map;

import egovframework.happy.service.client.vo.CustomerVO;

/**
 * <pre>
 * [관리자페이지]
 * 고객센터에 관한 Controller
 * </pre>
 * @Class CustomerService.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 19.
 * @version 1.0
 */
public interface CustomerService {
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'의 리스트를 가져온다 (페이징 적용)
	 * </pre>
	 * @Class CustomerService.java
	 * @Method selecCustomertList
	 * @param Map <Stirng, Object>
	 * @return List<CustomerVO>
	 * @throws Exception 
	 */
	List<CustomerVO> selecCustomertList(Map <String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'의 총 갯수를 가져온다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method customerTotCnt
	 * @return Count of All Customer (int)
	 * @throws Exception 
	 */
	int customerTotCnt() throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 질문에 답변이 완료된 총 갯수를 가져온다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method customerApplyYCnt
	 * @return int
	 * @throws Exception 
	 */
	int customerApplyYCnt() throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 질문에 답변이 대기중인 총 갯수를 가져온다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method customerApplyNCnt
	 * @return int
	 * @throws Exception 
	 */
	int customerApplyNCnt() throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 등록된 문의글에 답변을 등록 한다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method writeApplyinAdminPage
	 * @param customerVO
	 * @return
	 * @throws Exception 
	 */
	int writeApplyinAdminPage(CustomerVO customerVO) throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 등록된 문의글에 답변을 등록할때, 로그를 남긴다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method writeApplyLogInAdminPage
	 * @param customerVO
	 * @return
	 * @throws Exception 
	 */
	int writeApplyLogInAdminPage(CustomerVO customerVO) throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 검색 (페이징 적용)
	 * </pre>
	 * @Class CustomerService.java
	 * @Method searchCustomertList
	 * @param Map <Stirng, Object>
	 * @return List<CustomerVO>
	 * @throws Exception 
	 */
	List<CustomerVO> searchCustomertList(Map <String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 검색한 갯수를 가져온다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method searchTotCnt
	 * @param Map <Stirng, Object>
	 * @return int
	 * @throws Exception 
	 */
	int searchTotCnt(Map <String, Object> map) throws Exception;

	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 Cust_Center_Seq로 자세한 내용을 가져온다.
	 * </pre>
	 * @Class CustomerService.java
	 * @Method selectCustomerQnA
	 * @param customerVO
	 * @return
	 * @throws Exception 
	 */
	CustomerVO selectCustomerQnA(CustomerVO customerVO) throws Exception;
}
