package egovframework.happy.service.impl.client.basic;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.basic.CommonService;
import egovframework.happy.service.client.vo.FileVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("commonService")
public class CommonServiceImpl extends EgovAbstractServiceImpl implements CommonService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="commonMapper")
	private CommonMapper commonMapper;
	
	@Override
	public List<?> selectCode(String s) throws Exception {
		return commonMapper.selectCode(s);
	}

	@Override
	public int insertFileInfo(FileVO vo) throws Exception {
		return commonMapper.insertFileInfo(vo);
	}

	@Override
	public int deleteFileInfo(FileVO vo) throws Exception {
		return commonMapper.deleteFileInfo(vo);
	}

	@Override
	public FileVO selectFileinfo(int seq) throws Exception {
		return commonMapper.selectFileinfo(seq);
	}
}
