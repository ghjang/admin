package egovframework.happy.service.impl.client.log;

import java.util.List;

import egovframework.happy.service.client.vo.SampleDefaultVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("logSampleMapper")
public interface LogSampleMapper {
	/**
	 * 글 목록을 조회한다.
	 * @param logVO - 조회할 정보가 담긴 VO
	 * @return 글 목록
	 * @exception Exception
	 */
	List<?> selectLogList(SampleDefaultVO sampleVO) throws Exception;

	/**
	 * 글 총 갯수를 조회한다.
	 * @param logVO - 조회할 정보가 담긴 VO
	 * @return 글 총 갯수
	 * @exception
	 */
	int selectLogListTotCnt(SampleDefaultVO sampleVO);
}