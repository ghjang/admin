package egovframework.happy.service.impl.client.basic;

import egovframework.happy.service.client.vo.ChapterVO;
import egovframework.happy.service.client.vo.CourseVO;
import egovframework.happy.service.client.vo.LectureVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("logMapper")
public interface LogMapper {
	int insertTblProcessLog(CourseVO vo) throws Exception;
	int insertTblChapterLog(ChapterVO vo) throws Exception;
	int insertTblLectureLog1(CourseVO vo) throws Exception;
	int insertTblLectureLog2(LectureVO vo) throws Exception;
	int insertTblAgreementLog(CourseVO vo) throws Exception;
	int insertCourseVisitorLog(CourseVO vo) throws Exception;
}
