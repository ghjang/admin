package egovframework.happy.service.impl.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("lectureRoomMapper")
public interface LectureRoomMapper {

	List<?> selectTakeLectureList(TakeLectureVO vo) throws Exception;
	TakeLectureVO selectTakeLecture(TakeLectureVO vo) throws Exception;
	int insertTakeLectureReview(TakeLectureReviewVO vo) throws Exception;
}
