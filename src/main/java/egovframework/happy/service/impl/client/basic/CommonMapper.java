package egovframework.happy.service.impl.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.FileVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("commonMapper")
public interface CommonMapper {
	List<?> selectCode(String s) throws Exception;
	
	int insertFileInfo(FileVO vo) throws Exception;
	int deleteFileInfo(FileVO vo) throws Exception;
	//List<?> selectFileList(FileVO vo) throws Exception;
	FileVO selectFileinfo(int seq) throws Exception;
}
