package egovframework.happy.service.impl.client.basic;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.basic.QnaService;
import egovframework.happy.service.client.vo.QnaAnswerReplyVO;
import egovframework.happy.service.client.vo.QnaAnswerVO;
import egovframework.happy.service.client.vo.QnaVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("qnaService")
public class QnaServiceImpl extends EgovAbstractServiceImpl implements QnaService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="qnaMapper")
	private QnaMapper qnaMapper;
	
	@Resource(name="logMapper")
	private LogMapper logMapper;

	@Override
	public int selectQnaListCount(QnaVO vo) throws Exception {
		return qnaMapper.selectQnaListCount(vo);
	}
	
	@Override
	public List<?> selectQnaList(QnaVO vo) throws Exception {
		return qnaMapper.selectQnaList(vo);
	}

	@Override
	public QnaVO selectQna(QnaVO vo) throws Exception {
		return qnaMapper.selectQna(vo);
	}
	
	@Override
	public int updateQnaReadCount(QnaVO vo) throws Exception {
		return qnaMapper.updateQnaReadCount(vo);
	}
	
	@Override
	public int insertQna(QnaVO vo) throws Exception {
		return qnaMapper.insertQna(vo);
	}
	
	@Override
	public int updateQna(QnaVO vo) throws Exception {
		return qnaMapper.updateQna(vo);
	}
	
	@Override
	public int deleteQna(QnaVO vo) throws Exception {
		return qnaMapper.deleteQna(vo);
	}
	
	@Override
	public int insertProcessQuestionLike(QnaVO vo) throws Exception {
		return qnaMapper.insertProcessQuestionLike(vo);
	}

	@Override
	public int deleteProcessQuestionLike(QnaVO vo) throws Exception {
		return qnaMapper.deleteProcessQuestionLike(vo);
	}

	@Override
	public List<QnaAnswerVO> selectQnaAnswerList(QnaAnswerVO vo) throws Exception {
		return qnaMapper.selectQnaAnswerList(vo);
	}
	
	@Override
	public QnaAnswerVO selectQnaAnswer(QnaAnswerVO vo) throws Exception {
		return qnaMapper.selectQnaAnswer(vo);
	}

	@Override
	public int insertQnaAnswer(QnaAnswerVO vo) throws Exception {
		return qnaMapper.insertQnaAnswer(vo);
	}
	
	@Override
	public int updateQnaAnswer(QnaAnswerVO vo) throws Exception {
		return qnaMapper.updateQnaAnswer(vo);
	}
	
	@Override
	public int deleteQnaAnswer(QnaAnswerVO vo) throws Exception {
		return qnaMapper.deleteQnaAnswer(vo);
	}
	
	@Override
	public List<QnaAnswerReplyVO> selectQnaReplyList(QnaAnswerReplyVO vo) throws Exception {
		return qnaMapper.selectQnaReplyList(vo);
	}
	
	@Override
	public QnaAnswerReplyVO selectQnaReply(QnaAnswerReplyVO vo) throws Exception {
		return qnaMapper.selectQnaReply(vo);
	}
	
	@Override
	public int insertQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception {
		return qnaMapper.insertQnaAnswerReply(vo);
	}
	
	@Override
	public int updateQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception {
		return qnaMapper.updateQnaAnswerReply(vo);
	}
	
	@Override
	public int deleteQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception {
		return qnaMapper.deleteQnaAnswerReply(vo);
	}
}