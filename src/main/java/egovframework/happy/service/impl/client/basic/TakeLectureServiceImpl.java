package egovframework.happy.service.impl.client.basic;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.basic.TakeLectureService;
import egovframework.happy.service.client.vo.TakeLectureReviewTailVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("takeLectureService")
public class TakeLectureServiceImpl extends EgovAbstractServiceImpl implements TakeLectureService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="takeLectureMapper")
	private TakeLectureMapper takeLectureMapper;
	
	@Resource(name="logMapper")
	private LogMapper logMapper;

	@Override
	public int selectTakeLectureReviewListCount(TakeLectureReviewVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureReviewListCount(vo);
	}
	
	@Override
	public List<?> selectTakeLectureReviewList(TakeLectureReviewVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureReviewList(vo);
	}
	
	@Override
	public List<?> selectTakeLectureReviewTailList(TakeLectureReviewTailVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureReviewTailList(vo);
	}
	
	@Override
	public TakeLectureReviewTailVO selectTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureReviewTail(vo);
	}

	@Override
	public int insertTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception {
		return takeLectureMapper.insertTakeLectureReviewTail(vo);
	}
	
	@Override
	public int updateTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception {
		return takeLectureMapper.updateTakeLectureReviewTail(vo);
	}
	
	@Override
	public int deleteTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception {
		return takeLectureMapper.deleteTakeLectureReviewTail(vo);
	}
	
	@Override
	public int selectTakeLectureListCount(TakeLectureVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureListCount(vo);
	}
	
	@Override
	public List<?> selectTakeLectureList(TakeLectureVO vo) throws Exception {
		return takeLectureMapper.selectTakeLectureList(vo);
	}
}