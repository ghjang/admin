package egovframework.happy.service.impl.client.basic;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.basic.ManageStatisticsService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("manageStatisticsService")
public class ManageStatisticsServiceImpl extends EgovAbstractServiceImpl implements ManageStatisticsService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="manageStatisticsMapper")
	private ManageStatisticsMapper manageStatisticsMapper;
	
	@Resource(name="logMapper")
	private LogMapper logMapper;

}