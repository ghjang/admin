package egovframework.happy.service.impl.client.basic;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.basic.LectureRoomService;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("lectureRoomService")
public class LectureRoomServiceImpl extends EgovAbstractServiceImpl implements LectureRoomService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="lectureRoomMapper")
	private LectureRoomMapper lectureRoomMapper;
	
	@Override
	public List<?> selectTakeLectureList(TakeLectureVO vo) throws Exception {
		return lectureRoomMapper.selectTakeLectureList(vo);
	}

	@Override
	public TakeLectureVO selectTakeLecture(TakeLectureVO vo) throws Exception {
		return lectureRoomMapper.selectTakeLecture(vo);
	}

	@Override
	public int insertTakeLectureReview(TakeLectureReviewVO vo) throws Exception {
		return lectureRoomMapper.insertTakeLectureReview(vo);
	}

}
