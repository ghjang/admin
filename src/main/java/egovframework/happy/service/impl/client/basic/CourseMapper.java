package egovframework.happy.service.impl.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.ChapterVO;
import egovframework.happy.service.client.vo.CourseVO;
import egovframework.happy.service.client.vo.LectureVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("courseMapper")
public interface CourseMapper {
	/**
	 * 과정 기본정보 등록/수정
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	List<CourseVO> selectCourseList(CourseVO vo) throws Exception;
	int updateCourseOpenYn(CourseVO vo) throws Exception;
	CourseVO selectCourseBasicInfo(CourseVO vo) throws Exception;
	int insertCourseBasicInfo(CourseVO vo) throws Exception;
	int updateCourseBasicInfo(CourseVO vo) throws Exception;
	String selectNowProcessCode(CourseVO vo) throws Exception;
	
	/**
	 * 태그 등록/삭제
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int insertTag(TagVO vo) throws Exception;
	int deleteTag(CourseVO vo) throws Exception;

	/**
	 * 과정 소개글
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int updateCourseIntro(CourseVO vo) throws Exception;
	
	/**
	 * 챕터 등록/수정/삭제
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	List<?> selectChapterList(ChapterVO vo) throws Exception;
	int insertChapter(ChapterVO vo) throws Exception;
	int updateChapter(ChapterVO vo) throws Exception;
	int deleteChapter(ChapterVO vo) throws Exception;
	
	/**
	 * 강의 등록/수정/삭제
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	List<?> selectLectureList(LectureVO vo) throws Exception;
	LectureVO selectLecture(LectureVO vo) throws Exception;
	int insertLecture(LectureVO vo) throws Exception;
	int updateLecture(LectureVO vo) throws Exception;
	int updateLectureOrd(LectureVO vo) throws Exception;
	int deleteLecture(LectureVO vo) throws Exception;
	
	/**
	 * 강의 상세정보
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int updateCourseDtlContSetting(CourseVO vo) throws Exception;
	
	/**
	 * 샘플 강의 정보 조회/등록/삭제
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	List<?> selectSampleLectureList(CourseVO vo) throws Exception;
	int insertSampleLectureSeq(CourseVO vo) throws Exception;
	int deleteSampleLectureSeq(CourseVO vo) throws Exception;
}
