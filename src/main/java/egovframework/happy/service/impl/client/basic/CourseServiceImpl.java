package egovframework.happy.service.impl.client.basic;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.cmmn.CommonUtil;
import egovframework.happy.service.client.basic.CourseService;
import egovframework.happy.service.client.vo.ChapterVO;
import egovframework.happy.service.client.vo.CourseVO;
import egovframework.happy.service.client.vo.LectureVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("courseService")
public class CourseServiceImpl extends EgovAbstractServiceImpl implements CourseService {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EgovSampleServiceImpl.class);

	@Resource(name="courseMapper")
	private CourseMapper courseMapper;
	
	@Resource(name="logMapper")
	private LogMapper logMapper;

	@Override
	public List<CourseVO> selectCourseList(CourseVO vo) throws Exception {
		return courseMapper.selectCourseList(vo);
	}
	
	@Override
	public int updateCourseOpenYn(CourseVO vo) throws Exception {
		int i = courseMapper.updateCourseOpenYn(vo);
		logMapper.insertTblProcessLog(vo);
		return i;
	}
	
	@Override
	public int updateCourseOpenYnAgreementLog(CourseVO vo) throws Exception {
		int i = this.updateCourseOpenYn(vo);
		logMapper.insertTblAgreementLog(vo);
		return i;
	}
	
	@Override
	public CourseVO selectCourseBasicInfo(CourseVO vo) throws Exception {
		return courseMapper.selectCourseBasicInfo(vo);
	}
	
	@Override
	public int insertCourseBasicInfo(CourseVO vo) throws Exception {
		int i = courseMapper.insertCourseBasicInfo(vo);
		logMapper.insertTblProcessLog(vo);
		return i;
	}

	@Override
	public int updateCourseBasicInfo(CourseVO vo) throws Exception {
		int i = courseMapper.updateCourseBasicInfo(vo);
		logMapper.insertTblProcessLog(vo);
		return i;
	}

	@Override
	public String selectNowProcessCode(CourseVO vo) throws Exception {
		return courseMapper.selectNowProcessCode(vo);
	}
	
	@Override
	public int insertTag(TagVO vo) throws Exception {
		return courseMapper.insertTag(vo);
	}

	@Override
	public int deleteTag(CourseVO vo) throws Exception {
		return courseMapper.deleteTag(vo);
	}

	@Override
	public int updateCourseIntro(CourseVO vo) throws Exception {
		int i = courseMapper.updateCourseIntro(vo);
		logMapper.insertTblProcessLog(vo);
		return i;
	}

	@Override
	public List<?> selectChapterList(ChapterVO vo) throws Exception {
		return courseMapper.selectChapterList(vo);
	}

	@Override
	public int insertChapter(ChapterVO vo) throws Exception {
		int i = courseMapper.insertChapter(vo);
		logMapper.insertTblChapterLog(vo);
		return i;
	}

	@Override
	public int updateChapter(ChapterVO vo) throws Exception {
		int i = courseMapper.updateChapter(vo);
		logMapper.insertTblChapterLog(vo);
		return i;
	}

	@Override
	public int deleteChapter(ChapterVO vo) throws Exception {
		int i = courseMapper.deleteChapter(vo);
		logMapper.insertTblChapterLog(vo);
		return i;
	}

	@Override
	public int insertLecture(LectureVO vo) throws Exception {
		int i = courseMapper.insertLecture(vo);
		logMapper.insertTblLectureLog2(vo);
		return i;
	}

	@Override
	public int updateLecture(LectureVO vo) throws Exception {
		int i = courseMapper.updateLecture(vo);
		logMapper.insertTblLectureLog2(vo);
		return i;
	}
	
	@Override
	public int updateLectureOrd(LectureVO vo) throws Exception {
		int i = courseMapper.updateLectureOrd(vo);
		logMapper.insertTblLectureLog2(vo);
		return i;
	}

	@Override
	public int deleteLecture(LectureVO vo) throws Exception {
		int i = courseMapper.deleteLecture(vo);
		logMapper.insertTblLectureLog2(vo);
		return i;
	}
	
	@Override
	public List<?> selectLectureList(LectureVO vo) throws Exception {
		return courseMapper.selectLectureList(vo);
	}
	
	@Override
	public LectureVO selectLecture(LectureVO vo) throws Exception {
		return courseMapper.selectLecture(vo);
	}

	@Override
	public void mergeChapterLecture(ChapterVO vo1, LectureVO vo2) throws Exception {
		String process_Code = vo1.getProcess_Code();
		int[] chapterSeq = CommonUtil.stringArrTointArr(vo1.getMulti_Chapter_Seq().split("\\|"));
		int[] chapterIdx = CommonUtil.stringArrTointArr(vo1.getTmp_Chapter_Index().split("\\|"));
		String[] chapterNm = vo1.getChapter_Nm().split("\\|");
		String[] chapterLectureSeq = vo2.getMulti_Lecture_Seq().split("\\|");
		String[] lectureIdx = vo2.getTmp_Lecture_Index().split("\\|");
		String[] lectureNm = vo2.getLecture_Nm().split("\\|");
		String[] lectureUrl = vo2.getLecture_Url().split("\\|");
		String[] lectureTime = vo2.getTmp_Lecture_Time().split("\\|");

		//챕터 등록/수정, 강의 수정
		if(! "".equals(vo1.getTmp_Chapter_Index())) {
			for(int x=0; x<chapterIdx.length; x++) {
			//for(int cIdx : chapterIdx) {
				ChapterVO cVo = new ChapterVO();
				cVo.setProcess_Code(process_Code);
				cVo.setChapter_Nm(chapterNm[x]);
				if(chapterSeq[x] != 0) {
					//챕처 수정
					cVo.setChapter_Seq(chapterSeq[x]);
					this.updateChapter(cVo);
				} else {
					//챕터 등록
					this.insertChapter(cVo);
				}
				
				//챕터 등록
				LectureVO lVo = new LectureVO();
				lVo.setProcess_Code(process_Code);
				lVo.setChapter_Seq(cVo.getChapter_Seq());

				for(int i=0; i<lectureIdx.length; i++) {
					if(! "".equals(lectureIdx[i])) {
						int c = CommonUtil.stringArrTointArr(lectureIdx[i].split(","))[0];
						int l = CommonUtil.stringArrTointArr(lectureIdx[i].split(","))[1];
						if(chapterIdx[x] == c) {
							lVo.setLecture_Nm(lectureNm[i]);
							lVo.setLecture_Url(lectureUrl[i]);
							lVo.setLecture_Time(Integer.parseInt(lectureTime[i]));
							lVo.setLecture_Index(l);
							
							this.insertLecture(lVo);
						}
					}
				}
			}
		}

		//챕터 수정
		for(int i=0; i<chapterLectureSeq.length; i++) {
			if(! "".equals(chapterLectureSeq[i]) && ! ",".equals(chapterLectureSeq[i])) {
				int[] tmp = CommonUtil.stringArrTointArr(chapterLectureSeq[i].split(","));
				if(tmp.length == 2) {
					int lSeq = tmp[1];
					int lIdx = CommonUtil.stringArrTointArr(lectureIdx[i].split(","))[1];
					
					LectureVO lVo = new LectureVO();
					lVo.setLecture_Seq(lSeq);
					lVo.setLecture_Nm(lectureNm[i]);
					lVo.setLecture_Url(lectureUrl[i]);
					lVo.setLecture_Time(Integer.parseInt(lectureTime[i]));
					lVo.setLecture_Index(lIdx);
					
					this.updateLecture(lVo);
				} else {
					int cSeq = tmp[0];
					int lIdx = CommonUtil.stringArrTointArr(lectureIdx[i].split(","))[1];
					
					LectureVO lVo = new LectureVO();
					lVo.setProcess_Code(process_Code);
					lVo.setChapter_Seq(cSeq);
					lVo.setLecture_Nm(lectureNm[i]);
					lVo.setLecture_Url(lectureUrl[i]);
					lVo.setLecture_Time(Integer.parseInt(lectureTime[i]));
					lVo.setLecture_Index(lIdx);
					
					this.insertLecture(lVo);
				}
			}
		}
	}

	@Override
	public int updateCourseDtlContSetting(CourseVO vo) throws Exception {
		int i = courseMapper.updateCourseDtlContSetting(vo);
		logMapper.insertTblProcessLog(vo);
		return i;
	}

	@Override
	public int insertSampleLectureSeq(CourseVO vo) throws Exception {
		int i = courseMapper.insertSampleLectureSeq(vo);
		//<|누락|>
		//logMapper.insertTblLectureLog1(vo);
		return i;
	}

	@Override
	public int deleteSampleLectureSeq(CourseVO vo) throws Exception {
		return courseMapper.deleteSampleLectureSeq(vo);
	}

	@Override
	public List<?> selectSampleLectureList(CourseVO vo) throws Exception {
		return courseMapper.selectSampleLectureList(vo);
	}

	@Override
	public int insertCourseVisitorLog(CourseVO vo) throws Exception {
		return logMapper.insertCourseVisitorLog(vo);
	}
}
