package egovframework.happy.service.impl.client.log;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.client.log.EgovLogService;
import egovframework.happy.service.client.vo.SampleDefaultVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("logService")
public class EgovLogServiceImpl extends EgovAbstractServiceImpl implements EgovLogService {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(EgovLogServiceImpl.class);
	
	@Resource(name="logSampleMapper")
	private LogSampleMapper logSampleMapper;
	
	/**
	 * 글 목록을 조회한다.
	 * @param sampleVO - 조회할 정보가 담긴 VO
	 * @return 글 목록
	 * @exception Exception
	 */
	@Override
	public List<?> selectLogList(SampleDefaultVO sampleVO) throws Exception {
		return logSampleMapper.selectLogList(sampleVO);
	}
	
	/**
	 * 글 총 갯수를 조회한다.
	 * @param sampleVO - 조회할 정보가 담긴 VO
	 * @return 글 총 갯수
	 * @exception
	 */
	@Override
	public int selectLogListTotCnt(SampleDefaultVO sampleVO) {
		return logSampleMapper.selectLogListTotCnt(sampleVO);
	}
}
