package egovframework.happy.service.impl.client.basic;

import egovframework.happy.service.client.vo.EmployeeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("sessionMapper")
public interface SessionMapper {
	EmployeeVO selectEmployee(String User_Id) throws Exception;
	int insertEmployee(EmployeeVO vo) throws Exception;
	int updateEmployee(EmployeeVO vo) throws Exception;
}
