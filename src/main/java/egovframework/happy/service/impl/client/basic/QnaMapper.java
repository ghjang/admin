package egovframework.happy.service.impl.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.QnaAnswerReplyVO;
import egovframework.happy.service.client.vo.QnaAnswerVO;
import egovframework.happy.service.client.vo.QnaVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("qnaMapper")
public interface QnaMapper {
	/**
	 * QNA 조회/등록/수정/삭제
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int selectQnaListCount(QnaVO vo) throws Exception;
	List<?> selectQnaList(QnaVO vo) throws Exception;
	QnaVO selectQna(QnaVO vo) throws Exception;
	int updateQnaReadCount(QnaVO vo) throws Exception;
	int insertQna(QnaVO vo) throws Exception;
	int updateQna(QnaVO vo) throws Exception;
	int deleteQna(QnaVO vo) throws Exception;
	int insertProcessQuestionLike(QnaVO vo) throws Exception;
	int deleteProcessQuestionLike(QnaVO vo) throws Exception;
	List<QnaAnswerVO> selectQnaAnswerList(QnaAnswerVO vo) throws Exception;
	QnaAnswerVO selectQnaAnswer(QnaAnswerVO vo) throws Exception;
	int insertQnaAnswer(QnaAnswerVO vo) throws Exception;
	int updateQnaAnswer(QnaAnswerVO vo) throws Exception;
	int deleteQnaAnswer(QnaAnswerVO vo) throws Exception;
	List<QnaAnswerReplyVO> selectQnaReplyList(QnaAnswerReplyVO vo) throws Exception;
	QnaAnswerReplyVO selectQnaReply(QnaAnswerReplyVO vo) throws Exception;
	int insertQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception;
	int updateQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception;
	int deleteQnaAnswerReply(QnaAnswerReplyVO vo) throws Exception;
}
