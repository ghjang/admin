package egovframework.happy.service.impl.admin.basic;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.CustomerService;
import egovframework.happy.service.client.vo.CustomerVO;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Resource(name="customerMapper")
	private CustomerMapper customerMapper;
	
	@Override
	public List<CustomerVO> selecCustomertList(Map <String, Object> map) throws Exception {
		try {
			return customerMapper.selecCustomertList(map);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public int customerTotCnt() throws Exception {
		try {
			return customerMapper.customerTotCnt();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int customerApplyYCnt() throws Exception {
		try {
			return customerMapper.customerApplyYCnt();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int customerApplyNCnt() throws Exception {
		try {
			return customerMapper.customerApplyNCnt();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int writeApplyinAdminPage(CustomerVO customerVO) throws Exception {
		try {
			return customerMapper.writeApplyinAdminPage(customerVO);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public List<CustomerVO> searchCustomertList(Map <String, Object> map) throws Exception {
		try {
			return customerMapper.searchCustomertList(map);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public int searchTotCnt(Map <String, Object> map) throws Exception {
		try {
			System.out.println("시간 : " + map.get("sDate") + " / " + map.get("eDate"));
			return customerMapper.searchTotCnt(map);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public CustomerVO selectCustomerQnA(CustomerVO customerVO) throws Exception {
		try {
			return customerMapper.selectCustomerQnA(customerVO);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int writeApplyLogInAdminPage(CustomerVO customerVO) throws Exception {
		try {
			return customerMapper.writeApplyLogInAdminPage(customerVO);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}