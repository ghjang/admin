package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.NoticeService;
import egovframework.happy.service.client.vo.NoticeVO;

@Service("noticeService")
public class NoticeServiceImpl implements NoticeService {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(NoticeServiceImpl.class);
	
	@Resource(name="noticeMapper")
	private NoticeMapper noticeMapper;

	@Override
	public int insertNotice(NoticeVO notice) throws Exception {
		return noticeMapper.insertNotice(notice);
	}

	@Override
	public int deleteNotice(NoticeVO notice) throws Exception {
		return noticeMapper.deleteNotice(notice);
	}

	@Override
	public int updateNotice(NoticeVO notice) throws Exception {
		return noticeMapper.updateNotice(notice);
	}

	@Override
	public int updateOpenInfo(NoticeVO notice) throws Exception {
		return noticeMapper.updateOpenInfo(notice);
	}
	@Override
	public int selectNoticeListTotCnt() throws Exception {
		return noticeMapper.selectNoticeListTotCnt();
	}

	@Override
	public NoticeVO selectNoticeDetailBySeq(int id) throws Exception {
		return noticeMapper.selectNoticeDetailBySeq(id);
	}

	@Override
	public List<NoticeVO> list(HashMap<String, Integer> map) throws Exception {
		return noticeMapper.list(map);
	}

}
