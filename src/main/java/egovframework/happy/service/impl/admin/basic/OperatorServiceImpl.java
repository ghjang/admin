package egovframework.happy.service.impl.admin.basic;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.OperatorService;
import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.OperatorVO;

@Service("operatorService")
public class OperatorServiceImpl implements OperatorService {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(OperatorServiceImpl.class);
	
	@Resource(name="operatorMapper")
	private OperatorMapper operatorMapper;
	
	public List<OperatorVO> selectOperatorList(Map<String, Object> map) throws Exception {
		try{
			return operatorMapper.selectOperatorList(map);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("[ERROR] : " + e.getMessage());
			return null;
		}
	}
	
	public int getCountOfAllOperator() throws Exception {
		return operatorMapper.getCountOfAllOperator();
	}
	
	public void insertOperator(OperatorVO operatorVO) throws Exception {
		operatorMapper.insertOperator(operatorVO);
	}
	
	public int updateOperatorOpenStatus(OperatorVO operatorVO) throws Exception {
		return operatorMapper.updateOperatorOpenStatus(operatorVO);
	}
	
	public List<MemberVO> searchEmployeeByType(Map<String, Object> map) throws Exception {
		return operatorMapper.searchEmployeeByType(map);
	}

	@Override
	public int selectByUserId(String suserId) throws Exception {
		return operatorMapper.selectByUserId(suserId);
	}
}
