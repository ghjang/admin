package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.ProcessService;
import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.ProcessVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;

@Service("processService")
public class ProcessServiceImpl implements ProcessService {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProcessServiceImpl.class);
	
	@Resource(name="processMapper")
	private ProcessMapper processMapper;

	@Override
	public List<TagVO> selectAllTagList(ProcessVO processVO) throws Exception {
		return processMapper.selectAllTagList(processVO);
	}

	@Override
	public List<ProcessDTO> selectProcessList() throws Exception {
		return processMapper.selectProcessList();
	}

	@Override
	public List<TagVO> selectTagList(ProcessDTO processDTO) throws Exception {
		return processMapper.selectTagList(processDTO);
	}

	@Override
	public List<TakeLectureReviewVO> selectReviewList(HashMap<String, Object> map) throws Exception {
		return processMapper.selectReviewList(map);
	}

	@Override
	public List<TakeLectureVO> selectTakeLectureList(HashMap<String, Object> map) throws Exception {
		return processMapper.selectTakeLectureList(map);
	}

	@Override
	public int updateOpenStatus(ProcessDTO processDTO) throws Exception {
		return processMapper.updateOpenStatus(processDTO);
	}

	@Override
	public int updateBlockStatus(ProcessBlockVO processBlock) throws Exception {
		return processMapper.updateBlockStatus(processBlock);
	}
	@Override
	public int takeLecturePeopleCnt(ProcessDTO processDTO) throws Exception {
		return processMapper.takeLecturePeopleCnt(processDTO);
	}
	@Override
	public int processDefaultCnt() throws Exception {
		return processMapper.processDefaultCnt();
	}

	@Override
	public List<ProcessDTO> list(HashMap<String, Object> pageMap) throws Exception {
		return processMapper.list(pageMap);
	}

	@Override
	public int selectSerchCnt(HashMap<String, Object> map) throws Exception {
		return processMapper.selectSerchCnt(map);
	}

	@Override
	public List<TakeLectureReviewVO> selectReviewTailList(TakeLectureReviewVO takeLectureReviewVO) throws Exception {
		return processMapper.selectReviewTailList(takeLectureReviewVO);
	}

	@Override
	public int insertProcessBlock(ProcessBlockVO processBlock) throws Exception {
		return processMapper.insertProcessBlock(processBlock);
	}

	@Override
	public List<ProcessBlockVO> selectBlockReason(String process_Code) throws Exception {
		return processMapper.selectBlockReason(process_Code);
	}

	@Override
	public ProcessBlockVO selectBlockReasonDetail(ProcessBlockVO processBlockVO) throws Exception {
		return processMapper.selectBlockReasonDetail(processBlockVO);
	}

	@Override
	public List<String> searchTagList(HashMap<String, String> tempMap) throws Exception {
		return processMapper.searchTagList(tempMap);
	}

}
