package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.MemberService;
import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.TakeLectureVO;

@Service("memberService")
public class MemberServiceImpl implements MemberService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Resource(name="memberMapper")
	private MemberMapper memberMapper;
	
	@Override
	public List<MemberVO> selectList(HashMap<String, Object> map) throws Exception {
		return memberMapper.selectList(map);
	}

	@Override
	public int searchCnt(HashMap<String, Object> map) throws Exception {
		return memberMapper.searchCnt(map);
	}

	@Override
	public int memberTotCnt() throws Exception {
		return memberMapper.memberTotCnt();
	}

	@Override
	public List<MemberVO> list(HashMap<String, Object> map) throws Exception {
		return memberMapper.list(map);
	}

	@Override
	public List<ProcessDTO> selectProcessById(HashMap<String, Object> map) throws Exception {
		return memberMapper.selectProcessById(map);
	}

	@Override
	public List<TakeLectureVO> selectTakeLectureById(HashMap<String, Object> map) throws Exception {
		return memberMapper.selectTakeLectureById(map);
	}

	@Override
	public int memJoinNewCnt() throws Exception {
		return memberMapper.memJoinNewCnt();
	}

	@Override
	public int memJoinOldCnt() throws Exception {
		return memberMapper.memJoinOldCnt();
	}

}
