package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.ProcessVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("processMapper")
public interface ProcessMapper {
	/**
	 * <pre>
	 * 해당 과정의 태그 전체 조회
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectAllTagList
	 * @param processVO
	 * @return
	 * @throws Exception 
	 */
	List<TagVO> selectAllTagList(ProcessVO processVO) throws Exception;
	/**
	 * <pre>
	 * 해당 과정의 태그 일부 조회(5개)
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectTagList
	 * @param processDTO
	 * @return
	 * @throws Exception 
	 */
	List<TagVO> selectTagList(ProcessDTO processDTO) throws Exception;
	
	/**
	 * <pre>
	 * 해당 과정의 수강후기 보기해당 과정의 수강후기 보기
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectReviewList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<TakeLectureReviewVO> selectReviewList(HashMap<String, Object> map) throws Exception;	
	/**
	 * <pre>
	 * 해당 과정의 수강자 보기
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectTakeLectureList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<TakeLectureVO> selectTakeLectureList(HashMap<String, Object> map) throws Exception;	
	
	/**
	 * <pre>
	 * 과정의 공개설정 변경
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method updateOpenStatus
	 * @param processDTO
	 * @return
	 * @throws Exception 
	 */
	int updateOpenStatus(ProcessDTO processDTO) throws Exception;
	/**
	 * <pre>
	 * 해당 과정 차단으로 변경 및 차단일, 차단 이유 업데이트
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method updateBlockStatus
	 * @param processBlock
	 * @return
	 * @throws Exception 
	 */
	int updateBlockStatus(ProcessBlockVO processBlock) throws Exception;
	/**
	 * <pre>
	 * 해당 과정의 전체 수강자 수 조회
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method takeLecturePeopleCnt
	 * @param processDTO
	 * @return
	 * @throws Exception 
	 */
	int takeLecturePeopleCnt(ProcessDTO processDTO) throws Exception;

	/**
	 * <pre>
	 * 전체 과정 수 
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method processDefaultCnt
	 * @return
	 * @throws Exception 
	 */
	int processDefaultCnt()	throws Exception;

	/**
	 * <pre>
	 * 과정현황 리스트 출력 
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method list
	 * @param pageMap
	 * @return
	 * @throws Exception 
	 */
	List<ProcessDTO> list(HashMap<String, Object> pageMap) throws Exception;

	/**
	 * <pre>
	 * 검색결과에 따른 과정의 수
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectSerchCnt
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	int selectSerchCnt(HashMap<String, Object> map) throws Exception;

	/**
	 * <pre>
	 * 과정 수강후기의 댓글보기
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectReviewTailList
	 * @param takeLectureReviewVO
	 * @return
	 * @throws Exception 
	 */
	List<TakeLectureReviewVO> selectReviewTailList(TakeLectureReviewVO takeLectureReviewVO) throws Exception;

	/**
	 * <pre>
	 * 과정 차단에 대한 내용을 TBL_PROCESS_BLOCK에 기록
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method insertProcessBlock
	 * @param processBlock
	 * @return
	 * @throws Exception 
	 */
	int insertProcessBlock(ProcessBlockVO processBlock) throws Exception;

	/**
	 * <pre>
	 * 차단 사유 출력
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectBlockReason
	 * @param process_Code
	 * @return
	 * @throws Exception 
	 */
	List<ProcessBlockVO> selectBlockReason(String process_Code) throws Exception;

	/**
	 * <pre>
	 * 차단 메일내용 자세히보기
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectBlockReasonDetail
	 * @param processBlockVO
	 * @return
	 * @throws Exception 
	 */
	ProcessBlockVO selectBlockReasonDetail(ProcessBlockVO processBlockVO) throws Exception;

	/**
	 * <pre>
	 * 태그명 검색
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method searchTagList
	 * @param tempMap
	 * @return
	 * @throws Exception 
	 */
	List<String> searchTagList(HashMap<String, String> tempMap) throws Exception;
	/**
	 * <pre>
	 * 전체 과정 조회
	 * </pre>
	 * @Class ProcessMapper.java
	 * @Method selectProcessList
	 * @return
	 * @throws Exception 
	 */
	List<ProcessDTO> selectProcessList() throws Exception;
}
