package egovframework.happy.service.impl.admin.basic;

import java.util.List;
import java.util.Map;

import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.OperatorVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("operatorMapper")
public interface OperatorMapper {
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 전체 목록을 가져온다. (mapper)
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method selectOperatorList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<OperatorVO> selectOperatorList(Map<String, Object> map) throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 전체 목록 갯수를 가져온다.
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method getCountOfAllOperator
	 * @return
	 * @throws Exception 
	 */
	int getCountOfAllOperator() throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 운영자를 추가한다.
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method insertOperator
	 * @param operatorVO
	 * @throws Exception 
	 */
	void insertOperator(OperatorVO operatorVO) throws Exception;
	
	/**
	 * <pre>
	 *  관라지페이지에서 '운영자관리'의 특정 운영자의 Q&A 게시판 공개상태를 변경한다.
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method updateOperatorOpenStatus
	 * @param operatorVO
	 * @return
	 * @throws Exception 
	 */
	int updateOperatorOpenStatus(OperatorVO operatorVO) throws Exception;
	
	/**
	 * <pre>
	 * 관리자페이지에서 '운영자관리'의 사용자를 검색한다. (AJAX 데이터 처리) 
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method searchEmployeeByType
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<MemberVO> searchEmployeeByType(Map<String, Object> map) throws Exception;

	/**
	 * <pre>
	 * 관리자페이지 '운영자관리'의 운영자를 추가하기전, 실제 존재하는 User인지 확인한다.
	 * </pre>
	 * @Class OperatorMapper.java
	 * @Method selectByUserId
	 * @param userId
	 * @return
	 * @throws Exception 
	 */
	int selectByUserId(String suserId) throws Exception;
}
