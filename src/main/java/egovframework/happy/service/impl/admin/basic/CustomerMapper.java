package egovframework.happy.service.impl.admin.basic;

import java.util.List;
import java.util.Map;

import egovframework.happy.service.client.vo.CustomerVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("customerMapper")
public interface CustomerMapper {
	
	/**
	 * (관리자 페이지) '고객센터'의 리스트를 가져온다 (페이징 적용)
	 * @param Map <Stirng, Object>
	 * @return List<CustomerVO>
	 * @throws Exception
	 */
	List<CustomerVO> selecCustomertList(Map <String, Object> map) throws Exception;
	
	/**
	 * (관리자 페이지) '고객센터'의 총 갯수를 가져온다.
	 * @return Count of All Customer (int)
	 * @throws Exception
	 */
	int customerTotCnt() throws Exception;
	
	/**
	 * (관리자 페이지) '고객센터'에서 질문에 답변이 완료된 총 갯수를 가져온다.
	 * @return int
	 * @throws Exception
	 */
	int customerApplyYCnt() throws Exception;
	
	/**
	 * (관리자 페이지) '고객센터'에서 질문에 답변이 대기중인 총 갯수를 가져온다.
	 * @return int
	 * @throws Exception
	 */
	int customerApplyNCnt() throws Exception;

	/**
	 * (관리자 페이지) '고객센터'에서 등록된 문의글에 답변을 등록 한다.
	 * @throws Exception
	 */
	int writeApplyinAdminPage(CustomerVO customerVO) throws Exception;
	
	/**
	 * (관리자 페이지) '고객센터'에서 검색 (페이징 적용)
	 * @param Map <Stirng, Object>
	 * @return List<CustomerVO>
	 * @throws Exception
	 */
	List<CustomerVO> searchCustomertList(Map <String, Object> map) throws Exception;
	
	/**
	 * (관리자 페이지) '고객센터'에서 검색한 갯수를 가져온다.
	 * @param Map <Stirng, Object>
	 * @return int 
	 * @throws Exception
	 */
	int searchTotCnt(Map <String, Object> map) throws Exception;

	/**
	 * (관리자 페이지) '고객센터'에서 Cust_Center_Seq로 자세한 내용을 가져온다.
	 * @param customerVO
	 * @return
	 * @throws Exception
	 */
	CustomerVO selectCustomerQnA(CustomerVO customerVO) throws Exception;

	/**
	 * <pre>
	 * (관리자 페이지) '고객센터'에서 등록된 문의글에 답변을 등록할때, 로그를 남긴다.
	 * </pre>
	 * @Class CustomerMapper.java
	 * @Method writeApplyLogInAdminPage
	 * @param customerVO
	 * @return 
	 */
	int writeApplyLogInAdminPage(CustomerVO customerVO) throws Exception;

}