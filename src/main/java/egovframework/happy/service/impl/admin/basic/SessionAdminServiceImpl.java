package egovframework.happy.service.impl.admin.basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.SessionAdminService;
import egovframework.happy.service.client.basic.SessionService;
import egovframework.happy.service.client.vo.AdminInfoVO;
import egovframework.happy.service.client.vo.EmployeeVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * <pre>
 * 세션 serevice
 * </pre>
 * @Class Name : SessionServiceImpl.java
 * @Description : SessionServiceImpl Class
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 1.
 * @version 1.0
 */
@Service("sessionAdminService")
public class SessionAdminServiceImpl extends EgovAbstractServiceImpl implements SessionAdminService {
	private static final Logger log = LoggerFactory.getLogger(SessionAdminServiceImpl.class);
	
	@Override
	public String userLoginCheck(String url, String userId, String accessKey) throws Exception {
		JSONObject data = new JSONObject();
		data.put("userId", userId);
		data.put("accessKey", accessKey);
		System.out.println("@@@@@@@@@@@@@@@@@@ [data.toString():" + data.toString() + "]");
		return this.postHttpMessage(url, data);
	}

	@Override
	public String userLogOut(String url, String userId, String accessKey) throws Exception {
		JSONObject data = new JSONObject();
		data.put("userId", userId);
		data.put("accessKey", accessKey);
		System.out.println("@@@@@@@@@@@@@@@@@@ [data.toString():" + data.toString() + "]");
		return this.postHttpMessage(url, data);
	}

	@Override
	public String getUserInfo(String url, String userId, String accessKey) throws Exception {
		JSONObject data = new JSONObject();
		data.put("userId", userId);
		data.put("accessKey", accessKey);
		System.out.println("@@@@@@@@@@@@@@@@@@ [data.toString():" + data.toString() + "]");
		return this.postHttpMessage(url, data);
	}
	
	/**
	 * <pre>
	 * POST HTTP 통신 (휴넷 API 호출용)
	 * </pre>
	 * @Class Name : SessionServiceImpl
	 * @Method Name : postHttpMessage
	 * @param url
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public String postHttpMessage(String url, JSONObject data) throws Exception {
		log.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ [url:"+url+"][data:"+data.toString()+"]");
		URL object = new URL(url);
		HttpURLConnection con = (HttpURLConnection) object.openConnection();

		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "*/*");
		con.setRequestProperty("X-Requested-With", "XMLHttpRequest");
		con.setRequestMethod("POST");

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());

		wr.write(data.toString());
		wr.flush();

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;

			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}

			br.close();
			System.out.println("" + sb.toString());

		} else {
			System.out.println(con.getResponseMessage());
		}
		
		return sb.toString();
	}
}
