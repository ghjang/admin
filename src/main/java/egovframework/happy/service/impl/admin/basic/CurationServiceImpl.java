package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.happy.service.admin.basic.CurationService;
import egovframework.happy.service.client.vo.CurationProcessVO;
import egovframework.happy.service.client.vo.CurationVO;
import egovframework.happy.service.client.vo.ProcessDTO;

/**
 * <pre>
 * 큐레이션 Service Implementation
 * </pre>
 * @Class CurationServiceImpl.java
 * @Description 큐레이션 실제 구현
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Service("curationService")
public class CurationServiceImpl implements CurationService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(NoticeServiceImpl.class);
	
	@Resource(name="curationMapper")
	private CurationMapper curationMapper;
	
	@Override
	public List<CurationVO> selectList(HashMap<String, Object> map) throws Exception {
		return curationMapper.selectList(map);
	}

	@Override
	public int insertCuration(CurationVO curationVO) throws Exception {
		return curationMapper.insertCuration(curationVO);
	}

	@Override
	public int updateCuration(CurationVO curationVO) throws Exception {
		return curationMapper.updateCuration(curationVO);
	}

	@Override
	public int curationTotCnt() throws Exception {
		return curationMapper.curationTotCnt();
	}

	@Override
	public CurationVO selectCurationBySeq(CurationVO curationVO) throws Exception {
		return curationMapper.selectCurationBySeq(curationVO);
	}

	@Override
	public List<ProcessDTO> selectProcessByType(Map<String, Object> map) throws Exception {
		return curationMapper.selectProcessByType(map);
	}

	@Override
	public ProcessDTO selectProcessByCode(String pCode) throws Exception {
		return curationMapper.selectProcessByCode(pCode);
	}

	@Override
	public int lastInsertCurationSeq() throws Exception {
		return curationMapper.lastInsertCurationSeq();
	}

	@Override
	public int insertCurationProcess(List<CurationProcessVO> cpList) throws Exception {
		return curationMapper.insertCurationProcess(cpList);
	}

	@Override
	public List<CurationProcessVO> selectCurationProcessBySeq(List<CurationVO> curationList) throws Exception {
		return curationMapper.selectCurationProcessBySeq(curationList);
	}

	@Override
	public int deleteCurationProcess(int seq) throws Exception {
		return curationMapper.deleteCurationProcess(seq);
	}

	@Override
	public int updateCurationOrder(HashMap<String, Object> map) throws Exception {
		return curationMapper.updateCurationOrder(map);
	}

}
