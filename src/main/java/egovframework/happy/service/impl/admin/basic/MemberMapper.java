package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("memberMapper")
public interface MemberMapper {
	
	/**
	 * <pre>
	 * 검색결과에 따른 회원현황의 리스트
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method selectList
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<MemberVO> selectList(HashMap<String, Object> map) throws Exception;	//출력
	int searchCnt(HashMap<String, Object> map) throws Exception;	//검색된 결과 수

	/**
	 * <pre>
	 * 회원현황의 총 과정 수
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method memberTotCnt
	 * @return
	 * @throws Exception 
	 */
	int memberTotCnt() throws Exception;	//전체수
	/**
	 * <pre>
	 * 회원의 전체 리스트 출력
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method list
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<MemberVO> list(HashMap<String, Object> map) throws Exception;	//default 출력
	
	/**
	 * <pre>
	 * 해당 회원이 개설한 과정 보기
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method selectProcessById
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<ProcessDTO> selectProcessById(HashMap<String, Object> map) throws Exception;
	/**
	 * <pre>
	 * 해당 회원의 수강한 과정 보기
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method selectTakeLectureById
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<TakeLectureVO> selectTakeLectureById(HashMap<String, Object> map) throws Exception;
	/**
	 * <pre>
	 * 신규회원의 수
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method memJoinNewCnt
	 * @return
	 * @throws Exception 
	 */
	int memJoinNewCnt() throws Exception;
	/**
	 * <pre>
	 * (휴넷)기존회원의 수
	 * </pre>
	 * @Class MemberMapper.java
	 * @Method memJoinOldCnt
	 * @return
	 * @throws Exception 
	 */
	int memJoinOldCnt() throws Exception;
}
