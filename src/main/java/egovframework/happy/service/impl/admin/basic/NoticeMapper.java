package egovframework.happy.service.impl.admin.basic;

import java.util.HashMap;
import java.util.List;

import egovframework.happy.service.client.vo.NoticeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("noticeMapper")
public interface NoticeMapper {
	
	/**
	 * <pre>
	 * 공지사항 등록
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method insertNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int insertNotice(NoticeVO notice) throws Exception;
	/**
	 * <pre>
	 * 공지사항 삭제
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method deleteNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int deleteNotice(NoticeVO notice) throws Exception;
	/**
	 * <pre>
	 * 공지사항 수정
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method updateNotice
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int updateNotice(NoticeVO notice) throws Exception;
	/**
	 * <pre>
	 * 공지사항 공개여부 수정
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method updateOpenInfo
	 * @param notice
	 * @return
	 * @throws Exception 
	 */
	int updateOpenInfo(NoticeVO notice) throws Exception;
	/**
	 * <pre>
	 * 공지사항 전체 수
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method selectNoticeListTotCnt
	 * @return
	 * @throws Exception 
	 */
	int selectNoticeListTotCnt() throws Exception;
	/**
	 * <pre>
	 * 공지사항 자세히 보기
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method selectNoticeDetailBySeq
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	NoticeVO selectNoticeDetailBySeq(int id) throws Exception;
	/**
	 * <pre>
	 * 공지사항 리스트 출력
	 * </pre>
	 * @Class NoticeMapper.java
	 * @Method list
	 * @param map
	 * @return
	 * @throws Exception 
	 */
	List<NoticeVO> list(HashMap<String, Integer> map) throws Exception;
}
