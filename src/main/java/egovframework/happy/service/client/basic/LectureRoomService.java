package egovframework.happy.service.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;

public interface LectureRoomService {
	List<?> selectTakeLectureList(TakeLectureVO vo) throws Exception;
	TakeLectureVO selectTakeLecture(TakeLectureVO vo) throws Exception;
	int insertTakeLectureReview(TakeLectureReviewVO vo) throws Exception;
}
