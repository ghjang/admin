package egovframework.happy.service.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.FileVO;

public interface CommonService {
	List<?> selectCode(String s) throws Exception;
	
	int insertFileInfo(FileVO vo) throws Exception;
	int deleteFileInfo(FileVO vo) throws Exception;
	//List<?> selectFileList(FileVO vo) throws Exception;
	FileVO selectFileinfo(int seq) throws Exception;
}
