package egovframework.happy.service.client.basic;

import egovframework.happy.service.client.vo.EmployeeVO;

/**
 * <pre>
 * 세션 service interface
 * </pre>
 * @Class Name : SessionService.java
 * @Description : SessionService Class
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 1.
 * @version 1.0
 */
public interface SessionService {
	/**
	 * <pre>
	 * 로그인 체크 - 휴넷 API
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : userLoginCheck
	 * @param url
	 * @param userId
	 * @param accessKey
	 * @return
	 * @throws Exception
	 */
	String userLoginCheck(String url, String userId, String accessKey) throws Exception;
	/**
	 * <pre>
	 * 로그아웃 - 휴넷 API
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : userLogOut
	 * @param url
	 * @param userId
	 * @param accessKey
	 * @return
	 * @throws Exception
	 */
	String userLogOut(String url, String userId, String accessKey) throws Exception;
	/**
	 * <pre>
	 * 회원정보 조회 - 휴넷 API
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : getUserInfo
	 * @param url
	 * @param userId
	 * @param accessKey
	 * @return
	 * @throws Exception
	 */
	String getUserInfo(String url, String userId, String accessKey) throws Exception;
	
	/**
	 * <pre>
	 * 회원정보 조회
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : selectEmployee
	 * @param User_Id
	 * @return
	 * @throws Exception
	 */
	EmployeeVO selectEmployee(String User_Id) throws Exception;
	/**
	 * <pre>
	 * 회원정보 등록
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : insertEmployee
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int insertEmployee(EmployeeVO vo) throws Exception;
	/**
	 * <pre>
	 * 회원정보 수정
	 * </pre>
	 * @Class Name : SessionService
	 * @Method Name : updateEmployee
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int updateEmployee(EmployeeVO vo) throws Exception;
}
