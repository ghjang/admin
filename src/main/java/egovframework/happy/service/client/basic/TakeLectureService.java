package egovframework.happy.service.client.basic;

import java.util.List;

import egovframework.happy.service.client.vo.TakeLectureReviewTailVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;

public interface TakeLectureService {
	/**
	 * 수강후기
	 * @param vo
	 * @return	
	 * @throws Exception
	 */
	int selectTakeLectureReviewListCount(TakeLectureReviewVO vo) throws Exception;
	List<?> selectTakeLectureReviewList(TakeLectureReviewVO vo) throws Exception;
	List<?> selectTakeLectureReviewTailList(TakeLectureReviewTailVO vo) throws Exception;
	TakeLectureReviewTailVO selectTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception;
	int insertTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception;
	int updateTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception;
	int deleteTakeLectureReviewTail(TakeLectureReviewTailVO vo) throws Exception;
	
	/**
	 * 수강자 목록 조회
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	int selectTakeLectureListCount(TakeLectureVO vo) throws Exception;
	List<?> selectTakeLectureList(TakeLectureVO vo) throws Exception;
}
