package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AdminInfoVO extends CommonVO implements Serializable {
	
	public String User_Id;
	public String User_Nm;
	public String accessKey;
	
	public AdminInfoVO() throws Exception {
		super();
//		this.User_Nm = "TOTO";
//		System.out.println("값 id : " + User_Id + " / key : " + accessKey);
	}

	public String getUser_Id() {
		return User_Id;
	}

	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}

	public String getUser_Nm() {
		return User_Nm;
	}

	public void setUser_Nm(String user_Nm) {
		User_Nm = user_Nm;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	
	
}
