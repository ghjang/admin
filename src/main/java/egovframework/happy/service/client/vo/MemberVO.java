package egovframework.happy.service.client.vo;

import java.io.Serializable;

/**
 * @author F1-PC
 *
 */
/**
 * @author F1-PC
 *
 */
@SuppressWarnings("serial")
public class MemberVO extends CommonVO implements Serializable {
	public MemberVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int User_Seq;			//회원 일련번호 17.07.12
	private String User_Id;			//회원아이디
	private String User_Code;		//회원코드
	private String Nickname;		//닉네임
	private String Email;			//이메일
	private int Profile_Image;		//프로필이미지
	private String Join_Path_Code;	//가입경로코드
	private String Join_Path_Url;	//가입경로URL
	
	private int TakeLecture_OnGoing_Count;	//수강하는 과정 총 수
	private int TakeLecture_Studying_Count;	//수강 과정 진행중 수
	private int TakeLecture_Expire_Count;	//수강 과정 종료 수
	
	private int Process_Count;	//모든 개설과정 수
	private int Making_Process_Count;//개설과정 제작중 수
	private int Open_Process_Count;//개설과정 오픈 수
	
	private byte[] Mobile_No;	//암호화된 휴대폰 번호
	private String Mobile;		//복호화된 휴대폰 번호
	private String Email_Yn;//이메일 수신 동의
	private String Sms_Yn;//SMS 수신 동의
	
	
	public int getUser_Seq() {
		return User_Seq;
	}
	public void setUser_Seq(int user_Seq) {
		User_Seq = user_Seq;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getJoin_Path_Url() {
		return Join_Path_Url;
	}
	public void setJoin_Path_Url(String join_Path_Url) {
		Join_Path_Url = join_Path_Url;
	}
	public int getTakeLecture_OnGoing_Count() {
		return TakeLecture_OnGoing_Count;
	}
	public void setTakeLecture_OnGoing_Count(int takeLecture_OnGoing_Count) {
		TakeLecture_OnGoing_Count = takeLecture_OnGoing_Count;
	}
	public int getProcess_Count() {
		return Process_Count;
	}
	public void setProcess_Count(int process_Count) {
		Process_Count = process_Count;
	}
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	public String getUser_Code() {
		return User_Code;
	}
	public void setUser_Code(String user_Code) {
		User_Code = user_Code;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getJoin_Path_Code() {
		return Join_Path_Code;
	}
	public void setJoin_Path_Code(String join_Path_Code) {
		Join_Path_Code = join_Path_Code;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getProfile_Image() {
		return Profile_Image;
	}
	public void setProfile_Image(int profile_Image) {
		Profile_Image = profile_Image;
	}
	public int getTakeLecture_Studying_Count() {
		return TakeLecture_Studying_Count;
	}
	public void setTakeLecture_Studying_Count(int takeLecture_Studying_Count) {
		TakeLecture_Studying_Count = takeLecture_Studying_Count;
	}
	public int getTakeLecture_Expire_Count() {
		return TakeLecture_Expire_Count;
	}
	public void setTakeLecture_Expire_Count(int takeLecture_Expire_Count) {
		TakeLecture_Expire_Count = takeLecture_Expire_Count;
	}
	public int getMaking_Process_Count() {
		return Making_Process_Count;
	}
	public void setMaking_Process_Count(int making_Process_Count) {
		Making_Process_Count = making_Process_Count;
	}
	public int getOpen_Process_Count() {
		return Open_Process_Count;
	}
	public void setOpen_Process_Count(int open_Process_Count) {
		Open_Process_Count = open_Process_Count;
	}
	public byte[] getMobile_No() {
		return Mobile_No;
	}
	public void setMobile_No(byte[] mobile_No) {
		Mobile_No = mobile_No;
	}
	public String getEmail_Yn() {
		return Email_Yn;
	}
	public void setEmail_Yn(String email_Yn) {
		Email_Yn = email_Yn;
	}
	public String getSms_Yn() {
		return Sms_Yn;
	}
	public void setSms_Yn(String sms_Yn) {
		Sms_Yn = sms_Yn;
	}
}
