package egovframework.happy.service.client.vo;

import egovframework.happy.cmmn.SessionUtil;

/**
 * <pre>
 * </pre>
 * @Class SessionAdminVO.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 14.
 * @version 1.0
 */
public class SessionAdminVO {
	private String Log_User_Id;
	private String Log_User_Nm;
	private String Log_Ip;
	private String Log_Access_Device;
	private String Log_User_Agent;
	
	public SessionAdminVO() throws Exception {
		AdminInfoVO vo = (AdminInfoVO) SessionUtil.getAttribute("ss_info");
		if(vo != null) {
			this.Log_User_Id		= vo.getUser_Id();
			this.Log_User_Nm		= vo.getUser_Nm();
		}
		this.Log_Ip				= (String) SessionUtil.getAttribute("ip");
		this.Log_Access_Device	= (String) SessionUtil.getAttribute("access_type");
		this.Log_User_Agent		= (String) SessionUtil.getAttribute("user_agent");
	}

	public String getLog_User_Id() {
		return Log_User_Id;
	}

	public void setLog_User_Id(String log_User_Id) {
		Log_User_Id = log_User_Id;
	}

	public String getLog_User_Nm() {
		return Log_User_Nm;
	}

	public void setLog_User_Nm(String log_User_Nm) {
		Log_User_Nm = log_User_Nm;
	}

	public String getLog_Ip() {
		return Log_Ip;
	}

	public void setLog_Ip(String log_Ip) {
		Log_Ip = log_Ip;
	}

	public String getLog_Access_Device() {
		return Log_Access_Device;
	}

	public void setLog_Access_Device(String log_Access_Device) {
		Log_Access_Device = log_Access_Device;
	}

	public String getLog_User_Agent() {
		return Log_User_Agent;
	}

	public void setLog_User_Agent(String log_User_Agent) {
		Log_User_Agent = log_User_Agent;
	}

}
