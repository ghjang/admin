package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class TakeLectureVO extends CommonVO implements Serializable {
	public TakeLectureVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Limit;
	private int TakeLecture_Seq;	//수강일련번호
	private String Process_Code;	//과정코드
	private String User_Id;	//회원아이디
	private String Nickname;
	private String Profile_Img_Url;	//회원사진
	private String TakeLecture_Complete_Yn;	//수강완료여부
	private int Progress_Rate;	//진도율
	private Timestamp TakeLecture_Start_Date;	//수강시작일시
	private Timestamp TakeLecture_End_Date;	//수강종료일시
	private String TakeLecture_Possible_Yn;	//수강가능여부
	private String TakeLecture_Possible_Date_Msg; //학습가능일자메시지
	private String Process_Nm;
	private String Process_Image_Url;
	private String Professor_Nm;
	private String Professor_Image_Url;
	private double Grade;
	private int Grade_Count;
	private int My_Grade_Count;
	
	public int getLimit() {
		return Limit;
	}
	public void setLimit(int limit) {
		Limit = limit;
	}
	public int getTakeLecture_Seq() {
		return TakeLecture_Seq;
	}
	public void setTakeLecture_Seq(int takeLecture_Seq) {
		TakeLecture_Seq = takeLecture_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getProfile_Img_Url() {
		return Profile_Img_Url;
	}
	public void setProfile_Img_Url(String profile_Img_Url) {
		Profile_Img_Url = profile_Img_Url;
	}
	public String getTakeLecture_Complete_Yn() {
		return TakeLecture_Complete_Yn;
	}
	public void setTakeLecture_Complete_Yn(String takeLecture_Complete_Yn) {
		TakeLecture_Complete_Yn = takeLecture_Complete_Yn;
	}
	public int getProgress_Rate() {
		return Progress_Rate;
	}
	public void setProgress_Rate(int progress_Rate) {
		Progress_Rate = progress_Rate;
	}
	public Timestamp getTakeLecture_Start_Date() {
		return TakeLecture_Start_Date;
	}
	public void setTakeLecture_Start_Date(Timestamp takeLecture_Start_Date) {
		TakeLecture_Start_Date = takeLecture_Start_Date;
	}
	public Timestamp getTakeLecture_End_Date() {
		return TakeLecture_End_Date;
	}
	public void setTakeLecture_End_Date(Timestamp takeLecture_End_Date) {
		TakeLecture_End_Date = takeLecture_End_Date;
	}
	public String getTakeLecture_Possible_Yn() {
		return TakeLecture_Possible_Yn;
	}
	public void setTakeLecture_Possible_Yn(String takeLecture_Possible_Yn) {
		TakeLecture_Possible_Yn = takeLecture_Possible_Yn;
	}
	public String getTakeLecture_Possible_Date_Msg() {
		return TakeLecture_Possible_Date_Msg;
	}
	public void setTakeLecture_Possible_Date_Msg(String takeLecture_Possible_Date_Msg) {
		TakeLecture_Possible_Date_Msg = takeLecture_Possible_Date_Msg;
	}
	public String getProcess_Nm() {
		return Process_Nm;
	}
	public void setProcess_Nm(String process_Nm) {
		Process_Nm = process_Nm;
	}
	public String getProcess_Image_Url() {
		return Process_Image_Url;
	}
	public void setProcess_Image_Url(String process_Image_Url) {
		Process_Image_Url = process_Image_Url;
	}
	public String getProfessor_Nm() {
		return Professor_Nm;
	}
	public void setProfessor_Nm(String professor_Nm) {
		Professor_Nm = professor_Nm;
	}
	public String getProfessor_Image_Url() {
		return Professor_Image_Url;
	}
	public void setProfessor_Image_Url(String professor_Image_Url) {
		Professor_Image_Url = professor_Image_Url;
	}
	public double getGrade() {
		return Grade;
	}
	public void setGrade(double grade) {
		Grade = grade;
	}
	public int getGrade_Count() {
		return Grade_Count;
	}
	public void setGrade_Count(int grade_Count) {
		Grade_Count = grade_Count;
	}
	public int getMy_Grade_Count() {
		return My_Grade_Count;
	}
	public void setMy_Grade_Count(int my_Grade_Count) {
		My_Grade_Count = my_Grade_Count;
	}
}
