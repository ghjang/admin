package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import egovframework.happy.service.client.vo.CommonVO;

@SuppressWarnings("serial")
public class ProcessDTO extends CommonVO implements Serializable {

	public ProcessDTO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private String Process_Code;			//과정코드
	private String Process_Nm;				//과정명
	private int Process_Image;				//과정이미지
	private String Field_Code;				//분야코드
	private String Process_Introduction;	//과정소개
	private int Sample_Lecture_Seq;			//샘플강의일련번호
	private String Free_Yn;					//무료여부
	private int Price;						//가격
	private String Study_Period;			//학습기간
	private String Open_Yn;
	private String Public_Yn;					//공개여부
	private String Public_Yn_Admin_Edit_Yn;	//공개여부관리자수정여부
	private String Block_Yn;				//차단여부
	private String Block_Reason;			//차단사유
	private Timestamp Block_Date;			//차단일시
	private int Facebook_Share_Count;		//페이스북공유수
	private int Kakaotalk_Share_Count;		//카카오톡공유수
	private int Letters_Share_Count;		//문자공유수
	private int Email_Share_Count;			//이메일공유수
	private int Process_QnA_Count;			//과정QnA수
	private int Process_Total_Grade;		//과정총평점
	private int Process_Evalationer_Count;	//과정평가자수
	private Timestamp Process_Entry_Date;	//과정진입일시
	private Timestamp Process_Open_Date;	//과정공개일시
	
	private float Grade;		//총 점수/사람수
	private String Nickname;		//닉네임
	private String Email;		//Email
	private int TakeLecture_Review_Count;	//수강후기수
	private int TakeLecture_OnGoing_Count;	//전체수강자수
	private int TakeLecture_Studying_Count;	//학습 중 수강자수
	private int TakeLecture_Expire_Count;	//기간종료 수강자수
	private int Tag_Count;
	
	
	
	public String getOpen_Yn() {
		return Open_Yn;
	}
	public void setOpen_Yn(String open_Yn) {
		Open_Yn = open_Yn;
	}
	public float getGrade() {
		return Grade;
	}
	public void setGrade(float grade) {
		Grade = grade;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getTag_Count() {
		return Tag_Count;
	}
	public void setTag_Count(int tag_Count) {
		Tag_Count = tag_Count;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getProcess_Nm() {
		return Process_Nm;
	}
	public void setProcess_Nm(String process_Nm) {
		Process_Nm = process_Nm;
	}
	public int getProcess_Image() {
		return Process_Image;
	}
	public void setProcess_Image(int process_Image) {
		Process_Image = process_Image;
	}
	public String getField_Code() {
		return Field_Code;
	}
	public void setField_Code(String field_Code) {
		Field_Code = field_Code;
	}
	public String getProcess_Introduction() {
		return Process_Introduction;
	}
	public void setProcess_Introduction(String process_Introduction) {
		Process_Introduction = process_Introduction;
	}
	public int getSample_Lecture_Seq() {
		return Sample_Lecture_Seq;
	}
	public void setSample_Lecture_Seq(int sample_Lecture_Seq) {
		Sample_Lecture_Seq = sample_Lecture_Seq;
	}
	public String getFree_Yn() {
		return Free_Yn;
	}
	public void setFree_Yn(String free_Yn) {
		Free_Yn = free_Yn;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	public String getStudy_Period() {
		return Study_Period;
	}
	public void setStudy_Period(String study_Period) {
		Study_Period = study_Period;
	}
	
	public String getBlock_Yn() {
		return Block_Yn;
	}
	public void setBlock_Yn(String block_Yn) {
		Block_Yn = block_Yn;
	}
	public String getBlock_Reason() {
		return Block_Reason;
	}
	public void setBlock_Reason(String block_Reason) {
		Block_Reason = block_Reason;
	}
	public Timestamp getBlock_Date() {
		return Block_Date;
	}
	public void setBlock_Date(Timestamp block_Date) {
		Block_Date = block_Date;
	}
	public int getFacebook_Share_Count() {
		return Facebook_Share_Count;
	}
	public void setFacebook_Share_Count(int facebook_Share_Count) {
		Facebook_Share_Count = facebook_Share_Count;
	}
	public int getKakaotalk_Share_Count() {
		return Kakaotalk_Share_Count;
	}
	public void setKakaotalk_Share_Count(int kakaotalk_Share_Count) {
		Kakaotalk_Share_Count = kakaotalk_Share_Count;
	}
	public int getLetters_Share_Count() {
		return Letters_Share_Count;
	}
	public void setLetters_Share_Count(int letters_Share_Count) {
		Letters_Share_Count = letters_Share_Count;
	}
	public int getEmail_Share_Count() {
		return Email_Share_Count;
	}
	public void setEmail_Share_Count(int email_Share_Count) {
		Email_Share_Count = email_Share_Count;
	}
	public int getProcess_QnA_Count() {
		return Process_QnA_Count;
	}
	public void setProcess_QnA_Count(int process_QnA_Count) {
		Process_QnA_Count = process_QnA_Count;
	}
	public int getProcess_Total_Grade() {
		return Process_Total_Grade;
	}
	public void setProcess_Total_Grade(int process_Total_Grade) {
		Process_Total_Grade = process_Total_Grade;
	}
	public int getProcess_Evalationer_Count() {
		return Process_Evalationer_Count;
	}
	public void setProcess_Evalationer_Count(int process_Evalationer_Count) {
		Process_Evalationer_Count = process_Evalationer_Count;
	}
	public Timestamp getProcess_Entry_Date() {
		return Process_Entry_Date;
	}
	public void setProcess_Entry_Date(Timestamp process_Entry_Date) {
		Process_Entry_Date = process_Entry_Date;
	}
	public Timestamp getProcess_Open_Date() {
		return Process_Open_Date;
	}
	public void setProcess_Open_Date(Timestamp process_Open_Date) {
		Process_Open_Date = process_Open_Date;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public int getTakeLecture_Review_Count() {
		return TakeLecture_Review_Count;
	}
	public void setTakeLecture_Review_Count(int takeLecture_Review_Count) {
		TakeLecture_Review_Count = takeLecture_Review_Count;
	}
	public int getTakeLecture_OnGoing_Count() {
		return TakeLecture_OnGoing_Count;
	}
	public void setTakeLecture_OnGoing_Count(int takeLecture_OnGoing_Count) {
		TakeLecture_OnGoing_Count = takeLecture_OnGoing_Count;
	}
	public int getTakeLecture_Studying_Count() {
		return TakeLecture_Studying_Count;
	}
	public void setTakeLecture_Studying_Count(int takeLecture_Studying_Count) {
		TakeLecture_Studying_Count = takeLecture_Studying_Count;
	}
	public int getTakeLecture_Expire_Count() {
		return TakeLecture_Expire_Count;
	}
	public void setTakeLecture_Expire_Count(int takeLecture_Expire_Count) {
		TakeLecture_Expire_Count = takeLecture_Expire_Count;
	}
	public String getPublic_Yn() {
		return Public_Yn;
	}
	public void setPublic_Yn(String public_Yn) {
		Public_Yn = public_Yn;
	}
	public String getPublic_Yn_Admin_Edit_Yn() {
		return Public_Yn_Admin_Edit_Yn;
	}
	public void setPublic_Yn_Admin_Edit_Yn(String public_Yn_Admin_Edit_Yn) {
		Public_Yn_Admin_Edit_Yn = public_Yn_Admin_Edit_Yn;
	}
	
}
