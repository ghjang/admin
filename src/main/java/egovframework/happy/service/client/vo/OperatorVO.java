package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class OperatorVO extends CommonVO implements Serializable {
	public OperatorVO() throws Exception {
		super();
	}
	
	private int Admin_Seq;				// 운영자 일련번호
	private String User_Id;				// 회원 아이디
	private String Nickname;			// 운영자 닉네임 - 이부분은 원래 EmployeeVo에 있어야됨
	private String Process_Code;		// 과정 코드
	private String Open_Yn;				// 공개 여부
	//Common - Regist_Id / Regist_IP / Regist_Date / Edit_Id / Edit_Ip / Edit_Date
	
	public int getAdmin_Seq() {
		return Admin_Seq;
	}
	public void setAdmin_Seq(int admin_Seq) {
		Admin_Seq = admin_Seq;
	}
	
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	
	public String getOpen_Yn() {
		return Open_Yn;
	}
	public void setOpen_Yn(String open_Yn) {
		Open_Yn = open_Yn;
	}

}
