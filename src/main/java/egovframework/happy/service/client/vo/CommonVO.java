package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@SuppressWarnings("serial")
public class CommonVO extends SessionAdminVO implements Serializable {
	public CommonVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	private String redirect;
	private String Regist_Id;//	등록아이디
	private String Regist_Profile_Img_Url;	//등록자 프로파일 이미지
	private String Regist_Id_Nm;	//등록자 명
	private String Regist_Ip;// 등록아이피
	private Timestamp Regist_Date;// 등록일시
	private String Edit_Id;// 수정아이디
	private String Edit_Ip;// 수정아이피
	private Timestamp Edit_Date;// 수정일시
	
	public String toString() {
		//return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public String getRegist_Id() {
		return Regist_Id;
	}

	public void setRegist_Id(String regist_Id) {
		Regist_Id = regist_Id;
	}

	public String getRegist_Ip() {
		return Regist_Ip;
	}

	public String getRegist_Profile_Img_Url() {
		return Regist_Profile_Img_Url;
	}

	public void setRegist_Profile_Img_Url(String regist_Profile_Img_Url) {
		Regist_Profile_Img_Url = regist_Profile_Img_Url;
	}

	public String getRegist_Id_Nm() {
		return Regist_Id_Nm;
	}

	public void setRegist_Id_Nm(String regist_Id_Nm) {
		Regist_Id_Nm = regist_Id_Nm;
	}

	public void setRegist_Ip(String regist_Ip) {
		Regist_Ip = regist_Ip;
	}

	public Timestamp getRegist_Date() {
		return Regist_Date;
	}

	public void setRegist_Date(Timestamp regist_Date) {
		Regist_Date = regist_Date;
	}

	public String getEdit_Id() {
		return Edit_Id;
	}

	public void setEdit_Id(String edit_Id) {
		Edit_Id = edit_Id;
	}

	public String getEdit_Ip() {
		return Edit_Ip;
	}

	public void setEdit_Ip(String edit_Ip) {
		Edit_Ip = edit_Ip;
	}

	public Timestamp getEdit_Date() {
		return Edit_Date;
	}

	public void setEdit_Date(Timestamp edit_Date) {
		Edit_Date = edit_Date;
	}
}
