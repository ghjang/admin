package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class QnaAnswerVO extends CommonVO implements Serializable {
	public QnaAnswerVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Process_QnA_Answer_Seq;	//과정Q&A답변일련번호
	private int Process_QnA_Seq;	//과정Q&A일련번호
	private String Process_Code;	//과정코드
	private String Answer_Content;	//댓글내용
	private String Profile_Img_Url;
	private String Nickname;
	private String Delete_Yn;	//삭제여부
	private List<QnaAnswerReplyVO> replyVOList;
	
	public int getProcess_QnA_Answer_Seq() {
		return Process_QnA_Answer_Seq;
	}
	public void setProcess_QnA_Answer_Seq(int process_QnA_Answer_Seq) {
		Process_QnA_Answer_Seq = process_QnA_Answer_Seq;
	}
	public int getProcess_QnA_Seq() {
		return Process_QnA_Seq;
	}
	public void setProcess_QnA_Seq(int process_QnA_Seq) {
		Process_QnA_Seq = process_QnA_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getAnswer_Content() {
		return Answer_Content;
	}
	public void setAnswer_Content(String answer_Content) {
		Answer_Content = answer_Content;
	}
	public String getProfile_Img_Url() {
		return Profile_Img_Url;
	}
	public void setProfile_Img_Url(String profile_Img_Url) {
		Profile_Img_Url = profile_Img_Url;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
	public List<QnaAnswerReplyVO> getReplyVOList() {
		return replyVOList;
	}
	public void setReplyVOList(List<QnaAnswerReplyVO> replyVOList) {
		this.replyVOList = replyVOList;
	}
}
