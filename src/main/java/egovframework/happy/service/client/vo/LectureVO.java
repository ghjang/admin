package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LectureVO extends CommonVO implements Serializable {
	public LectureVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private String Tmp_Chapter_Index = "";// 챕터인덱스 - 화면에서 컨트롤러 전달 시 강의랑 매핑용
	private String Tmp_Lecture_Index = "";// 강의인덱스 - 화면에서 컨트롤러 전달 시 챕터랑 매핑용
	private int Lecture_Seq = 0;// 강의일련번호
	private String multi_Lecture_Seq = "";// 강의일련번호
	private int Chapter_Seq = 0;// 챕터일련번호
	private String Process_Code = "";// 과정코드
	private int Lecture_Index = 0;// 강의인덱스 - 화면에서 컨트롤러 전달 시 챕터랑 매핑용
	private String Lecture_Nm = "";// 강의명
	private String Lecture_Url = "";// 강의URL
	private int Lecture_Time = 0;// 재생시간
	private String Tmp_Lecture_Time;// 재생시간
	private String Delete_Yn = "";// 삭제여부
	private String Sample_Yn;//	샘플강의여부
	
	public String getTmp_Chapter_Index() {
		return Tmp_Chapter_Index;
	}
	public void setTmp_Chapter_Index(String tmp_Chapter_Index) {
		Tmp_Chapter_Index = tmp_Chapter_Index;
	}
	public String getTmp_Lecture_Index() {
		return Tmp_Lecture_Index;
	}
	public void setTmp_Lecture_Index(String tmp_Lecture_Index) {
		Tmp_Lecture_Index = tmp_Lecture_Index;
	}
	public int getLecture_Seq() {
		return Lecture_Seq;
	}
	public void setLecture_Seq(int lecture_Seq) {
		Lecture_Seq = lecture_Seq;
	}
	public int getChapter_Seq() {
		return Chapter_Seq;
	}
	public void setChapter_Seq(int chapter_Seq) {
		Chapter_Seq = chapter_Seq;
	}
	public String getMulti_Lecture_Seq() {
		return multi_Lecture_Seq;
	}
	public void setMulti_Lecture_Seq(String multi_Lecture_Seq) {
		this.multi_Lecture_Seq = multi_Lecture_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public int getLecture_Index() {
		return Lecture_Index;
	}
	public void setLecture_Index(int lecture_Index) {
		Lecture_Index = lecture_Index;
	}
	public String getLecture_Nm() {
		return Lecture_Nm;
	}
	public void setLecture_Nm(String lecture_Nm) {
		Lecture_Nm = lecture_Nm;
	}
	public String getLecture_Url() {
		return Lecture_Url;
	}
	public void setLecture_Url(String lecture_Url) {
		Lecture_Url = lecture_Url;
	}
	public int getLecture_Time() {
		return Lecture_Time;
	}
	public void setLecture_Time(int lecture_Time) {
		Lecture_Time = lecture_Time;
	}
	public String getTmp_Lecture_Time() {
		return Tmp_Lecture_Time;
	}
	public void setTmp_Lecture_Time(String tmp_Lecture_Time) {
		Tmp_Lecture_Time = tmp_Lecture_Time;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
	public String getSample_Yn() {
		return Sample_Yn;
	}
	public void setSample_Yn(String sample_Yn) {
		Sample_Yn = sample_Yn;
	}
}
