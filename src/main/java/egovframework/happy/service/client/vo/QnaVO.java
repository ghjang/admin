package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class QnaVO extends CommonVO implements Serializable {
	public QnaVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Limit;
	private int Process_QnA_Seq;	//과정Q&A일련번호
	private String Process_Code;	//과정코드
	private String Title;	//제목
	private String Content;	//내용
	private int Recommend;	//추천
	private int My_Recommend;	//추천
	private int Read_Count;	//조회수
	private String Delete_Yn;	//삭제여부
	private String User_Id;
	private int Answer_Count;
	private int TakeLecture_Seq;	//수강일련번호
	
	public int getLimit() {
		return Limit;
	}
	public void setLimit(int limit) {
		Limit = limit;
	}
	public int getProcess_QnA_Seq() {
		return Process_QnA_Seq;
	}
	public void setProcess_QnA_Seq(int process_QnA_Seq) {
		Process_QnA_Seq = process_QnA_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public int getRecommend() {
		return Recommend;
	}
	public void setRecommend(int recommend) {
		Recommend = recommend;
	}
	public int getMy_Recommend() {
		return My_Recommend;
	}
	public void setMy_Recommend(int my_Recommend) {
		My_Recommend = my_Recommend;
	}
	public int getRead_Count() {
		return Read_Count;
	}
	public void setRead_Count(int read_Count) {
		Read_Count = read_Count;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	public int getAnswer_Count() {
		return Answer_Count;
	}
	public void setAnswer_Count(int answer_Count) {
		Answer_Count = answer_Count;
	}
	public int getTakeLecture_Seq() {
		return TakeLecture_Seq;
	}
	public void setTakeLecture_Seq(int takeLecture_Seq) {
		TakeLecture_Seq = takeLecture_Seq;
	}
}
