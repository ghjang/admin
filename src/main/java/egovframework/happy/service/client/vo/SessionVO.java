package egovframework.happy.service.client.vo;

import egovframework.happy.cmmn.SessionUtil;

public class SessionVO {
	private String Log_User_Id;
	private String Log_User_Nm;
	private String Log_Ip;
	private String Log_Access_Device;
	private String Log_User_Agent;
	
	public SessionVO() throws Exception {
		EmployeeVO vo = (EmployeeVO) SessionUtil.getAttribute("ss_info");
		if(vo != null) {
			this.Log_User_Id		= vo.getUser_Id();
			this.Log_User_Nm		= vo.getNickname();
		}
		this.Log_Ip				= (String) SessionUtil.getAttribute("ip");
		this.Log_Access_Device	= (String) SessionUtil.getAttribute("access_type");
		this.Log_User_Agent		= (String) SessionUtil.getAttribute("user_agent");
//		this.Admin_Yn			= (String) SessionUtil.getAttribute("admin_yn");
//		this.Qna_Open_Yn		= (String) SessionUtil.getAttribute("qna_open_yn");
	}
	
	public String getLog_User_Id() {
		return Log_User_Id;
	}

	public void setLog_User_Id(String log_user_id) {
		Log_User_Id = log_user_id;
	}

	public String getLog_User_Nm() {
		return Log_User_Nm;
	}

	public void setLog_User_Nm(String log_user_nm) {
		Log_User_Nm = log_user_nm;
	}

	public String getLog_Ip() {
		return Log_Ip;
	}

	public void setLog_Ip(String log_ip) {
		Log_Ip = log_ip;
	}

	public String getLog_Access_Device() {
		return Log_Access_Device;
	}

	public void setLog_Access_Device(String log_access_device) {
		Log_Access_Device = log_access_device;
	}

	public String getLog_User_Agent() {
		return Log_User_Agent;
	}

	public void setLog_User_Agent(String log_user_agent) {
		Log_User_Agent = log_user_agent;
	}

}
