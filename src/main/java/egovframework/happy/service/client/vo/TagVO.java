package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TagVO extends CommonVO implements Serializable {
	public TagVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Tag_Seq = 0;// 태그일련번호
	private String Process_Code = "";// 과정코드
	private String Tag = "";// 태그
	
	public int getTag_Seq() {
		return Tag_Seq;
	}
	public void setTag_Seq(int tag_Seq) {
		Tag_Seq = tag_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getTag() {
		return Tag;
	}
	public void setTag(String tag) {
		Tag = tag;
	}

}
