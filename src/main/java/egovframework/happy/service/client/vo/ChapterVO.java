package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChapterVO extends CommonVO implements Serializable {
	public ChapterVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private String Tmp_Chapter_Index = "";// 챕터인덱스 - 화면에서 컨트롤러 전달 시 강의랑 매핑용
	private int Chapter_Seq = 0;// 챕터일련번호
	private String multi_Chapter_Seq = "";
	private String Process_Code = "";// 과정코드
	private int Chapter_Index = 0;// 챕터인덱스 - 화면에서 컨트롤러 전달 시 강의랑 매핑용
	private String Chapter_Nm = "";// 챕터명
	private String Delete_Yn = "";// 삭제여부
	
	public String getTmp_Chapter_Index() {
		return Tmp_Chapter_Index;
	}
	public void setTmp_Chapter_Index(String tmp_Chapter_Index) {
		Tmp_Chapter_Index = tmp_Chapter_Index;
	}
	public int getChapter_Seq() {
		return Chapter_Seq;
	}
	public void setChapter_Seq(int chapter_Seq) {
		Chapter_Seq = chapter_Seq;
	}
	public String getMulti_Chapter_Seq() {
		return multi_Chapter_Seq;
	}
	public void setMulti_Chapter_Seq(String multi_Chapter_Seq) {
		this.multi_Chapter_Seq = multi_Chapter_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public int getChapter_Index() {
		return Chapter_Index;
	}
	public void setChapter_Index(int chapter_Index) {
		Chapter_Index = chapter_Index;
	}
	public String getChapter_Nm() {
		return Chapter_Nm;
	}
	public void setChapter_Nm(String chapter_Nm) {
		Chapter_Nm = chapter_Nm;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
}
