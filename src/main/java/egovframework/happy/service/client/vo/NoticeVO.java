package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NoticeVO extends CommonVO implements Serializable {
	public NoticeVO() throws Exception{
		super();
		// TODO Auto-generated constructor stub
	}
	
	private int Notice_Seq;	//공지사항 일련번호
	private String Title;	//제목
	private String Content;	//내용
	private int Image;	//공지사항 이미지
	private String Notice_Image_Url;	//공지사항 이미지 url
	private int Read_Count;	//조회수
	private String Open_Yn;	//공개여부
	private String Delete_Yn;//삭제여부
	public int getNotice_Seq() {
		return Notice_Seq;
	}
	public void setNotice_Seq(int notice_Seq) {
		Notice_Seq = notice_Seq;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public int getRead_Count() {
		return Read_Count;
	}
	public void setRead_Count(int read_Count) {
		Read_Count = read_Count;
	}
	public String getOpen_Yn() {
		return Open_Yn;
	}
	public void setOpen_Yn(String open_Yn) {
		Open_Yn = open_Yn;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
	public int getImage() {
		return Image;
	}
	public void setImage(int image) {
		Image = image;
	}
	public String getNotice_Image_Url() {
		return Notice_Image_Url;
	}
	public void setNotice_Image_Url(String notice_Image_Url) {
		Notice_Image_Url = notice_Image_Url;
	}

}
