package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class FileVO extends CommonVO implements Serializable {

	public FileVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Attach_File_Seq = 0;// 첨부파일일련번호
	private String Attach_File_Grp_Nm = "";// 첨부파일그룹명
	private String Save_Path = "";// 저장경로
	private String Original_File_Nm = "";// 원본파일명
	private String Save_File_Nm = "";// 저장파일명
	private String Ext_File_Nm = "";// 파일확장자
	private long File_Size = 0;// 파일사이즈
	private String Regist_Id = "";// 등록아이디
	private String Regist_Ip = "";// 등록아이피
	private Timestamp Regist_Date;// 등록일시
	
	public int getAttach_File_Seq() {
		return Attach_File_Seq;
	}
	public void setAttach_File_Seq(int attach_File_Seq) {
		Attach_File_Seq = attach_File_Seq;
	}
	public String getAttach_File_Grp_Nm() {
		return Attach_File_Grp_Nm;
	}
	public void setAttach_File_Grp_Nm(String attach_File_Grp_Nm) {
		Attach_File_Grp_Nm = attach_File_Grp_Nm;
	}
	public String getSave_Path() {
		return Save_Path;
	}
	public void setSave_Path(String save_Path) {
		Save_Path = save_Path;
	}
	public String getOriginal_File_Nm() {
		return Original_File_Nm;
	}
	public void setOriginal_File_Nm(String original_File_Nm) {
		Original_File_Nm = original_File_Nm;
	}
	public String getSave_File_Nm() {
		return Save_File_Nm;
	}
	public void setSave_File_Nm(String save_File_Nm) {
		Save_File_Nm = save_File_Nm;
	}
	public String getExt_File_Nm() {
		return Ext_File_Nm;
	}
	public void setExt_File_Nm(String ext_File_Nm) {
		Ext_File_Nm = ext_File_Nm;
	}
	public long getFile_Size() {
		return File_Size;
	}
	public void setFile_Size(long file_Size) {
		File_Size = file_Size;
	}
	public String getRegist_Id() {
		return Regist_Id;
	}
	public void setRegist_Id(String regist_Id) {
		Regist_Id = regist_Id;
	}
	public String getRegist_Ip() {
		return Regist_Ip;
	}
	public void setRegist_Ip(String regist_Ip) {
		Regist_Ip = regist_Ip;
	}
	public Timestamp getRegist_Date() {
		return Regist_Date;
	}
	public void setRegist_Date(Timestamp regist_Date) {
		Regist_Date = regist_Date;
	}
}
