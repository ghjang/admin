package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CodeVO extends CommonVO implements Serializable {
	public CodeVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private String Code_Grp = "";// 코드그룹명
	private String Code = "";// 코드
	private String Code_Nm = "";// 코드명
	
	public String getCode_Grp() {
		return Code_Grp;
	}
	public void setCode_Grp(String code_Grp) {
		Code_Grp = code_Grp;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getCode_Nm() {
		return Code_Nm;
	}
	public void setCode_Nm(String code_Nm) {
		Code_Nm = code_Nm;
	}
}
