package egovframework.happy.service.client.vo;

import java.io.Serializable;

import egovframework.happy.service.client.vo.CommonVO;

@SuppressWarnings("serial")
public class EmployeeVO extends CommonVO implements Serializable {
	public EmployeeVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private String User_Id;	//회원아이디
	private String User_Code;	//회원코드
	private String Nickname;	//닉네임
	private String Email;	//이메일
	private int Profile_Image;	//프로필이미지
	private String Join_Path_Code;	//가입경로코드
	private String accessKey;
	
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	public String getUser_Code() {
		return User_Code;
	}
	public void setUser_Code(String user_Code) {
		User_Code = user_Code;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getProfile_Image() {
		return Profile_Image;
	}
	public void setProfile_Image(int profile_Image) {
		Profile_Image = profile_Image;
	}
	public String getJoin_Path_Code() {
		return Join_Path_Code;
	}
	public void setJoin_Path_Code(String join_Path_Code) {
		Join_Path_Code = join_Path_Code;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
}
