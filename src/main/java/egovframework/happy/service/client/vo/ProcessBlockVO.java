package egovframework.happy.service.client.vo;

@SuppressWarnings("serial")
public class ProcessBlockVO extends CommonVO {

	public ProcessBlockVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private int Process_Block_Email_Seq;
	private String Process_Code;
	private String Email;
	private String Content;
	public int getProcess_Block_Email_Seq() {
		return Process_Block_Email_Seq;
	}
	public void setProcess_Block_Email_Seq(int process_Block_Email_Seq) {
		Process_Block_Email_Seq = process_Block_Email_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	
}
