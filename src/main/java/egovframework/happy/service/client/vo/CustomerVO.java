package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CustomerVO extends CommonVO implements Serializable {

	public CustomerVO() throws Exception {
		super();
	}
	
	private int Cust_Center_Seq;			//순번
	private String Cust_Center_Category;	//코드(C20)
	private String Nickname;		//글을 쓴 유저의 아이디,닉네임
	private String Email;				//이메일
	private String Title;					//제목
	private String Content;					//질문 내용
	private String Apply_Confirm_Yn;		//답변확인여부
	private String Apply_Yn;				//답변 여부
	private String Apply_Content; 			//답변 내용
	private String Apply_Id;				//답변한 사용자의 ID
	private String Apply_Ip;				//답변한 사용자의 IP
	private String Apply_Date;				//답변한 날짜
	
	private byte[] Mobile_No;	//암호화된 휴대폰 번호
	private String Mobile;		//복호화된 휴대폰 번호
	
	public byte[] getMobile_No() {
		return Mobile_No;
	}
	public void setMobile_No(byte[] mobile_No) {
		Mobile_No = mobile_No;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getApply_Confirm_Yn() {
		return Apply_Confirm_Yn;
	}
	public void setApply_Confirm_Yn(String apply_Confirm_Yn) {
		Apply_Confirm_Yn = apply_Confirm_Yn;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getCust_Center_Seq() {
		return Cust_Center_Seq;
	}
	public void setCust_Center_Seq(int cust_Center_Seq) {
		Cust_Center_Seq = cust_Center_Seq;
	}
	
	public String getCust_Center_Category() {
		return Cust_Center_Category;
	}
	public void setCust_Center_Category(String cust_Center_Category) {
		Cust_Center_Category = cust_Center_Category;
	}
	

	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	
	public String getApply_Yn() {
		return Apply_Yn;
	}
	public void setApply_Yn(String apply_Yn) {
		Apply_Yn = apply_Yn;
	}
	
	public String getApply_Content() {
		return Apply_Content;
	}
	public void setApply_Content(String apply_Content) {
		Apply_Content = apply_Content;
	}
	
	public String getApply_Id() {
		return Apply_Id;
	}
	public void setApply_Id(String apply_Id) {
		Apply_Id = apply_Id;
	}
	
	public String getApply_Ip() {
		return Apply_Ip;
	}
	public void setApply_Ip(String apply_Ip) {
		Apply_Ip = apply_Ip;
	}
	
	public String getApply_Date() {
		return Apply_Date;
	}
	public void setApply_Date(String apply_Date) {
		Apply_Date = apply_Date;
	} 
	
}
