package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CurationProcessVO extends CommonVO implements Serializable {
	public CurationProcessVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Curation_Process_Seq;	//큐레이션 과정일련번호
	private int Curation_Seq;			//큐레이션 일련번호
	private String Process_Code;		//과정코드
	private int Curation_Process_Order;	//큐레이션과정순서
	
	private String Public_Yn;					//공개여부
	private String Block_Yn;				//차단여부
	private String Process_Nm;				//과정명
	
	
	
	public String getBlock_Yn() {
		return Block_Yn;
	}
	public void setBlock_Yn(String block_Yn) {
		Block_Yn = block_Yn;
	}
	public String getProcess_Nm() {
		return Process_Nm;
	}
	public void setProcess_Nm(String process_Nm) {
		Process_Nm = process_Nm;
	}

	public String getPublic_Yn() {
		return Public_Yn;
	}
	public void setPublic_Yn(String public_Yn) {
		Public_Yn = public_Yn;
	}
	public int getCuration_Process_Order() {
		return Curation_Process_Order;
	}
	public void setCuration_Process_Order(int curation_Process_Order) {
		Curation_Process_Order = curation_Process_Order;
	}
	public int getCuration_Process_Seq() {
		return Curation_Process_Seq;
	}
	public void setCuration_Process_Seq(int curation_Process_Seq) {
		Curation_Process_Seq = curation_Process_Seq;
	}
	public int getCuration_Seq() {
		return Curation_Seq;
	}
	public void setCuration_Seq(int curation_Seq) {
		Curation_Seq = curation_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	
	
}
