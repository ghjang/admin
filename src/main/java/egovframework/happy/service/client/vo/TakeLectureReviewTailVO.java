package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TakeLectureReviewTailVO extends CommonVO implements Serializable {
	public TakeLectureReviewTailVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int TakeLecture_Review_Tail_Seq;	//수강후기댓글일련번호
	private int TakeLecture_Review_Seq;	//수강후기일련번호
	private int TakeLecture_Seq;	//수강일련번호
	private String Process_Code;	//과정코드
	private String User_Id;	//회원아이디
	private String Nickname;
	private String Profile_Img_Url;	//회원사진
	private String Tail_Content;	//댓글내용
	private String Delete_Yn;	//삭제여부
	
	public int getTakeLecture_Review_Tail_Seq() {
		return TakeLecture_Review_Tail_Seq;
	}
	public void setTakeLecture_Review_Tail_Seq(int takeLecture_Review_Tail_Seq) {
		TakeLecture_Review_Tail_Seq = takeLecture_Review_Tail_Seq;
	}
	public int getTakeLecture_Review_Seq() {
		return TakeLecture_Review_Seq;
	}
	public void setTakeLecture_Review_Seq(int takeLecture_Review_Seq) {
		TakeLecture_Review_Seq = takeLecture_Review_Seq;
	}
	public int getTakeLecture_Seq() {
		return TakeLecture_Seq;
	}
	public void setTakeLecture_Seq(int takeLecture_Seq) {
		TakeLecture_Seq = takeLecture_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getUser_Id() {
		return User_Id;
	}
	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getProfile_Img_Url() {
		return Profile_Img_Url;
	}
	public void setProfile_Img_Url(String profile_Img_Url) {
		Profile_Img_Url = profile_Img_Url;
	}
	public String getTail_Content() {
		return Tail_Content;
	}
	public void setTail_Content(String tail_Content) {
		Tail_Content = tail_Content;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
}
