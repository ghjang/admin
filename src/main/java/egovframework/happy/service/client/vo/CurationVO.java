package egovframework.happy.service.client.vo;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class CurationVO extends CommonVO implements Serializable {

	public CurationVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private int Curation_Seq;				//큐레이션 일련번호
	private String Curation_Nm;				//큐레이션명
	private String Line_Introduction;		//한줄소개
	private Timestamp Expression_Start_Day;	//노출 시작일
	private Timestamp Expression_CLOSE_Day;	//노출 종료일
	private String Use_Yn;					//사용여부
	private String Open_Yn;					//공개여부
	private String Delete_Yn;				//삭제여부
	private int Order;						//순서
	public int getCuration_Seq() {
		return Curation_Seq;
	}
	public void setCuration_Seq(int curation_Seq) {
		Curation_Seq = curation_Seq;
	}
	public String getCuration_Nm() {
		return Curation_Nm;
	}
	public void setCuration_Nm(String curation_Nm) {
		Curation_Nm = curation_Nm;
	}
	public String getLine_Introduction() {
		return Line_Introduction;
	}
	public void setLine_Introduction(String line_Introduction) {
		Line_Introduction = line_Introduction;
	}
	public Timestamp getExpression_Start_Day() {
		return Expression_Start_Day;
	}
	public void setExpression_Start_Day(Timestamp expression_Start_Day) {
		Expression_Start_Day = expression_Start_Day;
	}
	public Timestamp getExpression_CLOSE_Day() {
		return Expression_CLOSE_Day;
	}
	public void setExpression_CLOSE_Day(Timestamp expression_CLOSE_Day) {
		Expression_CLOSE_Day = expression_CLOSE_Day;
	}
	public String getUse_Yn() {
		return Use_Yn;
	}
	public void setUse_Yn(String use_Yn) {
		Use_Yn = use_Yn;
	}
	public String getOpen_Yn() {
		return Open_Yn;
	}
	public void setOpen_Yn(String open_Yn) {
		Open_Yn = open_Yn;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
	public int getOrder() {
		return Order;
	}
	public void setOrder(int order) {
		Order = order;
	}
	
}
