package egovframework.happy.service.client.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class QnaAnswerReplyVO extends CommonVO implements Serializable {
	public QnaAnswerReplyVO() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	private int Process_QnA_Reply_Seq;	//과정Q&A답글일련번호
	private int Process_QnA_Answer_Seq;	//과정Q&A답변일련번호
	private int Process_QnA_Seq;	//과정Q&A일련번호
	private String Process_Code;	//과정코드
	private String Reply_Content;	//답글내용
	private String Profile_Img_Url;
	private String Nickname;
	private String Delete_Yn;	//삭제여부
	
	public int getProcess_QnA_Reply_Seq() {
		return Process_QnA_Reply_Seq;
	}
	public void setProcess_QnA_Reply_Seq(int process_QnA_Reply_Seq) {
		Process_QnA_Reply_Seq = process_QnA_Reply_Seq;
	}
	public int getProcess_QnA_Answer_Seq() {
		return Process_QnA_Answer_Seq;
	}
	public void setProcess_QnA_Answer_Seq(int process_QnA_Answer_Seq) {
		Process_QnA_Answer_Seq = process_QnA_Answer_Seq;
	}
	public int getProcess_QnA_Seq() {
		return Process_QnA_Seq;
	}
	public void setProcess_QnA_Seq(int process_QnA_Seq) {
		Process_QnA_Seq = process_QnA_Seq;
	}
	public String getProcess_Code() {
		return Process_Code;
	}
	public void setProcess_Code(String process_Code) {
		Process_Code = process_Code;
	}
	public String getReply_Content() {
		return Reply_Content;
	}
	public void setReply_Content(String reply_Content) {
		Reply_Content = reply_Content;
	}
	public String getProfile_Img_Url() {
		return Profile_Img_Url;
	}
	public void setProfile_Img_Url(String profile_Img_Url) {
		Profile_Img_Url = profile_Img_Url;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getDelete_Yn() {
		return Delete_Yn;
	}
	public void setDelete_Yn(String delete_Yn) {
		Delete_Yn = delete_Yn;
	}
}
