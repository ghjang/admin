package egovframework.happy.web.admin;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

/**
 * <pre>
 * 다운로드구현
 * </pre>
 * @Class DownloadView.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 15.
 * @version 1.0
 */
public class DownloadView extends AbstractView {
	
	/**
	 * <pre>
	 * ContentType 설정하기
	 * </pre>
	 * @Class DownloadView.java
	 * @Method Download 
	 */
	public void Download() {
		setContentType("application/download; utf-8");
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		File file = (File)model.get("downloadFile");
		String fileStr = null;
		if(file.exists()) {
			System.out.println("File exists");
			byte readByte[] = new byte[4096];

			fileStr = file.getName();
			
			response.setContentType("application/octet-stream");
			fileStr = URLEncoder.encode(fileStr, "UTF-8");
			response.setHeader("Content-disposition", "attachment;filename=" + fileStr);

			BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
			OutputStream outs = response.getOutputStream();

			int read;
			while ((read = fin.read(readByte, 0, 4096)) != -1)
				outs.write(readByte, 0, read);
			outs.flush();
			outs.close();
			fin.close();
		}//end if(file.exists())
	}
}
