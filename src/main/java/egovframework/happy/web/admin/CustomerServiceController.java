package egovframework.happy.web.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.happy.cmmn.AES256Util;
import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.CustomerService;
import egovframework.happy.service.client.vo.CustomerVO;

/**
 * <pre>
 * [관리자페이지]
 * 고객센터 관련 Controller
 * </pre>
 * @Class CustomerServiceController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class CustomerServiceController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page
	
	@Resource(name = "customerService")
	private CustomerService customerService;
	
	/**
	 * <pre>
	 * 고객센터 - 화면 보여주기
	 * </pre>
	 * @Class CustomerServiceController.java
	 * @Method customerServiceList
	 * @param model
	 * @param request
	 * @param currentPage 현재 페이지
	 * @param answerDelay 답변대기
	 * @param answerComplete
	 * @param searchText
	 * @param orderBy
	 * @param sDate
	 * @param eDate
	 * @param chkSearch
	 * @param searchCategory
	 * @return "customerService"-고객센터 View
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/customerService.do")
	public ModelAndView customerServiceList(ModelMap model, HttpServletRequest request,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			@RequestParam(value="answer_delay", required=false) boolean answerDelay,
			@RequestParam(value="answer_complete", required=false) boolean answerComplete,
			@RequestParam(value="searchText", required=false, defaultValue="") String searchText,
			@RequestParam(value="orderBy", required=false, defaultValue="D") String orderBy,
			@RequestParam(value="start_period", required=false, defaultValue="") String sDate,
			@RequestParam(value="end_period", required=false, defaultValue="") String eDate,
			@RequestParam(value="chkSearch", required=false, defaultValue="N") String chkSearch,
			@RequestParam(value="searchCategory", required=false, defaultValue="") String searchCategory
			) throws Exception {
		
		
		model.addAttribute("category", commonService.selectCode("CUST_CENTER_CATEGORY"));

		System.out.println("order by : " + orderBy + " / Text : " + searchText +" / searchCategory : " + searchCategory);
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		int applyYCount = 0;
		int applyNCount = 0;
		int searchItems = 0;
		int startingSNo = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		totalItems =  customerService.customerTotCnt();
		applyYCount = customerService.customerApplyYCnt();
		applyNCount = customerService.customerApplyNCnt();
		
		List<CustomerVO> customerList = new ArrayList<>();
		List<Integer> pageNumbers = new ArrayList<>();
		
		numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		from = Paging.getStartingIndex(currentPage, itemSize);
		
		searchItems = totalItems;
		
		if ("N".equalsIgnoreCase(chkSearch)) { 
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			map.put("orderBy", orderBy);
			
			customerList = customerService.selecCustomertList(map);
			
			pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
			startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list
			
			answerDelay = true;
			answerComplete = true;
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			
			System.out.println(answerDelay + ", " + answerComplete);
			
			map.put("answerDelay", answerDelay);
			map.put("answerComplete", answerComplete);
			map.put("searchText", searchText);
			map.put("sDate", sDate + " 00:00:00");
			map.put("eDate", eDate + " 23:59:59.99");
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			map.put("orderBy", orderBy);
			map.put("searchCategory", searchCategory);
			
			searchItems = customerService.searchTotCnt(map); //검색 결과 화면의 아이템 갯수
			numberOfPages = Paging.getNumberOfPages(searchItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			customerList = customerService.searchCustomertList(map); //검색 결과 리스트
			
			pageNumbers = Paging.addPageNumbers(currentPage, searchItems, numberOfPages);
			startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list
			
		}
		
		for (CustomerVO vo : customerList) {
			if (null != vo.getMobile_No() && 0 != vo.getMobile_No().length)
				vo.setMobile(AES256Util.decrypt(vo.getMobile_No()));
		}
		
		model.addAttribute("searchCount", searchItems);
		model.addAttribute("searchCategory", searchCategory);
		model.addAttribute("searchText", searchText);
		model.addAttribute("orderBy", orderBy);
		model.addAttribute("chkSearch", chkSearch);
		model.addAttribute("start_period", sDate);
		model.addAttribute("end_period", eDate);
		model.addAttribute("answerDelay", answerDelay);
		model.addAttribute("answerComplete", answerComplete);
		model.addAttribute("customerList", customerList);
		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		model.addAttribute("totalItems", totalItems);
		model.addAttribute("itemSize", itemSize);
		model.addAttribute("applyYCount", applyYCount);
		model.addAttribute("applyNCount", applyNCount);
		
		return new ModelAndView("admin/admin/customerService");
	}
	
	/**
	 * <pre>
	 * 고객센터 - 검색하기
	 * </pre>
	 * @Class CustomerServiceController.java
	 * @Method SearchCustomer
	 * @param request
	 * @param model
	 * @param sDate
	 * @param eDate
	 * @param chkSearch
	 * @param answerStatus
	 * @return 
	 */
	@RequestMapping(value = "/admin/SearchCustomer.do")
	public String SearchCustomer(HttpServletRequest request, ModelMap model,
			@RequestParam(value="start_period", required=true, defaultValue="2017-12-25") String sDate,
			@RequestParam(value="start_period", required=true, defaultValue="2017-12-25") String eDate,
			@RequestParam(value="chkSearch", required=true, defaultValue="Y") String chkSearch,
			@RequestParam(value="answer_status", required=true) String [] answerStatus) {
		
		System.out.println("sDate : " + sDate + " /  eDate : " + eDate + " / " + chkSearch);
		for (String abc : answerStatus) {
			System.out.println("base : " + abc);
		}
		
		return null;
	}
	
	
	/**
	 * <pre>
	 * 고객센터 - 답변달기
	 * </pre>
	 * @Class CustomerServiceController.java
	 * @Method writeApplyinAdminPage
	 * @param customerVO
	 * @param model
	 * @param request
	 * @param currentPage
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/writeApplyinAdminPage.do")
	public String writeApplyinAdminPage(@ModelAttribute("CustomerVO") CustomerVO customerVO, ModelMap model, HttpServletRequest request, 
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		System.out.println("Cust_Center_Seq : " + customerVO.getCust_Center_Seq());
		
//		String content = customerVO.getApply_Content().replace("\r\n", "<br/>");
//		customerVO.setApply_Content(content);
		int check = customerService.writeApplyinAdminPage(customerVO);
		if (0 < check) {
			System.out.println("답변 달았습니다.");
			int chk = customerService.writeApplyLogInAdminPage(customerVO);
			System.out.println("Customer Center Log 추가: "+ chk);
		} else {
			System.out.println("실패했어요 답변달기.");
		}
		return "redirect:/admin/customerService.do?pn="+currentPage;
	}

	/**
	 * <pre>
	 * 고객센터 - 고객센터 문의 내용 자세히보기
	 * </pre>
	 * @Class CustomerServiceController.java
	 * @Method qnaDetail
	 * @param customerVO
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/custService/modal/qnaDetail.do")
	public String qnaDetail(@ModelAttribute("customerVO") CustomerVO customerVO, ModelMap model,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		customerVO = customerService.selectCustomerQnA(customerVO);
		
		model.addAttribute("category", commonService.selectCode("CUST_CENTER_CATEGORY"));
		model.addAttribute("custService", customerVO);
		model.addAttribute("pn", currentPage);
		return "admin/modal/qnaDetail";
	}
}
