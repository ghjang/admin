package egovframework.happy.web.admin;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import egovframework.happy.cmmn.FileUtils;
import egovframework.happy.service.client.basic.CommonService;
import egovframework.rte.fdl.property.EgovPropertyService;

public class BaseController {
	@Autowired
	EgovPropertyService propertiesService;
	
	@Autowired
	FileUtils fileUtils;
	
	@Resource(name = "commonService")
	protected CommonService commonService;
	
	// Logger
	protected final Logger log = LoggerFactory.getLogger(getClass());
}
