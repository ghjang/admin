package egovframework.happy.web.admin;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import egovframework.happy.cmmn.ExcelUtil;
import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.CurationService;
import egovframework.happy.service.admin.basic.CustomerService;
import egovframework.happy.service.admin.basic.NoticeService;
import egovframework.happy.service.admin.basic.OperatorService;
import egovframework.happy.service.admin.basic.ProcessService;
import egovframework.happy.service.admin.dto.ProcessDTO;
import egovframework.happy.service.client.basic.ManageStatisticsService;
import egovframework.happy.service.client.vo.CurationVO;
import egovframework.happy.service.client.vo.CustomerVO;
import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.FileVO;
import egovframework.happy.service.client.vo.NoticeVO;
import egovframework.happy.service.client.vo.OperatorVO;
import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
public class AdminController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Resource(name = "noticeService")
	private NoticeService noticeService;
	
	@Resource(name = "processService")
	private ProcessService processService;
	
	@Resource(name = "curationService")
	private CurationService curationService;
	
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "manageStatisticsService")
	private ManageStatisticsService manageStatisticsService;
	
	@Resource(name = "customerService")
	private CustomerService customerService;
	
	@Resource(name = "operatorService")
	private OperatorService operatorService;
	
	@RequestMapping(value = { "/admin", "/admin/", "/admin/index.do" })
	public String index(ModelMap model) throws Exception {
		
		return "redirect:/admin/memberStatus.do";
	}
	
	
////회원현황/////////////////////////////////////////////////////////////////////////////////////////	
	@RequestMapping(value = "/admin/memberStatus.do")
	public String memberStatus(ModelMap model,@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			HttpServletRequest request) throws Exception {
		System.out.println("memberStatus");
		//검색화면 세팅
		model.addAttribute("search_period", commonService.selectCode("SEARCH_PERIOD"));

		return "admin/admin/memberStatus";
	}
	
	
	
////과정현황/////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/admin/processStatus.do")
	public ModelAndView processStatus(ModelMap model,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			HttpServletRequest request) throws Exception {
		
		//검색화면 세팅
		model.addAttribute("field_code", commonService.selectCode("FIELD"));
		model.addAttribute("open_code", commonService.selectCode("OPEN_YN"));
		model.addAttribute("star_ratings", commonService.selectCode("STAR_RATINGS"));
		model.addAttribute("search_period", commonService.selectCode("SEARCH_PERIOD"));
		model.addAttribute("processDefaultCnt", processService.processDefaultCnt());
		
		HashMap<String, Object> map = new HashMap<>();
		List<ProcessDTO> list = null;
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		
		if(request.getParameter("search") == null) {
			System.out.println("default 화면");
			
			System.out.println("검색결과 수: "+ processService.processDefaultCnt());
			model.addAttribute("searchCnt", processService.processDefaultCnt());
			
			totalItems = processService.processDefaultCnt();	//count 결과 넣어줘야함 
			numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
			
			map.put("offset", from);
			map.put("pagesize", ITEMS_PER_PAGE);
			
			map.put("start_period", "2016-01-01");
			map.put("end_period", new Date());
			
			list = processService.list(map);

		} else {
			System.out.println("검색화면");
			String[] Process_Total_Grade_list = request.getParameterValues("Process_Total_Grade");
			String[] Field_Code_list = request.getParameterValues("Field_Code");
			String[] Open_Yn_list = request.getParameterValues("Open_Yn");
			String[] Free_Yn_list = request.getParameterValues("Free_Yn");
			String ordering = request.getParameter("ordering");
			
			String Block_Yn = request.getParameter("Block_Yn") == null ? "N" : request.getParameter("Block_Yn");
			String start_period = request.getParameter("start_period") == null ? "" : request.getParameter("start_period");
			String end_period = request.getParameter("end_period") == null ? "" : request.getParameter("end_period");
			
			System.out.println("ordering: " + ordering);
			
			map.put("Process_Total_Grade", Process_Total_Grade_list);
			map.put("Field_Code", Field_Code_list);
			map.put("Open_Yn", Open_Yn_list);
			map.put("Block_Yn", Block_Yn);
			map.put("Free_Yn", Free_Yn_list);
			map.put("start_period", start_period);
			map.put("end_period", end_period);
			map.put("ordering", ordering);
			
/*			//콘솔창에 확인
			System.out.println("----processStatus----start-----");
			System.out.println("start_period: "+start_period);
			System.out.println("end_period: "+end_period);

			System.out.print("Process_Total_Grade: ");
			for(int i=0; i < Process_Total_Grade_list.length; i++) {
				System.out.print(Process_Total_Grade_list[i] + " ");
			}
			System.out.println();

			System.out.print("Field_Code: ");
			for(int i=0; i < Field_Code_list.length; i++) {
				System.out.print(Field_Code_list[i] + " ");
			}
			System.out.println();
			
			System.out.print("Open_Yn: ");
			for(int i=0; i < Open_Yn_list.length; i++) {
				System.out.print(Open_Yn_list[i] + " ");
			}
			System.out.println();
			System.out.println("Block_Yn: "+Block_Yn);
			
			System.out.print("Free_Yn: ");
			for(int i=0; i < Free_Yn_list.length; i++) {
				System.out.print(Free_Yn_list[i] + " ");
			}
			System.out.println();
			
			System.out.println("ordering: " + ordering);
			
			System.out.println("----processStatus-----end----");
			//콘솔창에 확인 끝
			*/
			
			String searchCategory = request.getParameter("searchCategory");
			String content = request.getParameter("content");
			System.out.println("searchCategory: " + searchCategory);
			System.out.println("content: " + content);
			
			if(searchCategory.equals("Tag")) {
				System.out.println("Tag");
				HashMap<String, String> tempMap = new HashMap<>();
				tempMap.put("content", content);
				List<String> searchedTagProcessCodeList = processService.searchTagList(tempMap);
				for (String string : searchedTagProcessCodeList) {
					System.out.println(string);
				}
				map.put("searchedTagProcessCodeList", searchedTagProcessCodeList);
			}

			System.out.println("하하");

			map.put("searchCategory", searchCategory);
			map.put("content", content);
			
			System.out.println("검색결과 수: "+ processService.selectSerchCnt(map));
			model.addAttribute("searchCnt", processService.selectSerchCnt(map));
			
			totalItems = processService.selectSerchCnt(map);	/////////////////////////////바꿔야해 검색어있는 결과수로//////////////////////////
			numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
			
			map.put("offset", from);
			map.put("pagesize", ITEMS_PER_PAGE);
			
			list = processService.list(map);

			System.out.println("ooordering: " + ordering);
			
			//체크된 애들 값 보내줌
			model.addAttribute("chk_start", start_period);
			model.addAttribute("chk_end", end_period);
			model.addAttribute("chk_Grade", Process_Total_Grade_list);
			model.addAttribute("chk_Field", Field_Code_list);
			model.addAttribute("chk_Open", Open_Yn_list);
			model.addAttribute("chk_Block", Block_Yn);
			model.addAttribute("chk_Free", Free_Yn_list);
			model.addAttribute("chk_Order", ordering);
			
			model.addAttribute("search", true);
		} //ifelse(search == null)
		
		//Tag setting
		HashMap<String, List<TagVO>> resultMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			resultMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
//			System.out.println(processDTO.getProcess_Code() + ": " + resultMap.get(processDTO.getProcess_Code()));
		}
		//차단 사유 세팅
		HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
		}
		model.addAttribute("blockMap", blockMap);
		
//		System.out.println(resultMap);
		
		//paging 화면 설정 시작
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * ITEMS_PER_PAGE; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		//paging 화면 설정 끝
		
		model.addAttribute("processList", list);
		
		return new ModelAndView("admin/admin/processStatus", "resultMap", resultMap);
	}
	
	@RequestMapping(value = "/admin/searchProcess.do", method = {RequestMethod.POST})
	public String searchProcess(HttpServletRequest request, HttpServletResponse response, ModelMap model,
			String Block_Yn, String start_period, String end_period) throws Exception {
		System.out.println("searchProcess------------------------------------------------------");
		System.out.println("searchProcess] request.getAttribute(process_total_grade): " + request.getAttribute("Process_Total_Grade"));
		System.out.println("searchProcess] request.getParameterValues(Process_Total_Grade): " + request.getParameterValues("Process_Total_Grade"));
		System.out.println("searchProcess] request.getParameterMap():" +request.getParameterMap());
		String[] Process_Total_Grade = request.getParameterValues("Process_Total_Grade");
		String[] Field_Code =request.getParameterValues("Field_Code");
		String[] Open_Yn = request.getParameterValues("Open_Yn");
		String[] Free_Yn = request.getParameterValues("Free_Yn");
		String searchCategory = request.getParameter("searchCategory");
		String content = request.getParameter("content");
		String ordering = request.getParameter("ordering");
		
		String excel = request.getParameter("excel");
		System.out.println("excel: " + excel);
		
		System.out.println("start_period: "+start_period);
		System.out.println("end_period: "+end_period);
//		
//		for(int i=0; i<Process_Total_Grade.length; i++) {
//			System.out.print(" Process_Total_Grade^^: " + Process_Total_Grade[i]);
//		}
//		System.out.println();
//		for(int i=0; i<Field_Code.length;i++) {
//			System.out.print(" Field_Code^^: "+ Field_Code[i]);
//		}
		System.out.println();
		System.out.println("Open_Yn "+ Open_Yn);
		System.out.println("Block_Yn "+Block_Yn);
		System.out.println("Free_Yn "+Free_Yn);
		System.out.println("ordering: "+ordering);
		
		model.addAttribute("start_period",start_period);
		model.addAttribute("end_period",end_period);
		model.addAttribute("Process_Total_Grade", Process_Total_Grade);
		model.addAttribute("Block_Yn", Block_Yn);
		model.addAttribute("Free_Yn", Free_Yn);
		model.addAttribute("Open_Yn", Open_Yn);
		model.addAttribute("Field_Code", Field_Code);
		model.addAttribute("searchCategory", searchCategory);
		model.addAttribute("content", content);
		model.addAttribute("ordering", ordering);
		
		model.addAttribute("search",true);
		
		if(excel.equals("Y")) {
			return "redirect:/admin/processExcelDown.do";
		}
		
		return "redirect:/admin/processStatus.do";
	}
	
	@RequestMapping(value = "/admin/process/modal/TagDetail.do")
	public String TagDetail(@ModelAttribute("processVO") ProcessVO processVO, ModelMap model) throws Exception {
		List<TagVO> tagList = processService.selectAllTagList(processVO);
		model.addAttribute("tagList",tagList);
		return "admin/modal/tagDetail";
	}
	
	
	@RequestMapping(value = "/admin/process/modal/reviewDetail.do")
	public Object reviewDetail(HttpServletRequest request,
			@ModelAttribute("processDTO") ProcessDTO processDTO, ModelMap model, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		System.out.println("reviewDetail----------------------------------------");
		
		System.out.println("processDTO- Process_Code: "+processDTO.getProcess_Code());
		System.out.println("processDTO- TakeLecture_Review_Count: "+processDTO.getTakeLecture_Review_Count());

		model.addAttribute("processDTO", processDTO);
		
		List<TakeLectureReviewVO> reviewList = null;
		
		// for paging
		int totalItems = processDTO.getTakeLecture_Review_Count();
		
		System.out.println("currentPage: " + currentPage);
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
		
		System.out.println("totalItems:" + totalItems);
		System.out.println("numberOfPages: " + numberOfPages);
		System.out.println("currentPage: " + currentPage);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("ProcessDTO", processDTO);
		map.put("offset", from);
		map.put("pagesize", ITEMS_PER_PAGE);
		
		reviewList = processService.selectReviewList(map);

		HashMap<Integer, List<TakeLectureReviewVO>> resultMap = new HashMap<>();
		for (TakeLectureReviewVO takeLectureReviewVO : reviewList) {
			resultMap.put(takeLectureReviewVO.getTakeLecture_Review_Seq(), processService.selectReviewTailList(takeLectureReviewVO));
		}
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * ITEMS_PER_PAGE; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		model.addAttribute("reviewList",reviewList);
		ModelAndView mav;
		if(request.getParameter("Paging") == null) {
			System.out.println("Paging: "+request.getParameter("Paging"));
			mav = new ModelAndView("admin/modal/reviewDetail","resultMap",resultMap);
		} else {
			System.out.println(request.getParameter("Paging"));
			System.out.println("currentPage: "+currentPage);
			System.out.println("list: "+reviewList.toString());
			mav = new ModelAndView("admin/modal/reviewDetail","resultMap",resultMap);
			mav.addObject("reviewList",reviewList);
			mav.addObject("startingSNo", startingSNo);
			mav.addObject("numOfPages", numberOfPages);
			mav.addObject("currentPage", currentPage);
			mav.addObject("pageNumbers", pageNumbers);
			mav.addObject("processDTO", processDTO);
		}
		return mav;
	}
	
	@RequestMapping(value = "/admin/process/modal/takeLecturePeople.do")
	public String takeLecturePeople(@ModelAttribute("processDTO") ProcessDTO processDTO, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage, ModelMap model) throws Exception {
		List<TakeLectureVO> lectureList = null;
		
		// for paging
		int totalItems = 0;

		totalItems = processService.takeLecturePeopleCnt(processDTO);
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("ProcessDTO", processDTO);
		map.put("offset", from);
		map.put("pagesize", ITEMS_PER_PAGE);
		
		lectureList = processService.selectTakeLectureList(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * ITEMS_PER_PAGE; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		model.addAttribute("lectureList",lectureList);
		return "admin/modal/takeLecturePeople";
	}
	
	@RequestMapping(value = "/admin/updateProcessOpenStatus.do")
	public Object updateProcessOpenStatus(@ModelAttribute("processDTO") ProcessDTO processDTO, Model model, HttpServletRequest request) throws Exception {
		System.out.println(processDTO.getProcess_Code() + ", " + processDTO.getOpen_Yn() + ", " + processDTO.getBlock_Yn());
		
		int updateOpenResult = processService.updateOpenStatus(processDTO);

		ModelAndView mav = new ModelAndView("jsonV");
		mav.addObject("updateOpenResult", updateOpenResult);
		
		return mav;
	}
	
	@RequestMapping(value = "/admin/sendEmail.do")
	public String sendEmail(@ModelAttribute("processBlockVO") ProcessBlockVO processBlockVO, Model model, HttpServletRequest request) throws Exception {
		System.out.println(processBlockVO.getProcess_Code() + ", " + request.getParameter("Block_Yn"));
		System.out.println(processBlockVO.getEmail() + ", " + processBlockVO.getContent());
		
		ProcessVO processVO = new ProcessVO();
		processVO.setBlock_Yn("Y");
		processVO.setProcess_Code(processBlockVO.getProcess_Code());
		processService.updateBlockStatus(processVO);
		processService.insertProcessBlock(processBlockVO);
		
		String fromEmail = "yjjung@ccfocusone.com";
//		String toEmail = processBlockVO.getEmail();
		String toEmail = "yjjung@ccfocusone.com";
		String content = processBlockVO.getContent();
		String title = "[HC]차단되었습니다.";
		
		try{
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom(fromEmail);
			messageHelper.setTo(toEmail);
			messageHelper.setSubject(title);
			messageHelper.setText(content);
			
			mailSender.send(message);
		} catch(Exception e) {
			System.out.println("sendEmail error: " + e.getMessage());
		}
		
		return "redirect:/admin/processStatus.do";
	}
	
	@RequestMapping(value = "/admin/process/modal/sendEmailBlocking.do")
	public String sendEmailBlocking(@ModelAttribute("processDTO") ProcessDTO processDTO, ModelMap model) throws Exception {
		System.out.println("차단메일보내기");
		System.out.println(processDTO);
		model.addAttribute("process", processDTO);
		return "admin/modal/sendEmailBlocking";
	}

	@RequestMapping(value = "/admin/process/modal/blockReason.do")
	public String blockReason(@ModelAttribute("processBlockVO") ProcessBlockVO processBlockVO, ModelMap model) throws Exception {
		
		model.addAttribute("block", processService.selectBlockReasonDetail(processBlockVO));
		return "admin/modal/blockReasonDetail";
	}
	
	//엑셀 다운로드
	@RequestMapping(value = "/admin/processExcelDown.do")
	public void processExcelDown(ModelMap model, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("process excel");
		//일단 지금은 전체결과를 엑셀로 다운로드. 조건에 맞는 내용만 다운로드 받으려면 조작이 필요******************************************************
		List<ProcessDTO> list = null;
		
		String[] Process_Total_Grade_list = request.getParameterValues("Process_Total_Grade");
		String[] Field_Code_list = request.getParameterValues("Field_Code");
		String[] Open_Yn_list = request.getParameterValues("Open_Yn");
		String[] Free_Yn_list = request.getParameterValues("Free_Yn");
		String ordering = request.getParameter("ordering");
		
		String Block_Yn = request.getParameter("Block_Yn") == null ? "N" : request.getParameter("Block_Yn");
		String start_period = request.getParameter("start_period") == null ? "" : request.getParameter("start_period");
		String end_period = request.getParameter("end_period") == null ? "" : request.getParameter("end_period");
		
		HashMap<String, Object> map = new HashMap<>();
		
		map.put("Process_Total_Grade", Process_Total_Grade_list);
		map.put("Field_Code", Field_Code_list);
		map.put("Open_Yn", Open_Yn_list);
		map.put("Block_Yn", Block_Yn);
		map.put("Free_Yn", Free_Yn_list);
		map.put("start_period", start_period);
		map.put("end_period", end_period);
		map.put("ordering", ordering);
		
		list = processService.list(map);
		System.out.println("엑셀 list: " + list);
		System.out.println("엑셀 list size: " + list.size());
		System.out.println(map);
		
		//Tag setting
		HashMap<String, List<TagVO>> tagMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			tagMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
		}
		
		//차단 사유 세팅
		HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
		}
		
		String fileStr = ExcelUtil.excelProcessStatus(list, tagMap, blockMap);
		
		File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
		
		if(file.exists()) {
			System.out.println("File exists");
			byte readByte[] = new byte[4096];

			response.setContentType("application/octet-stream");
			fileStr = URLEncoder.encode(fileStr, "UTF-8");
			response.setHeader("Content-disposition", "attachment;filename=" + fileStr);

			BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
			OutputStream outs = response.getOutputStream();

			int read;
			while ((read = fin.read(readByte, 0, 4096)) != -1)
				outs.write(readByte, 0, read);
			outs.flush();
			outs.close();
			fin.close();
		}
	}
	
	@RequestMapping(value = "/admin/defaultExcel.do")
	public void defaultExcel(ModelMap model, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<ProcessDTO> list = null;
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("start_period", "2016-01-01");
		map.put("end_period", new Date());
		
		list = processService.list(map);
		System.out.println("엑셀 list: " + list);
		System.out.println("엑셀 list size: " + list.size());
		System.out.println(map);
		
		//Tag setting
		HashMap<String, List<TagVO>> tagMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			tagMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
		}
		
		//차단 사유 세팅
		HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
		}
		
		String fileStr = ExcelUtil.excelProcessStatus(list, tagMap, blockMap);
		
		File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
		
		if(file.exists()) {
			System.out.println("File exists");
			byte readByte[] = new byte[4096];

			response.setContentType("application/octet-stream");
			fileStr = URLEncoder.encode(fileStr, "UTF-8");
			response.setHeader("Content-disposition", "attachment;filename=" + fileStr);

			BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
			OutputStream outs = response.getOutputStream();

			int read;
			while ((read = fin.read(readByte, 0, 4096)) != -1)
				outs.write(readByte, 0, read);
			outs.flush();
			outs.close();
			fin.close();
		}
		
	}
	
	//큐레이션//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/admin/curation.do")
	public ModelAndView curation(@RequestParam(value="pn", required=false, defaultValue="1") int currentPage, ModelMap model) throws Exception {
		List<CurationVO> curationList = null;

		// for paging
		int totalItems = curationService.curationTotCnt();
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
		
		HashMap<String, Integer> map = new HashMap<>();
		map.put("offset", from);
		map.put("pagesize", ITEMS_PER_PAGE);
		
		curationList = curationService.selectList(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * ITEMS_PER_PAGE; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		model.addAttribute("totalItems", totalItems);
		
		return new ModelAndView("admin/admin/curation","curationList",curationList);
	}
	
	
	@RequestMapping(value = "/admin/registerCurationView.do")
	public String registerCurationView() throws Exception {
		return "admin/admin/registerCuration";
	}
	
	@RequestMapping(value = "/admin/registerCuration.do")
	public String registerCuration(@ModelAttribute("curationVO") CurationVO curationVO) throws Exception {
		// order 정해줘야만함!! 
		curationService.insertCuration(curationVO);
		
		return "redirect:/admin/curation.do";
	}
	
	@RequestMapping(value = "/admin/process/modal/curationProcessAdd.do")
	public String curationProcessAdd(ModelMap model) throws Exception {
		List<ProcessDTO> processList = processService.selectProcessList();
		
		model.addAttribute("processList", processList);
		return "admin/modal/curationProcessAdd";
	}
	
	
	//공지사항/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/admin/notice.do")
	public ModelAndView notice(@RequestParam(value="pn", required=false, defaultValue="1") int currentPage, ModelMap model) throws Exception {
//		List<NoticeVO> noticeList = noticeService.selectNoticeList();
		List<NoticeVO> list = null;
		
		// for paging
		int totalItems = 0;

		totalItems = noticeService.selectNoticeListTotCnt();
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, ITEMS_PER_PAGE);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, ITEMS_PER_PAGE);
		
		HashMap<String, Integer> map = new HashMap<>();
		map.put("offset", from);
		map.put("pagesize", ITEMS_PER_PAGE);
		
		list = noticeService.list(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * ITEMS_PER_PAGE; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		return new ModelAndView("admin/admin/notice", "noticeList", list);
	}
	
	@RequestMapping(value = "/admin/registerArticle.do")
	public String registerArticle(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model, MultipartHttpServletRequest multiReq, RedirectAttributes redirectAttributes) throws Exception {
		 //첨부파일 업로드
  		final Map<String, MultipartFile> files = multiReq.getFileMap();

  		System.out.println(files);
  		
  		if(! files.isEmpty()) {
  			List<FileVO> ListFileVo = fileUtils.parseMultiFileInfo(files, "NOTICE", "");
  			for(FileVO vo : ListFileVo) {
  				System.out.println("@@@@@@@@@@@@@@@ [vo:"+vo.toString()+"]");
  				commonService.insertFileInfo(vo);
  				System.out.println(" [getAttach_File_Seq:"+vo.getAttach_File_Seq()+"]");
  				noticeVO.setImage(vo.getAttach_File_Seq());
  			}
  		}
		noticeService.insertNotice(noticeVO);
		model.addAttribute("notice", noticeVO);
		return "redirect:/admin/notice.do";
	}
	
	@RequestMapping(value = "/admin/selectNoticeDetail.do")
	public String selectNoticeDetail(@RequestParam("id") int id, Model model) throws Exception {
		System.out.println("selectNoticeDetail");
		NoticeVO resultNotice = noticeService.selectNoticeDetailBySeq(id);
		model.addAttribute("notice", resultNotice);
		return "redirect:/admin/notice.do";
	}
	
	@RequestMapping(value = "/admin/deleteNotice.do")
	public String deleteNotice(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model) throws Exception {
		System.out.println("deleteNotice");
//		System.out.println("noticeVO: " + noticeVO);
		int result = noticeService.deleteNotice(noticeVO);
//		System.out.println("delete 수: " + result);
		return "redirect:/admin/notice.do";
	}
	
	@RequestMapping(value = "/admin/deleteNoticeBySeq.do")
	public String deleteNoticeBySeq(@RequestParam("id") int id, Model model) throws Exception {
		System.out.println("deleteNoticeBySeq  " + id);
		noticeService.deleteNoticeBySeq(id);
		return "redirect:/admin/notice.do";
	}
	
	@RequestMapping(value = "/admin/updateNotice.do", method={RequestMethod.POST})
	public String updateNotice(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model, MultipartHttpServletRequest multiReq, RedirectAttributes redirectAttributes) throws Exception {
		System.out.println("updateNotice");

		 //첨부파일 업로드
  		final Map<String, MultipartFile> files = multiReq.getFileMap();

  		System.out.println(files);
  		
  		if(! files.isEmpty()) {
  			List<FileVO> ListFileVo = fileUtils.parseMultiFileInfo(files, "NOTICE", "");
  			for(FileVO vo : ListFileVo) {
  				System.out.println("@@@@@@@@@@@@@@@ [vo:"+vo.toString()+"]");
  				commonService.insertFileInfo(vo);
  				System.out.println(" [getAttach_File_Seq:"+vo.getAttach_File_Seq()+"]");
  				noticeVO.setImage(vo.getAttach_File_Seq());
  			}
  		}
		
		noticeService.updateNotice(noticeVO);
		return "redirect:/admin/notice.do";
	}
	
	@RequestMapping(value = "/admin/updateOpenInfo.do")
	public String updateOpenInfo(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model) throws Exception {
		System.out.println("registerArticle noviceVO: " + noticeVO);
		
		noticeService.updateOpenInfo(noticeVO);
		return "redirect:/admin/notice.do";
	}

	
	
	
	
//////고객센터////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/admin/customerService.do")
	public ModelAndView customerServiceList(ModelMap model, HttpServletRequest request,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		int applyYCount = 0;
		int applyNCount = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		totalItems =  customerService.customerTotCnt();
		applyYCount = customerService.customerApplyYCnt();
		applyNCount = customerService.customerApplyNCnt();
		
		numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		from = Paging.getStartingIndex(currentPage, itemSize);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("from", from);
		map.put("itemsPerPage", itemSize);
		
		List<CustomerVO> customerList = customerService.selecCustomertList(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list
		
		model.addAttribute("customerList", customerList);
		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		model.addAttribute("totalItems", totalItems);
		model.addAttribute("applyYCount", applyYCount);
		model.addAttribute("applyNCount", applyNCount);
		//model.addAttribute("searchCount", "1");
		
		return new ModelAndView("admin/admin/customerService");
	}
	
	@RequestMapping(value = "/admin/writeApplyinAdminPage.do")
	public String writeApplyinAdminPage(@ModelAttribute("CustomerVO") CustomerVO customerVO, ModelMap model, HttpServletRequest request, 
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		System.out.println("Cust_Center_Seq : " + customerVO.getCust_Center_Seq());
		System.out.println("Apply_Id : " + customerVO.getApply_Id());
		System.out.println("Apply_IP : " + customerVO.getApply_Ip());
		System.out.println("Apply_Content : " + customerVO.getApply_Content());
		
		int check = customerService.writeApplyinAdminPage(customerVO);
		if (0 < check) {
			System.out.println("답변 달았습니다.");
		} else {
			System.out.println("실패했어요 답변달기.");
		}
		return "redirect:/admin/customerService.do?pn="+currentPage;
	}
	
	
	
//////운영자관리////////////////////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/admin/operatorManagement.do")
	public ModelAndView operatorManagement(ModelMap model,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			HttpServletRequest request) throws Exception {
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		int operatorCount = operatorService.getCountOfAllOperator();
		numberOfPages = Paging.getNumberOfPages(operatorCount, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		from = Paging.getStartingIndex(currentPage, itemSize);
		Map <String, Object> map = new HashMap<String, Object>();
		map.put("from", from);
		map.put("itemsPerPage", itemSize);
		
		List<OperatorVO> operatorList = operatorService.selectOperatorList(map);

		model.addAttribute("operatorList", operatorList);
		model.addAttribute("operatorCount", operatorCount);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);

		model.addAttribute("startingSNo", from);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		return new ModelAndView("admin/admin/operatorManagement");
	}
	
	@RequestMapping(value = "/admin/updateOperatorOpenStatus.do")
	public String updateOperatorOpenStatus(@ModelAttribute("operatorVO") 
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			OperatorVO operatorVO, HttpServletRequest request) {
		
		try {
			int result = operatorService.updateOperatorOpenStatus(operatorVO);
			
			if (1 == result) {
				System.out.println("변경 성공");
			} else { 
				System.out.println("변경 실패  - Admin_Seq : " + operatorVO.getAdmin_Seq() + " / Open_Yn : " + operatorVO.getOpen_Yn());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/admin/operatorManagement.do?pn="+currentPage;
	}
	
	@RequestMapping(value = "/admin/searchUserInfo.do")
	public Object searchUserInfo(HttpServletRequest request,
			@RequestParam(value="searchText", required=false, defaultValue="") String searchText,
			@RequestParam(value="searchType", required=true, defaultValue="N") String searchType) {
		// searchType -> N = NickName / I = User Id
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchText", searchText);
		map.put("type", searchType);
		List<MemberVO> userList = null;
		try {
			userList = operatorService.searchEmployeeByType(map);
		} catch (Exception e) {
			System.out.println("[Error] " + e.getMessage());
			e.printStackTrace();
		}
		
		ModelAndView model = new ModelAndView("jsonV");
		if (null != userList && 0 < userList.size()) {
			model.addObject("userList", userList);
		}
		model.addObject("resultCode", 0);
		
		return model;
	}
	
	@RequestMapping(value = "/admin/insertOperatorInAdminPage.do")
	public String insertOperatorInAdminPage(HttpServletRequest request,
			@RequestParam(value="userInfo", required=false, defaultValue="") String userInfo,
			@RequestParam(value="registIP", required=true, defaultValue="0.0.0.0") String registIP) {
		
		if (!"".equals(userInfo)) {
			String [] data = userInfo.split("/");
			String userId = data[0].trim();
			String nickname = data[1].trim();
			try {
				OperatorVO operator = new OperatorVO();
				operator.setUser_Id(userId);
				operator.setNickname(nickname);
				operator.setProcess_Code(null);
				operator.setRegist_Ip(registIP);
				operatorService.insertOperator(operator);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return "redirect:/admin/operatorManagement.do";
	}
}