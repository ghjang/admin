package egovframework.happy.web.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.NoticeService;
import egovframework.happy.service.client.vo.FileVO;
import egovframework.happy.service.client.vo.NoticeVO;

/**
 * <pre>
 * [관리자페이지]
 * 공지사항 사항 관련 Controller
 * </pre>
 * @Class NoticeController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class NoticeController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page

	@Resource(name = "noticeService")
	private NoticeService noticeService;
	
	
	/**
	 * <pre>
	 * 공지사항 - 전체 공지사항 보기
	 * </pre>
	 * @Class NoticeController.java
	 * @Method notice
	 * @param currentPage
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/notice.do")
	public ModelAndView notice(@RequestParam(value="pn", required=false, defaultValue="1") int currentPage, ModelMap model) throws Exception {
		List<NoticeVO> list = null;
		
		// for paging
		int totalItems = 0;
		int itemSize = 0;
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		totalItems = noticeService.selectNoticeListTotCnt();
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, itemSize);
		
		HashMap<String, Integer> map = new HashMap<>();
		map.put("offset", from);
		map.put("pagesize", itemSize);
		
		list = noticeService.list(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("itemSize", itemSize);
		model.addAttribute("totalItems", totalItems);
		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		return new ModelAndView("admin/admin/notice", "noticeList", list);
	}
	
	
	/**
	 * <pre>
	 * 공지사항 - 공지사항 등록
	 * </pre>
	 * @Class NoticeController.java
	 * @Method registerArticle
	 * @param noticeVO
	 * @param model
	 * @param multiReq
	 * @param redirectAttributes
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/registerArticle.do")
	public String registerArticle(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model, MultipartHttpServletRequest multiReq, RedirectAttributes redirectAttributes) throws Exception {
		 //첨부파일 업로드
  		final Map<String, MultipartFile> files = multiReq.getFileMap();

  		System.out.println(files);
  		
  		if(! files.isEmpty()) {
  			List<FileVO> ListFileVo = fileUtils.parseMultiFileInfo(files, "NOTICE", "");
  			for(FileVO vo : ListFileVo) {
  				System.out.println("@@@@@@@@@@@@@@@ [vo:"+vo.toString()+"]");
  				commonService.insertFileInfo(vo);
  				System.out.println(" [getAttach_File_Seq:"+vo.getAttach_File_Seq()+"]");
  				noticeVO.setImage(vo.getAttach_File_Seq());
  			}
  		}
  		
//  		String content = noticeVO.getContent().replace("\r\n", "<br/>");
//  		noticeVO.setContent(content);
  		String content = noticeVO.getContent();
  		content = content.replace("<", "&lt;");
  		content = content.replace(">", "&gt;");
  		noticeVO.setContent(content);
  		
  		String title = noticeVO.getTitle();
  		title = title.replace("<", "&lt;");
  		title = title.replace(">", "&gt;");
  		noticeVO.setTitle(title);
  		
		noticeService.insertNotice(noticeVO);
		model.addAttribute("notice", noticeVO);
		return "redirect:/admin/notice.do";
	}
	
	/**
	 * 공지사항 - 공지사항 자세히 보기
	 * @param id
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/selectNoticeDetail.do")
	public String selectNoticeDetail(@RequestParam("id") int id, Model model) throws Exception {
		System.out.println("selectNoticeDetail");
		try {
			NoticeVO resultNotice = noticeService.selectNoticeDetailBySeq(id);
			model.addAttribute("notice", resultNotice);
		} catch (Exception e) {
			e.printStackTrace();
			log.debug("selectNoticeDetail err: " + e.getMessage());
		}
		return "redirect:/admin/notice.do";
	}
	
	/**
	 * 공지사항 - 공지사항 삭제 
	 * @param noticeVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/deleteNotice.do")
	public String deleteNotice(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model) throws Exception {
		System.out.println("deleteNotice");
		try {
			noticeService.deleteNotice(noticeVO);
		} catch (Exception e) {
			e.printStackTrace();
			log.debug("deleteNotice err: " + e.getMessage());
		}
		return "redirect:/admin/notice.do";
	}
	
	/**
	 * <pre>
	 * 공지사항 - 공지사항 수정
	 * </pre>
	 * @Class NoticeController.java
	 * @Method updateNotice
	 * @param noticeVO
	 * @param model
	 * @param multiReq
	 * @param redirectAttributes
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/updateNotice.do", method={RequestMethod.POST})
	public String updateNotice(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model, MultipartHttpServletRequest multiReq, RedirectAttributes redirectAttributes) throws Exception {
		System.out.println("updateNotice");

		 //첨부파일 업로드
  		final Map<String, MultipartFile> files = multiReq.getFileMap();

  		System.out.println(files);
  		
  		if(! files.isEmpty()) {
  			List<FileVO> ListFileVo = fileUtils.parseMultiFileInfo(files, "NOTICE", "");
  			for(FileVO vo : ListFileVo) {
  				System.out.println("@@@@@@@@@@@@@@@ [vo:"+vo.toString()+"]");
  				commonService.insertFileInfo(vo);
  				System.out.println(" [getAttach_File_Seq:"+vo.getAttach_File_Seq()+"]");
  				noticeVO.setImage(vo.getAttach_File_Seq());
  			}
  		}
		
		noticeService.updateNotice(noticeVO);
		return "redirect:/admin/notice.do";
	}
	
	/**
	 * <pre>
	 * 공지사항 - 공개여부 수정
	 * </pre>
	 * @Class NoticeController.java
	 * @Method updateOpenInfo
	 * @param noticeVO
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/updateOpenInfo.do")
	public String updateOpenInfo(@ModelAttribute("noticeVO") NoticeVO noticeVO, Model model) throws Exception {
		System.out.println("registerArticle noviceVO: " + noticeVO);
		
		noticeService.updateOpenInfo(noticeVO);
		return "redirect:/admin/notice.do";
	}


}
