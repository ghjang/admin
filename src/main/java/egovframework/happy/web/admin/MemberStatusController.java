package egovframework.happy.web.admin;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.happy.cmmn.AES256Util;
import egovframework.happy.cmmn.ExcelUtil;
import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.MemberService;
import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.TakeLectureVO;


/**
 * <pre>
 * [관리자페이지]
 * 회원현황 관련 Controller
 * </pre>
 * @Class MemberStatusController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class MemberStatusController extends BaseController{
	private static final int ITEMS_PER_PAGE = 10; // rows per page
	
	@Resource(name = "memberService")
	private MemberService memberService;
	
	/**
	 * <pre>
	 * 회원현황 - 리스트보여주기
	 * </pre>
	 * @Class MemberStatusController.java
	 * @Method memberStatus
	 * @param model
	 * @param currentPage
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/memberStatus.do",  method = {RequestMethod.POST, RequestMethod.GET})
	public Object memberStatus(ModelMap model,@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("memberStatus-------------------------");
		
		String search = request.getParameter("search") == null ? "" : request.getParameter("search");
		String excel = request.getParameter("excel") == null ? "" : request.getParameter("excel");
		String ordering = request.getParameter("ordering") == null? "Regist_Date" : request.getParameter("ordering");
		
//		model.addAttribute("join_yn", commonService.selectCode("JOIN_YN"));
		
		HashMap<String, Object> map = new HashMap<>();
		List<MemberVO> list = null;
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		if(!search.equals("Y")) {
			System.out.println("default 화면");
			map = new HashMap<>();
			
			//엑셀다운로드
			if(excel.equals("Y")) {
				try {
					list = memberService.list(map);
				} catch (Exception e) {
					System.out.println("memberDownload err: " +e.getMessage());
					log.debug("memberDownload err: " +e.getMessage());
				}
				
				for (MemberVO vo : list) {
					if (null != vo.getMobile_No() && 0 != vo.getMobile_No().length)
						vo.setMobile(AES256Util.decrypt(vo.getMobile_No()));
				}
				
				String fileStr = "";
				ExcelUtil excelUtil = new ExcelUtil(propertiesService);
				fileStr = excelUtil.excelMemberStatus(list);
				File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
				
				return new ModelAndView("download", "downloadFile", file);
			}
			
			totalItems = memberService.memberTotCnt();
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			map.put("ordering", ordering);
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			
			list = memberService.list(map);
			model.addAttribute("searchCnt", memberService.memberTotCnt());
			model.addAttribute("chk_ordering", ordering);
		} else {
			System.out.println("검색 화면");
			map = new HashMap<>();
			
			String[] memClassification = request.getParameterValues("member_classification");
			String[] courseProgress = request.getParameterValues("course_progress");
			String[] processOpening = request.getParameterValues("process_opening");
			String start_period = request.getParameter("start_period") == null ? "" : request.getParameter("start_period");
			String end_period = request.getParameter("end_period") == null ? "" : request.getParameter("end_period");
			
			String searchCategory = request.getParameter("searchCategory");
			String content = request.getParameter("content");
						
			if(content != null && content != "") {
				map.put("searchCategory", searchCategory);
				map.put("content", content);
				if(searchCategory.equals("Mobile_No")) {
					String phone = AES256Util.encrypt(content);
					map.put("content", phone);
				}
			}
			
			map.put("memClassification", memClassification);
			map.put("courseProgress", courseProgress);
			map.put("processOpening", processOpening);
			map.put("ordering", ordering);
			map.put("start_period", start_period + " 00:00:00.000");
			map.put("end_period", end_period + " 23:59:59.999");
			
//			totalItems = memberService.searchCnt(map);
			totalItems =  memberService.selectList(map).size();
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			if(excel.equals("Y")) {
				try {
					list = memberService.selectList(map);
				} catch (Exception e) {
					System.out.println("memberDownload err: " +e.getMessage());
					log.debug("memberDownload err: " +e.getMessage());
				}

				for (MemberVO vo : list) {
					if (null != vo.getMobile_No() && 0 != vo.getMobile_No().length)
						vo.setMobile(AES256Util.decrypt(vo.getMobile_No()));
				}
				
				String fileStr = "";
				ExcelUtil excelUtil = new ExcelUtil(propertiesService);
				fileStr = excelUtil.excelMemberStatus(list);
				File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
				
				return new ModelAndView("download", "downloadFile", file);
			}
			
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			
			list = memberService.selectList(map);
			
			model.addAttribute("searchCnt", totalItems);
			
			model.addAttribute("search", "Y");
			model.addAttribute("chk_start", start_period);
			model.addAttribute("chk_end", end_period);
			model.addAttribute("chk_memClass", memClassification);
			model.addAttribute("chk_courseProgress", courseProgress);
			model.addAttribute("chk_processOpening", processOpening);
			model.addAttribute("chk_ordering", ordering);
			model.addAttribute("chk_searchCategory", searchCategory);
			model.addAttribute("searchText", content);
		}
		model.addAttribute("memberTotCnt", memberService.memberTotCnt());
		model.addAttribute("memJoinNewCnt", memberService.memJoinNewCnt());
		model.addAttribute("memJoinOldCnt", memberService.memJoinOldCnt());
		
		//paging 화면 설정 시작
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		//paging 화면 설정 끝

		for (MemberVO vo : list) {
			if (null != vo.getMobile_No() && 0 != vo.getMobile_No().length)
				vo.setMobile(AES256Util.decrypt(vo.getMobile_No()));
		}
		
		model.addAttribute("memberList", list);
		return "admin/admin/memberStatus";
	}
	
	/**
	 * <pre>
	 * 회원현황 - 프로필 자세히 보기
	 * </pre>
	 * @Class MemberStatusController.java
	 * @Method profileImage
	 * @param model
	 * @param memberVO
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/member/modal/profileImage.do")
	public String profileImage(ModelMap model, @ModelAttribute("memberVO") MemberVO memberVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
//		System.out.println(memberVO);
		model.addAttribute("member", memberVO);
		return "admin/modal/profileImage";
	}
	
	/**
	 * <pre>
	 * 회원현황 - 수강 과정 자세히보기
	 * </pre>
	 * @Class MemberStatusController.java
	 * @Method takeLectureDetail
	 * @param model
	 * @param memberVO
	 * @param currentPage
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/member/modal/takeLectureDetail.do")
	public String takeLectureDetail(ModelMap model, @ModelAttribute("memberVO") MemberVO memberVO, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage,HttpServletRequest request, HttpServletResponse response) 
			throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		map.put("User_Id", memberVO.getUser_Id());
		
		List<TakeLectureVO> takeLectureList = memberService.selectTakeLectureById(map);
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		if(takeLectureList != null){
			
			// for paging
			totalItems = takeLectureList.size();
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			
			takeLectureList = memberService.selectTakeLectureById(map);
		} else {
			
		}
		
		model.addAttribute("totalItems", totalItems);
		//paging 화면 설정 시작
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		//paging 화면 설정 끝
		
		model.addAttribute("takeLectureList", takeLectureList);
		model.addAttribute("member", memberVO);
		return "admin/modal/takeLectureDetail";
	}
	

	/**
	 * <pre>
	 * 회원현황 - 개설 과정 자세히보기
	 * </pre>
	 * @Class MemberStatusController.java
	 * @Method makeLectureDetail
	 * @param model
	 * @param memberVO
	 * @param currentPage
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/member/modal/makeLectureDetail.do")
	public String makeLectureDetail(ModelMap model, @ModelAttribute("memberVO") MemberVO memberVO, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage, HttpServletRequest request, HttpServletResponse response) 
			throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		map.put("User_Id", memberVO.getUser_Id());

		List<ProcessDTO> processList = memberService.selectProcessById(map);
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		if(processList != null){
			// for paging
			totalItems = processList.size();
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			map.put("from", from);
			map.put("itemsPerPage", itemSize);
			
			processList = memberService.selectProcessById(map);
		} else {
			
		}
		model.addAttribute("totalItems", totalItems);
		//paging 화면 설정 시작
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		//paging 화면 설정 끝
		
		model.addAttribute("member", memberVO);
		model.addAttribute("processList", processList);
		return "admin/modal/makeLectureDetail";
	}
}
