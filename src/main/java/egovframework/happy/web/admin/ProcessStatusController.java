package egovframework.happy.web.admin;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.happy.cmmn.ExcelUtil;
import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.ProcessService;
import egovframework.happy.service.client.vo.ProcessBlockVO;
import egovframework.happy.service.client.vo.ProcessDTO;
import egovframework.happy.service.client.vo.ProcessVO;
import egovframework.happy.service.client.vo.TagVO;
import egovframework.happy.service.client.vo.TakeLectureReviewVO;
import egovframework.happy.service.client.vo.TakeLectureVO;

/**
 * <pre>
 * [관리자페이지]
 * 과정현황 관련 Controller
 * </pre>
 * @Class ProcessStatusController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class ProcessStatusController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Resource(name = "processService")
	private ProcessService processService;
	
	/**
	 * <pre>
	 * 과정현황 - 전체 과정 보기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method processStatus
	 * @param model
	 * @param currentPage 현재 페이지
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/processStatus.do")
	public ModelAndView processStatus(ModelMap model, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage, HttpServletRequest request) throws Exception {
		System.out.println("processStatus------------------------");
		
		String search = request.getParameter("search") == null ? "" : request.getParameter("search");
		String excel = request.getParameter("excel") == null ? "" : request.getParameter("excel");
		String ordering = request.getParameter("ordering") == null? "Regist_Date" : request.getParameter("ordering");
		String content = request.getParameter("content") == null? "" : request.getParameter("content");
		String searchCategory = request.getParameter("searchCategory");
		System.out.println("excel: " + excel);
		
		int processDefaultCount = processService.processDefaultCnt();
		
		//검색화면 세팅
		model.addAttribute("field_code", commonService.selectCode("FIELD_CODE"));
		model.addAttribute("open_code", commonService.selectCode("OPEN_YN"));
		model.addAttribute("processDefaultCnt", processDefaultCount);
		
		HashMap<String, Object> map = new HashMap<>();
		List<ProcessDTO> list = null;
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
//		System.out.println("ordering: " + ordering);
		
		if(!search.equals("Y")) {
			log.debug("processStatus] default 화면, 검색결과 수: " + processDefaultCount);
			
			//엑셀 다운로드
			if(excel.equals("Y")) {
				try {
					list = processService.list(map);
				} catch (Exception e) {
					System.out.println("processDownload err: " +e.getMessage());
					log.debug("processDownload err: " +e.getMessage());
				}
				
				//Tag setting
				HashMap<String, List<TagVO>> tagMap = new HashMap<>();
				for (ProcessDTO processDTO : list) {
					tagMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
				}
				
				//차단 사유 세팅
				HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
				for (ProcessDTO processDTO : list) {
					blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
				}
				String fileStr = "";
				ExcelUtil excelUtil = new ExcelUtil(propertiesService);
				fileStr = excelUtil.excelProcessStatus(list, tagMap, blockMap);
				
				File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
				return new ModelAndView("download", "downloadFile", file);
			}
			//엑셀 다운로드

			model.addAttribute("searchCnt", processDefaultCount);
			
			totalItems = processDefaultCount;	//count 결과 넣어줘야함 
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			map.put("offset", from);
			map.put("pagesize", itemSize);
			map.put("ordering", ordering);
			
			list = processService.list(map);
			model.addAttribute("chk_Order", ordering);

		} else {
			System.out.println("검색화면");
			String[] Process_Total_Grade_list = request.getParameterValues("Process_Total_Grade");
			String[] Field_Code_list = request.getParameterValues("Field_Code");
			String[] Open_Yn_list = request.getParameterValues("Open_Yn");
			String[] Free_Yn_list = request.getParameterValues("Free_Yn");
			String[] Block_Yn_list = request.getParameterValues("Block_Yn");
			
//			String Block_Yn = request.getParameter("Block_Yn") == null ? "N" : request.getParameter("Block_Yn");
			String start_period = request.getParameter("start_period") == null ? "" : request.getParameter("start_period");
			String end_period = request.getParameter("end_period") == null ? "" : request.getParameter("end_period");
			
			map.put("Process_Total_Grade", Process_Total_Grade_list);
			map.put("Field_Code", Field_Code_list);
			map.put("Open_Yn", Open_Yn_list);
			map.put("Block_Yn", Block_Yn_list);
			map.put("Free_Yn", Free_Yn_list);
			map.put("start_period", start_period + " 00:00:00.000");
			map.put("end_period", end_period + " 23:59:59.999");
			map.put("ordering", ordering);
			
			//엑셀 다운로드 start
			if(excel.equals("Y")) {
				try {
					list = processService.list(map);
				} catch (Exception e) {
					System.out.println("memberDownload err: " +e.getMessage());
					log.debug("processDownload err: " +e.getMessage());
				}
				
				//Tag setting
				HashMap<String, List<TagVO>> tagMap = new HashMap<>();
				for (ProcessDTO processDTO : list) {
					tagMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
				}
				
				//차단 사유 세팅
				HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
				for (ProcessDTO processDTO : list) {
					blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
				}
				String fileStr = "";
				ExcelUtil excelUtil = new ExcelUtil(propertiesService);
				fileStr = excelUtil.excelProcessStatus(list, tagMap, blockMap);
				
				File file = new File(propertiesService.getString("UPLOAD.FILE.PATH") + fileStr);
				
				return new ModelAndView("download", "downloadFile", file);
			}
			//엑셀 다운로드 end
			
			if(searchCategory.equals("Tag")) {
				System.out.println("Tag");
				HashMap<String, String> tempMap = new HashMap<>();
				tempMap.put("content", content);
				List<String> searchedTagProcessCodeList = processService.searchTagList(tempMap);
				if(searchedTagProcessCodeList.size() == 0) {
					searchedTagProcessCodeList.add("");
				}

				map.put("searchedTagProcessCodeList", searchedTagProcessCodeList);
			}

			map.put("searchCategory", searchCategory);
			map.put("content", content);

//			int searchCnt = processService.selectSerchCnt(map);
			int searchCnt = processService.list(map).size();
			System.out.println("검색결과 수: "+ searchCnt);
			model.addAttribute("searchCnt", searchCnt);
			
			totalItems = searchCnt;
			numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
			currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
			from = Paging.getStartingIndex(currentPage, itemSize);
			
			map.put("offset", from);
			map.put("pagesize", itemSize);
			
			list = processService.list(map);

			//체크된 애들 값 보내줌
			model.addAttribute("chk_start", start_period);
			model.addAttribute("chk_end", end_period);
			model.addAttribute("chk_Grade", Process_Total_Grade_list);
			model.addAttribute("chk_Field", Field_Code_list);
			model.addAttribute("chk_Open", Open_Yn_list);
			model.addAttribute("chk_Block", Block_Yn_list);
			model.addAttribute("chk_Free", Free_Yn_list);
			model.addAttribute("chk_Order", ordering);
			model.addAttribute("chk_searchCategory", searchCategory);
			model.addAttribute("searchText", content);
			model.addAttribute("search", "Y");
		} //ifelse(search == null)
		
		//Tag setting
		HashMap<String, List<TagVO>> resultMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			resultMap.put(processDTO.getProcess_Code(), processService.selectTagList(processDTO));
		}
		//차단 사유 세팅
		HashMap<String, List<ProcessBlockVO>> blockMap = new HashMap<>();
		for (ProcessDTO processDTO : list) {
			blockMap.put(processDTO.getProcess_Code(), processService.selectBlockReason(processDTO.getProcess_Code()));
		}
		model.addAttribute("blockMap", blockMap);
		
		//paging 화면 설정 시작
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		//paging 화면 설정 끝
		
		model.addAttribute("processList", list);
		
		return new ModelAndView("admin/admin/processStatus", "resultMap", resultMap);
	}
	
	/**
	 * <pre>
	 * 과정현황 - 태그 자세히 보기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method TagDetail
	 * @param processVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/process/modal/TagDetail.do")
	public String TagDetail(@ModelAttribute("processVO") ProcessVO processVO, ModelMap model) throws Exception {
		List<TagVO> tagList = processService.selectAllTagList(processVO);
		model.addAttribute("tagList",tagList);
		return "admin/modal/tagDetail";
	}
	
	/**
	 * <pre>
	 * 과정현황 - 수강후기 내용 보기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method reviewDetail
	 * @param request
	 * @param processDTO
	 * @param model
	 * @param currentPage
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/process/modal/reviewDetail.do")
	public String reviewDetail(HttpServletRequest request,
			@ModelAttribute("processDTO") ProcessDTO processDTO, ModelMap model, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		System.out.println("reviewDetail----------------------------------------");
		
		System.out.println("processDTO- Process_Code: "+processDTO.getProcess_Code());
		System.out.println("processDTO- TakeLecture_Review_Count: "+processDTO.getTakeLecture_Review_Count());

		model.addAttribute("processDTO", processDTO);
		List<TakeLectureReviewVO> reviewList = null;
		
		// for paging
		int totalItems = processDTO.getTakeLecture_Review_Count();
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, itemSize);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("ProcessDTO", processDTO);
		map.put("offset", from);
		map.put("pagesize", itemSize);
		
		reviewList = processService.selectReviewList(map);

		//reply 붙이기
		HashMap<Integer, List<TakeLectureReviewVO>> resultMap = new HashMap<>();
		for (TakeLectureReviewVO takeLectureReviewVO : reviewList) {
			resultMap.put(takeLectureReviewVO.getTakeLecture_Review_Seq(), processService.selectReviewTailList(takeLectureReviewVO));
		}
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		model.addAttribute("reviewList",reviewList);
		model.addAttribute("resultMap",resultMap);

		return "admin/modal/reviewDetail";
	}
	

	/**
	 * <pre>
	 * 과정현황 - 전체 수강자 보기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method takeLecturePeople
	 * @param processDTO
	 * @param currentPage
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/process/modal/takeLecturePeople.do")
	public String takeLecturePeople(@ModelAttribute("processDTO") ProcessDTO processDTO, ModelMap model, @RequestParam(value="pn", required=false, defaultValue="1") int currentPage) throws Exception {
		List<TakeLectureVO> lectureList = null;
		
		// for paging
		int totalItems = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}

		totalItems = processService.takeLecturePeopleCnt(processDTO);
		
		int numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, itemSize);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("ProcessDTO", processDTO);
		map.put("offset", from);
		map.put("pagesize", itemSize);
		
		lectureList = processService.selectTakeLectureList(map);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize; // Starting serial number in list

		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		model.addAttribute("lectureList",lectureList);
		return "admin/modal/takeLecturePeople";
	}
	

	/**
	 * <pre>
	 * 과정현황 - 과정의 공개 설정 변경
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method updateProcessOpenStatus
	 * @param processDTO
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/updateProcessOpenStatus.do")
	public Object updateProcessOpenStatus(@ModelAttribute("processDTO") ProcessDTO processDTO, Model model, HttpServletRequest request) throws Exception {
		processDTO.setBlock_Yn("N");
		System.out.println("updateProcessOpenStatus.do");
//		System.out.println(processDTO);
		int updateOpenResult = processService.updateOpenStatus(processDTO);
		ModelAndView mav = new ModelAndView("jsonV");
		mav.addObject("updateOpenResult", updateOpenResult);
		return mav;
	}
	
	/**
	 * <pre>
	 * 과정현황 - 차단 메일 보내기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method sendEmail
	 * @param processBlockVO
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/sendEmail.do")
	public String sendEmail(@ModelAttribute("processBlockVO") ProcessBlockVO processBlockVO, Model model, HttpServletRequest request) throws Exception {
		System.out.println(processBlockVO.getProcess_Code() + ", " + request.getParameter("Block_Yn"));
		System.out.println(processBlockVO.getEmail() + ", " + processBlockVO.getContent());
		
		ProcessDTO processDTO = new ProcessDTO();
		processDTO.setPublic_Yn("N");
		processDTO.setProcess_Code(processBlockVO.getProcess_Code());
		
		String fromEmail = "help@hhc.ac";
		String toEmail = processBlockVO.getEmail();
		String content = processBlockVO.getContent();
		String title = "[해피칼리지] 제작하신 과정이 관리자에 의해 차단되었습니다.";
		
		try{
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
//			messageHelper.setFrom(new InternetAddress(fromEmail));
			messageHelper.setFrom(fromEmail, "해피칼리지");
			messageHelper.setTo(toEmail);
			messageHelper.setSubject(title);
			messageHelper.setText(content);
			
			processService.updateBlockStatus(processBlockVO);
			processService.insertProcessBlock(processBlockVO);
			processService.updateOpenStatus(processDTO);
			mailSender.send(message);
		} catch(MailException me) {
			System.out.println("mailSender.send error: " + me.getMessage());
			System.out.println("차단메일 재전송 필요");
		} catch(Exception e) {
			System.out.println("sendEmail err: " + e.getMessage());
		}
		
		return "redirect:/admin/processStatus.do";
	}
	
	/**
	 * <pre>
	 * 과정현황 - 차단 메일 보내는 폼 띄우기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method sendEmailBlocking
	 * @param processDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/process/modal/sendEmailBlocking.do")
	public String sendEmailBlocking(@ModelAttribute("processDTO") ProcessDTO processDTO, ModelMap model) throws Exception {
		System.out.println("차단메일보내기: " + processDTO.getProcess_Code());
		model.addAttribute("process", processDTO);
		return "admin/modal/sendEmailBlocking";
	}
	
	/**
	 * <pre>
	 * 과정현황 - 차단 사유 자세히 보기
	 * </pre>
	 * @Class ProcessStatusController.java
	 * @Method blockReason
	 * @param processBlockVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/process/modal/blockReason.do")
	public String blockReason(@ModelAttribute("processBlockVO") ProcessBlockVO processBlockVO, ModelMap model) throws Exception {
		model.addAttribute("block", processService.selectBlockReasonDetail(processBlockVO));
		return "admin/modal/blockReasonDetail";
	}
	
}
