package egovframework.happy.web.admin;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.CurationService;
import egovframework.happy.service.admin.basic.ProcessService;
import egovframework.happy.service.client.vo.CurationProcessVO;
import egovframework.happy.service.client.vo.CurationVO;
import egovframework.happy.service.client.vo.ProcessDTO;


/**
 * <pre>
 * [관리자페이지]
 * 큐레이션 관련 Controller
 * </pre>
 * @Class CurationController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class CurationController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page

	@Resource(name = "curationService")
	private CurationService curationService;

	@Resource(name = "processService")
	private ProcessService processService;

	/**
	 * <pre>
	 * 큐레이션 - 전체 큐레이션 보기
	 * </pre>
	 * @Class CurationController.java
	 * @Method curation
	 * @param currentPage
	 * @param model
	 * @return 큐레이션 View
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation.do")
	public ModelAndView curation(@RequestParam(value = "pn", required = false, defaultValue = "1") int currentPage, ModelMap model) throws Exception {
		List<CurationVO> curationList = null;

		// for paging
		int totalItems = curationService.curationTotCnt();
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}

		int numberOfPages = Paging.getNumberOfPages(totalItems, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		int from = Paging.getStartingIndex(currentPage, itemSize);

		HashMap<String, Object> map = new HashMap<>();
		map.put("offset", from);
		map.put("pagesize", itemSize);

		curationList = curationService.selectList(map);
		List<CurationProcessVO> cpList = null;
		if(null != curationList) {
			if(curationList.size() > 0 ) {
				cpList = curationService.selectCurationProcessBySeq(curationList);
			}
		}

		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);
		int startingSNo = (currentPage - 1) * itemSize;
		
		model.addAttribute("startingSNo", startingSNo);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		model.addAttribute("totalItems", totalItems);

		model.addAttribute("cpList", cpList);
		return new ModelAndView("admin/admin/curation", "curationList", curationList);
	}


	/**
	 * <pre>
	 * 큐레이션 - 큐레이션 등록/수정 화면 띄우기
	 * </pre>
	 * @Class CurationController.java
	 * @Method registerCurationView
	 * @param curationVO
	 * @param processList
	 * @param request
	 * @param model
	 * @return 큐레이션 등록/수정 View
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/registerCurationView.do")
	public String registerCurationView(@ModelAttribute("curationVO") CurationVO curationVO, @ModelAttribute("processList") ArrayList<ProcessDTO> processList, HttpServletRequest request, Model model)
			throws Exception {
		System.out.println("registerCurationView-------------------");
		System.out.println("curationVO: " + curationVO.getCuration_Seq() + ", " + curationVO.getCuration_Nm());
		List<CurationProcessVO> cpList = null;
		model.addAttribute("curation", curationVO);
		if (null != request.getParameter("Curation_Seq") || 0 != curationVO.getCuration_Seq()) {
			CurationVO c = curationService.selectCurationBySeq(curationVO);
			System.out.println("c:" + c);
			List<CurationVO> temp = new ArrayList<>();
			temp.add(c);
			System.out.println("processList size: " + processList.size());
			cpList = curationService.selectCurationProcessBySeq(temp);

			model.addAttribute("curation", c);
			System.out.println("cpList size: " + cpList.size());
			System.out.println("edit: " + request.getParameter("edit"));
			if (null != request.getParameter("edit")) {
				model.addAttribute("processList", cpList);
				return "admin/admin/registerCuration";
			}
			// model.addAttribute("cpList", cpList);
		}
		model.addAttribute("processList", processList);

		return "admin/admin/registerCuration";
	}


	/**
	 * <pre>
	 * 큐레이션 - 큐레이션 등록/수정하기
	 * </pre>
	 * @Class CurationController.java
	 * @Method registerCuration
	 * @param curationVO
	 * @param curationProcessVO
	 * @param request
	 * @return "redirect:/admin/curation.do"
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/registerCuration.do")
	public String registerCuration(@ModelAttribute("curationVO") CurationVO curationVO, @ModelAttribute("curationProcessVO") CurationProcessVO curationProcessVO, HttpServletRequest request)
			throws Exception {
		System.out.println("registerCuration-------------");
		
		String nm2 = curationVO.getCuration_Nm();
		String intro = curationVO.getLine_Introduction();
		
		if(nm2.indexOf("/BR/") >= 0) {
			nm2 = nm2.replace("/BR/", "<br>");
			curationVO.setCuration_Nm(nm2);
			System.out.println("nm2: " + curationVO.getCuration_Nm());
		}
		if(intro.indexOf("/BR/") >=0) {
			intro = intro.replace("/BR/", "<br>");
			curationVO.setLine_Introduction(intro);
			System.out.println("intro: " + curationVO.getLine_Introduction());
		}
		
		List<CurationProcessVO> cpList = new ArrayList<>();
		CurationProcessVO temp = null;
		String[] orderArray = request.getParameterValues("CurationProcessOrder");
		if (null != orderArray) {
			String[] process_Code = curationProcessVO.getProcess_Code().split(",");
			for (int i = 0; i < orderArray.length; i++) {
				temp = new CurationProcessVO();
				temp.setProcess_Code(process_Code[i]);
				temp.setCuration_Process_Order(Integer.parseInt(orderArray[i]));
				cpList.add(temp);
			}
		}
		System.out.println("cpList: " + cpList.size());

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS");
		Date date;

		String startDay = request.getParameter("start_day");
		String endDay = request.getParameter("end_day");
		String Curation_seq = request.getParameter("seq");
		System.out.println("seq:" + Curation_seq);

		date = dateFormat.parse(startDay + " 00:00:00.000");
		Timestamp ts = new Timestamp(date.getTime());
		curationVO.setExpression_Start_Day(ts);

		date = dateFormat.parse(endDay + " 23:59:59.999");
		ts = new Timestamp(date.getTime());
		curationVO.setExpression_CLOSE_Day(ts);

		if (!"".equalsIgnoreCase(Curation_seq) && !"0".equals(Curation_seq)) {
			System.out.println("[큐레이션 수정]: " + Curation_seq);
			curationVO.setCuration_Seq(Integer.parseInt(Curation_seq));
			System.out.println("수정: " + curationVO);
			if (0 < cpList.size()) {
				for (int i = 0; i < cpList.size(); i++) {
					cpList.get(i).setCuration_Seq(Integer.parseInt(Curation_seq));
				}
			}
			System.out.println("cpList size: " + cpList.size());
			System.out.println("cpList: " + cpList);
			curationService.updateCuration(curationVO);

			int result2 = curationService.deleteCurationProcess(Integer.parseInt(Curation_seq));
			System.out.println("Curation_seq: " + Curation_seq + ", delete 수: " + result2);
			if (!cpList.isEmpty()) {
				int result = curationService.insertCurationProcess(cpList);
				System.out.println("insert 수?: " + result);
			}

		} else {
			System.out.println("[큐레이션 등록]: " + Curation_seq);
			
			curationService.insertCuration(curationVO);
			int lastSeq = curationService.lastInsertCurationSeq();
			System.out.println("lastInsertSeq: " + lastSeq);
			if (0 < cpList.size()) {
				for (int i = 0; i < cpList.size(); i++) {
					cpList.get(i).setCuration_Seq(lastSeq);
				}
			}
			if (!cpList.isEmpty()) {
				int result = curationService.insertCurationProcess(cpList);
				System.out.println("insert 수?: " + result);
			}
		}
		return "redirect:/admin/curation.do";
	}
	
	/**
	 * <pre>
	 * 큐레이션 - 큐레이션과정 등록하기
	 * </pre>
	 * @Class CurationController.java
	 * @Method registerCurationProcessList
	 * @param model
	 * @param processDTO
	 * @param request
	 * @return registerCurationView(curation, processList, request, model)
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/registerCurationProcessList.do", method = { RequestMethod.POST })
	public Object registerCurationProcessList(Model model, @ModelAttribute("process") ProcessDTO processDTO,
			HttpServletRequest request) throws Exception {
		System.out.println("registerCurationProcessList---------------");

		ArrayList<ProcessDTO> processList = new ArrayList<>();
		if (null != processDTO.getProcess_Code()) {
			String[] codeArray = processDTO.getProcess_Code().split(",");
			String[] nameArray = processDTO.getProcess_Nm().split(",");
			String[] openArray = processDTO.getPublic_Yn().split(",");
			String[] blockArray = processDTO.getBlock_Yn().split(",");
			System.out.println("codeArray.length: " + codeArray.length);
			ProcessDTO temp = null;
			if (0 < codeArray.length) {
				for (int i = 0; i < codeArray.length; i++) {
					temp = new ProcessDTO();
					temp.setProcess_Code(codeArray[i]);
					temp.setProcess_Nm(nameArray[i]);
					temp.setPublic_Yn(openArray[i]);
					temp.setBlock_Yn(blockArray[i]);
					processList.add(temp);
				}
			}
		}
		System.out.println("processList.size(): " + processList.size());
		String cSeq = request.getParameter("chkCuration_Seq");
		System.out.println("Curation_Seq: " + cSeq);
		CurationVO curation = new CurationVO();
		if (null != cSeq) {
			if (!"".equals(cSeq) && !"0".equals(cSeq)) {
				curation.setCuration_Seq(Integer.parseInt(cSeq));
				curation = curationService.selectCurationBySeq(curation);
			}
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS");
		Date date;

		String startDay = request.getParameter("chkstart_period");
		if(null != startDay && !startDay.equals("")) {
			date = dateFormat.parse(startDay + " 00:00:00.000");
			Timestamp ts = new Timestamp(date.getTime());
			curation.setExpression_Start_Day(ts);
		}
		
		String endDay = request.getParameter("chkend_period");
		if(null != endDay && !endDay.equals("")) {
			date = dateFormat.parse(endDay + " 00:00:00.000");
			Timestamp ts = new Timestamp(date.getTime());
			curation.setExpression_CLOSE_Day(ts);
		}

		curation.setCuration_Nm(request.getParameter("chkCuration_Nm"));
		curation.setLine_Introduction(request.getParameter("chkCuration_Intro"));
		curation.setUse_Yn(request.getParameter("chkUse_Yn"));
		
		System.out.println("asdlfkjaslkj" + curation);
		
		model.addAttribute("curationVO", curation);
		// return "redirect:/admin/registerCurationView.do";
		return registerCurationView(curation, processList, request, model);
	}

	/**
	 * <pre>
	 * 큐레이션 - 큐레이션 과정 등록/수정 모달 띄우기
	 * </pre>
	 * @Class CurationController.java
	 * @Method curationProcessAddView
	 * @param model
	 * @param curationVO
	 * @param request
	 * @param session
	 * @return "curationProcessAddView"
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation/modal/curationProcessAddView.do")
	public String curationProcessAddView(ModelMap model, @ModelAttribute("CurationVO") CurationVO curationVO,
			HttpServletRequest request, HttpSession session) throws Exception {
		System.out.println("curationProcessAddView------------");
		String curationSeq = request.getParameter("Curation_Sequence");
		if(curationSeq.equals("")) {
			curationSeq = "0";
		}
		String chkCp = request.getParameter("chkCpList");
		System.out.println("curationSeq: " + curationSeq);
		System.out.println("chkCpList: " + chkCp);
		curationVO.setCuration_Seq(Integer.parseInt(curationSeq));
		
		Date date;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS");
		
		String startDay = request.getParameter("start_period");
		String endDay = request.getParameter("end_period");
		if(null != startDay && !startDay.equals("")) {
			date = dateFormat.parse(startDay + " 00:00:00.000");
			Timestamp ts = new Timestamp(date.getTime());
			curationVO.setExpression_Start_Day(ts);
		}
		if(null != endDay && !endDay.equals("")) {
			date = dateFormat.parse(endDay + " 23:59:59.999");
			Timestamp ts = new Timestamp(date.getTime());
			curationVO.setExpression_CLOSE_Day(ts);
		}
		
		model.addAttribute("chkCuration", curationVO);
		request.setAttribute("chkCuration", curationVO);
		
		System.out.println("RIRI: "+curationVO);
		
		String[] chkCpList;
		List<CurationProcessVO> list = new ArrayList<>();
		ProcessDTO pDTO;
		CurationProcessVO cpVO;
		if (null != chkCp && !"".equals(chkCp)) {
			chkCpList = chkCp.split("::");
			for (int i = 0; i < chkCpList.length; i++) {
				cpVO = new CurationProcessVO();

				pDTO = curationService.selectProcessByCode(chkCpList[i]);
				cpVO.setBlock_Yn(pDTO.getBlock_Yn());
				cpVO.setPublic_Yn(pDTO.getPublic_Yn());
				cpVO.setProcess_Code(pDTO.getProcess_Code());
				cpVO.setProcess_Nm(pDTO.getProcess_Nm());
				cpVO.setCuration_Process_Order((i + 1));

				list.add(cpVO);
			}
			model.addAttribute("cpList", list);
		}

		/*
		// 수정시, 원래 가지고 있던 Curation Process 목록 보여줌
		if (!"".equals(curationSeq) && !"0".equals(curationSeq)) {
			List<CurationVO> temp = new ArrayList<>();
			CurationVO c = new CurationVO();
			c.setCuration_Seq(Integer.parseInt(curationSeq));
			temp.add(c);
			List<CurationProcessVO> cpList = curationService.selectCurationProcessBySeq(temp);
			model.addAttribute("Curation_Seq", curationSeq);
			model.addAttribute("listSize", cpList.size());
			model.addAttribute("cpList", cpList);
			System.out.println("cpList.size: " + cpList.size());
		}
		*/

		// 과정 검색을 위한
		List<ProcessDTO> processList = processService.selectProcessList();
		model.addAttribute("processList", processList);
		return "admin/modal/curationProcessAddView";
	}

	/**
	 * <pre>
	 * 큐레이션 - 큐레이션과정 추가하기
	 * </pre>
	 * @Class CurationController.java
	 * @Method curationProcessAdd
	 * @param model
	 * @param request
	 * @return "jsonV"
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation/modal/curationProcessAdd.do")
	public Object curationProcessAdd(ModelMap model, HttpServletRequest request) throws Exception {
		String searchText = request.getParameter("searchText") == null? "" : request.getParameter("searchText");
		String pCode = searchText.substring(0, 10);

		ProcessDTO processDTO = null;
		if(pCode.length() > 0) {
			processDTO = curationService.selectProcessByCode(pCode);
		}

		int resultCode = 0;
		if(null == processDTO) {
			resultCode = 1;
		}
		
		ModelAndView mav = new ModelAndView("jsonV");
		mav.addObject("process", processDTO);
		mav.addObject("resultCode", resultCode);
		return mav;
	}
	
	/**
	 * <pre>
	 * 큐레이션 - 큐레이션과정에 보여줄 과정리스트 전달
	 * </pre>
	 * @Class CurationController.java
	 * @Method searchCurationProcessInfo
	 * @param request
	 * @param searchText
	 * @return 
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation/modal/searchCurationProcessInfo.do")
	public Object searchCurationProcessInfo(HttpServletRequest request,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) throws Exception {
		String searchType = request.getParameter("searchType");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchText", searchText);
		map.put("searchType", searchType);
		List<ProcessDTO> processList = null;
		try {
			processList = curationService.selectProcessByType(map);
		} catch (Exception e) {
			System.out.println("[Error] " + e.getMessage());
			e.printStackTrace();
		}

		ModelAndView model = new ModelAndView("jsonV");
		if (null != processList && 0 < processList.size()) {
			model.addObject("processList", processList);
		}
		model.addObject("resultCode", 0);
		return model;
	}

	/**
	 * <pre>
	 * 큐레이션 - 큐레이션 순서 변경 화면 보여주기
	 * </pre>
	 * @Class CurationController.java
	 * @Method orderCuration
	 * @param model
	 * @param request
	 * @param response
	 * @return "orderCuration"-큐레이션 순서 변경 화면
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation/modal/orderCuration.do")
	public String orderCuration(ModelMap model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		List<CurationVO> curationList = curationService.selectList(map);
		model.addAttribute("listSize", curationList.size());
		model.addAttribute("curationList", curationList);
		return "admin/modal/orderCuration";
	}

	/**
	 * <pre>
	 * 큐레이션 - 순서 변경 적용
	 * </pre>
	 * @Class CurationController.java
	 * @Method orderCurationSave
	 * @param model
	 * @param curationVO
	 * @param request
	 * @param response
	 * @return "redirect:/admin/curation.do"
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/curation/modal/orderCurationSave.do")
	public String orderCurationSave(ModelMap model, @ModelAttribute("curation") CurationVO curationVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		List<Integer> seqList = new ArrayList<>();
		String[] seqArray = request.getParameterValues("CurationOrder");
		if(null != seqArray) {
			if (0 < seqArray.length) {
				for (int i = 0; i < seqArray.length; i++) {
					seqList.add(Integer.parseInt(seqArray[i]));
				}
				map.put("seqList", seqList);
			}
		}
		curationService.updateCurationOrder(map);
		return "redirect:/admin/curation.do";
	}
}
