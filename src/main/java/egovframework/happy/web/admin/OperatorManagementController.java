package egovframework.happy.web.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.happy.cmmn.Paging;
import egovframework.happy.service.admin.basic.OperatorService;
import egovframework.happy.service.client.vo.MemberVO;
import egovframework.happy.service.client.vo.OperatorVO;


/**
 * <pre>
 * [관리자페이지]
 * 운영자관리에 관한 Controller
 * </pre>
 * @Class OperatorManagementController.java
 * @Description 
 * @author HappyCollege 신규 개발
 * @since 2017. 6. 16.
 * @version 1.0
 */
@Controller
public class OperatorManagementController extends BaseController {
	private static final int ITEMS_PER_PAGE = 10; // rows per page

	@Resource(name = "operatorService")
	private OperatorService operatorService;
	
	/**
	 * <pre>
	 * 운영자관리 - 운영자 관리 페이지 출력
	 * </pre>
	 * @Class OperatorManagementController.java
	 * @Method operatorManagement
	 * @param model
	 * @param currentPage
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/admin/operatorManagement.do")
	public ModelAndView operatorManagement(ModelMap model,
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			HttpServletRequest request) throws Exception {
		
		int totalItems = 0;
		int numberOfPages = 0;
		int from = 0;
		int itemSize = 0;
		
		try{
			itemSize = Integer.parseInt(propertiesService.getString("pageUnit"));
		}catch (Exception e) {
			itemSize = ITEMS_PER_PAGE;
		}
		
		int operatorCount = operatorService.getCountOfAllOperator();
		numberOfPages = Paging.getNumberOfPages(operatorCount, itemSize);
		currentPage = Paging.fixCurrentPage(currentPage, numberOfPages);
		from = Paging.getStartingIndex(currentPage, itemSize);
		Map <String, Object> map = new HashMap<String, Object>();
		map.put("from", from);
		map.put("itemsPerPage", itemSize);
		
		List<OperatorVO> operatorList = operatorService.selectOperatorList(map);

		model.addAttribute("operatorList", operatorList);
		model.addAttribute("operatorCount", operatorCount);
		
		List<Integer> pageNumbers = Paging.addPageNumbers(currentPage, totalItems, numberOfPages);

		model.addAttribute("startingSNo", from);
		model.addAttribute("numOfPages", numberOfPages);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("pageNumbers", pageNumbers);
		
		return new ModelAndView("admin/admin/operatorManagement");
	}
	
	/**
	 * <pre>
	 * 운영자관리 - 특정 운영자의 Q&A 게시판 공개상태를 변경
	 * </pre>
	 * @Class OperatorManagementController.java
	 * @Method updateOperatorOpenStatus
	 * @param currentPage
	 * @param operatorVO
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "/admin/updateOperatorOpenStatus.do")
	public String updateOperatorOpenStatus(@ModelAttribute("operatorVO") 
			@RequestParam(value="pn", required=false, defaultValue="1") int currentPage,
			OperatorVO operatorVO, HttpServletRequest request) {
		try {
			int result = operatorService.updateOperatorOpenStatus(operatorVO);
			
			if (1 == result) {
				System.out.println("변경 성공");
			} else { 
				System.out.println("변경 실패  - Admin_Seq : " + operatorVO.getAdmin_Seq() + " / Open_Yn : " + operatorVO.getOpen_Yn());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/admin/operatorManagement.do?pn="+currentPage;
	}
	
	/**
	 * <pre>
	 * 운영자관리 - 사용자를 검색한다. (AJAX 데이터 처리)
	 * </pre>
	 * @Class OperatorManagementController.java
	 * @Method searchUserInfo
	 * @param request
	 * @param searchText
	 * @param searchType
	 * @return 
	 */
	@RequestMapping(value = "/admin/searchUserInfo.do")
	public Object searchUserInfo(HttpServletRequest request,
			@RequestParam(value="searchText", required=false, defaultValue="") String searchText,
			@RequestParam(value="searchType", required=true, defaultValue="N") String searchType) throws Exception {
		// searchType -> N = NickName / I = User Id
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchText", searchText);
		map.put("type", searchType);
		List<MemberVO> userList = null;
		try {
			userList = operatorService.searchEmployeeByType(map);
		} catch (Exception e) {
			System.out.println("[Error] " + e.getMessage());
			e.printStackTrace();
		}
		
		ModelAndView model = new ModelAndView("jsonV");
		if (null != userList && 0 < userList.size()) {
			model.addObject("userList", userList);
		}
		model.addObject("resultCode", 0);
		
		return model;
	}
	
	/**
	 * <pre>
	 * 운영자관리 - 운영자를 추가한다.
	 * </pre>
	 * @Class OperatorManagementController.java
	 * @Method insertOperatorInAdminPage
	 * @param request
	 * @param userInfo
	 * @param registIP
	 * @return 
	 */
	@RequestMapping(value = "/admin/insertOperatorInAdminPage.do")
	public String insertOperatorInAdminPage(HttpServletRequest request,
			@RequestParam(value="searchText", required=false, defaultValue="") String userInfo,
			@RequestParam(value="registIP", required=true, defaultValue="0.0.0.0") String registIP) throws Exception {
		
		if (!"".equals(userInfo)) {
			String [] data = userInfo.split("/");
			String suserId = data[0].trim();
			String nickname = data[1].trim();
			System.out.println("userId: " + suserId + ", nickname: " + nickname);
			
			if(operatorService.selectByUserId(suserId) < 1 ) {
				return "redirect:/admin/operatorManagement.do";
			}
			
			try {
				OperatorVO operator = new OperatorVO();
				operator.setUser_Id(suserId);
				operator.setNickname(nickname);
				operator.setProcess_Code(null);
				operator.setRegist_Ip(registIP);
				operatorService.insertOperator(operator);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return "redirect:/admin/operatorManagement.do";
	}
}
