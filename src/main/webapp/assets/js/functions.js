$( document ).ready(function() {

$('.c-sitemap a').each(function(){
  var self = $(this),
      href = self.attr('href');

  
  self.append('<span>'+href+'</span>');
});


// 강의목차 구성 변경
$('.js-reorder--close').on('click',function(e){
  e.preventDefault();
  $(this).hide();
});

//.js-textarea
var placeholder = '교수의 말\n\n왜 이 강의를 제작하게 되었나요?\n어떤 내용으로 구성되어 있죠?\n초등학생도 이해할 수 있게 간단명료하게\n설명해주세요. \n\n이런 분들에게 추천해요\n\n이 강의는 누가 학습하면 좋을까요?\n구체적으로 직업이나 연령대를 명시해도 좋아요.';
if($('.js-textarea').val() == '') $('.js-textarea').text(placeholder);

$('.js-textarea').focus(function(){
    if($(this).text() === placeholder){
        $(this).text('');
    }
});

$('.js-textarea').blur(function(){
    if($(this).text() ===''){
        $(this).text(placeholder);
    }    
});


// 모달 윈도우 관련
$(document).on('click', '.u-mask, .js-modal--close', function(e){
	e.preventDefault();
	var self = $(this),
		target = self.data('modal'),
		upload_type = self.data('upload-type'),
		setting_type = self.data('setting-type');
	
	if($('.js-modal--close').data("remove")) $('.u-modal').html("");
	
	if(upload_type == "youtube") {
		if(! lectureMng.complete_youtube()) return;
	}
	if(target == "tutorial-video" || target == "upload-video") {
		jwplayer().stop();
	}
	
	$('.u-mask').hide();
	$('.u-modal').hide();
});

$(document).on('click', '.js-trigger-modal', function(e){
	  e.preventDefault();
	  var self = $(this),
	      target = self.data('modal'),
	      upload_type = self.data('upload-type'),
	      setting_type = self.data('setting-type');

	  $('.u-modal').hide();
	  $('.u-mask').show();
	  $('#' + target).show();
	  
	  if(upload_type == "lecture") {
		  var $chapter_idx = $('div#div_chapter').index(self.parents('div#div_chapter'));
		  var $lecture_idx = $('div#div_chapter').eq($chapter_idx).find('figure').index(self.parent('figure'));

		  lectureMng.setInfo($chapter_idx, $lecture_idx);
	  }
	  if(target == "youtube" && setting_type == "youtube") {
		  lectureMng.setting_youtube();
	  }
	});
/*
$('.js-trigger-modal').on('click',function(e){
  e.preventDefault();
  var self = $(this),
      target = self.data('modal');

  $('.u-modal').hide();
  $('.u-mask').show();
  $('#' + target).show();
});
*/

$('.js-notice').on('click',function(e){
  e.preventDefault();

  $('.u-notice').addClass('is-show');
});



// 강의 목차 계획
/*
$(document).on('click', '.js-delete', function(e){
  e.preventDefault();
  var self = $(this),
      p = self.parent();

  p.removeClass('u-added');
});
*/
/*
$('.js-delete--list').on('click',function(e){
  e.preventDefault();
  var self = $(this);
  self.parents('figure').prev('.lecture-name').remove();
  self.parents('figure').remove();
});
*/

//.js-price-free
$('.js-price-free').on('change',function(){
  if( $(this).prop("checked") ){
    $('.js-price-text').prop('disabled', true).attr('value','0');
  }
});

$('.js-price').on('change',function(){
  if( $(this).prop("checked") ){
    $('.js-price-text').prop('disabled', false);
  }
});

function commaSeparateNumber(val){
  while (/(\d+)(\d{3})/.test(val.toString())){
    val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
  }
  return val;
}

$('.js-price-text').on('change',function(){
  var price = $(this).val();
  $(this).val(commaSeparateNumber(price));
});

}); // dot ready end

/**
 * 바이트수 반환  
 * 
 * @param el : tag jquery object
 * @returns {Number}
 */
function byteCheck(el){
    var codeByte = 0;
    for (var idx = 0; idx < el.val().length; idx++) {
        var oneChar = escape(el.val().charAt(idx));
        if ( oneChar.length == 1 ) {
            codeByte ++;
        } else if (oneChar.indexOf("%u") != -1) {
            codeByte += 2;
        } else if (oneChar.indexOf("%") != -1) {
            codeByte ++;
        }
    }
    return codeByte;
}
