<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- header -->
<header>
<div id="header">
	<div class="brand">
		<a href="../admin/memberStatus.do"><img src="../assets/img/brand.png" alt="HUNET"></a>
<%-- 		<a href="#"><img src="<c:url value='/assets/img/brand.png'/>" alt="HUNET"></a> --%>
	</div>
	<div id="nav">
		<ul>
			<li><a href="#">전체메뉴</a>
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
<!-- 					<li><a href="#">메뉴명</a></li> -->
				</ul>
			</li>
			<li><a href="#">admin</a>
				<ul>
					<li><a href="#">LMS</a></li>
					<li><a href="#">학점은행 LMS</a></li>
					<li><a href="#">HSM(법인영업)</a></li>
					<li><a href="#">HPM(강사관리)</a></li>
					<li><a href="#">HCPM(제휴사관리)</a></li>
					<li><a href="#">HJM(기자관리)</a></li>
					<li><a href="#">한글샘</a></li>
					<li><a href="#">사내 그룹웨어</a></li>
					<li><a href="#">주니어 관리자</a></li>
				</ul>
			</li>
			<li><a href="#">${sessionScope.ss_info.user_Nm} 님</a>
				<ul>
					<li><a href="../admin/Logout.do">로그아웃</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
</header> 
<!-- /header -->