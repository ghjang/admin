<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>

<form action="../admin/writeApplyinAdminPage.do" method="post">
	<input type="hidden" name="Cust_Center_Seq"
		value="${custService.cust_Center_Seq}">
	<table class="table table-bordered">
		<tr>
			<!--<th>구분</th>
			<td class="text-left"> 
				<c:forEach var="code" items="${ category }">
					<c:if test="${ custService.cust_Center_Category eq code.code}">
						${code.code_Nm }
					</c:if>
				</c:forEach>
			-->
			<th>제목</th>
			<td class="text-left">${custService.title}</td>
		</tr>
		<tr>
			<th>내용</th>
			<td class="text-left">${fn:replace(custService.content, cn, br)}</td>
		</tr>
		<tr>
			<th>답변</th>
			<c:choose>
				<c:when test="${custService.apply_Yn eq 'N'}">
					<td><textarea name="Apply_Content" rows="10"
							style="width: 100%;"></textarea></td>
				</c:when>
				<c:otherwise>
					<td><textarea name="Apply_Content" rows="10"
							style="width: 100%;">${custService.apply_Content}</textarea></td>
				</c:otherwise>
			</c:choose>
		</tr>
	</table>
	<div class="footer">
		<c:choose>
			<c:when test="${custService.apply_Yn eq 'N'}">
				<button type="submit" class="btn btn-default">등록하기</button>
			</c:when>
			<c:otherwise>
				<button type="submit" class="btn btn-default">수정하기</button>
			</c:otherwise>
		</c:choose>
	</div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('textarea').on('keyup', function() {
    	chkword(this, 5000);
    });

});
function chkword(obj, maxByte) {
    var strValue = obj.value;
    var strLen = strValue.length;
    var totalByte = 0;
    var len = 0;
    var oneChar = "";
    var str2 = "";

    for (var i = 0; i < strLen; i++) {
        oneChar = strValue.charAt(i);
        if (escape(oneChar).length > 4) {
            totalByte += 2;
        } else {
            totalByte++;
        }

        // 입력한 문자 길이보다 넘치면 잘라내기 위해 저장
        if (totalByte <= maxByte) {
            len = i + 1;
        }
    }

    // 넘어가는 글자는 자른다.
    if (totalByte > maxByte) {
        alert(maxByte + "Byte를 초과 입력 할 수 없습니다.");
        str2 = strValue.substr(0, len);
        obj.value = str2;
        chkword(obj, 5000);
    }
    
    return totalByte;
}	//chkword function
</script>
