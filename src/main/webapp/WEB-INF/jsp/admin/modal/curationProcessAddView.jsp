<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>

<div id="CurationProcessAddViewModal">
	<div id="CurationProcessAddViewModalSearch">
		<select name="searchType" id="searchType">
			<option value="Process_Nm">과정명</option>
			<option value="Process_Code">과정코드</option>
		</select> <input type="text" id="searchText" autocomplete="off"
			multiple="multiple" list="searchList" style="width: 200px;">
		<input class="btn btn-default" type="button" value="추가"
			id="addProcess">
		<div id="selectList"></div>
	</div>
	<form action="../admin/registerCurationProcessList.do" method="post">
		<input type="hidden" name="chkCuration_Nm"
			value="${chkCuration.curation_Nm }"> <input type="hidden"
			name="chkCuration_Intro" value="${chkCuration.line_Introduction }">
		<input type="hidden" id="Curation_Seq" name="chkCuration_Seq"
			value="${chkCuration.curation_Seq }"> <input type="hidden"
			name="chkstart_period" value="${chkCuration.expression_Start_Day }">
		<input type="hidden" name="chkend_period"
			value="${chkCuration.expression_CLOSE_Day }"> <input
			type="hidden" name="chkUse_Yn" value="${chkCuration.use_Yn }">

		<table class="table table-bordered">
			<thead>
				<tr>
					<th>순서</th>
					<th>과정명</th>
					<th>공개여부</th>
					<th>순서변경</th>
					<th>기타</th>
				</tr>
			</thead>
			<tbody id="curationProcessModal_tbody">
				<c:if test="${cpList ne null and fn:length(cpList) > 0}">
					<input type="hidden" id="listSize" value="${listSize}">
					<input type="hidden" id="cpOOrder" value="${fn:length(cpList)}" />
					<c:forEach var="cp" items="${cpList }">
						<tr>
							<td>${cp.curation_Process_Order }</td>
							<td><div align="left"><b>${cp.process_Code }</b> ${cp.process_Nm }</div></td>
							<td><c:if test="${cp.public_Yn eq 'Y'}">
								공개
							</c:if> <c:if test="${cp.public_Yn eq 'N'}">
								비공개
							</c:if> <c:if test="${cp.block_Yn eq 'Y'}">
							&nbsp;&frasl;&nbsp;차단
							</c:if></td>
							<td><button type="button" class="btn btn-default"
									onclick="moveUp(this)" style="display: inline-block;">▲</button>
								<button type="button" class="btn btn-default"
									onclick="moveDown(this)" style="display: inline-block;">▼</button></td>
							<td><button type="button" class="btn btn-default"
									onclick="deleteProcess(this)" value="${cp.process_Code }">삭제</button></td>
						<input type="hidden" name="Process_Code" value="${cp.process_Code }" data-code="${cp.process_Code }" />
						<input type="hidden" name="Public_Yn" value="${cp.public_Yn }" data-code="${cp.process_Code }" />
						<input type="hidden" name="Block_Yn" value="${cp.block_Yn }" data-code="${cp.process_Code }" />
						<input type="hidden" name="Process_Nm" value="${cp.process_Nm }" data-code="${cp.process_Code }" />
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<div align="center">
			<button type="button" class="btn btn-default"
				id="curationProcessSave">완료</button>
		</div>
	</form>
</div>

<script type="text/javascript">
$(document).ready(function(){

	//과정 검색
	$("#searchText").on("paste textchange", function() {
// 	$("#searchText").on('click', function() {
		var el = $(this);
        setTimeout(function(){
            var text = $(el).val();
        },100);	
	
		var availableTags = [];
		var keyword = $("#searchText").val();
		var type = $("#searchType").val();
		console.log("keyword: " + keyword);
		if ('' != keyword) {
			$.ajax({
		        url : "../admin/curation/modal/searchCurationProcessInfo.do",
		        type : "post",
		        data : {searchText:keyword, searchType:type},
		        dataType : "json",
		        error :  function(e){
					alert(e.responseText);
				},
		        success : function(data){
		        	if (0 == data['resultCode']) {
		        		var pList = data["processList"];
// 						var data_html = '<datalist id="searchList">';
						if(pList != null) {
			        		for (var i=0; i < pList.length; i++) {
			        			var emp="";
			        			emp = pList[i].process_Code+' '+pList[i].process_Nm;
			        			availableTags.push(emp);
// 			        			data_html += ('<option value="'+pList[i].process_Code+' '+pList[i].process_Nm+'">');
							}
						}
// 		        		data_html += '</datalist>';
// 		        		$("#selectList").html(data_html);
// 						$("#searchText").trigger("autocompleteselect");
// 						$("#searchText").trigger("select");
// 						$("#searchText").trigger('select', 'autocompleteselect');
						$("#searchText").autocomplete({
		        			select: function(event, ui) {
		        				console.log("select");
								$("#searchText").autocomplete( "destroy" );
		        			},
							create: function(event, ui) {
								console.log("create");								
							},
							source: availableTags
		            	});
						
						$("#searchText").autocomplete("option", "appendTo", "#CurationProcessAddViewModal");
// 						$("#searchText").autocomplete("option", "appendTo", "#CurationProcessAddViewModal");
// 						$("#searchText").on("autocompleteopen", function(event, ui){});
		        	}
		        }
		    });

		}
// 		$("#searchText").trigger("autocompleteselect");
// 		$(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item:{value:$(this).val()}});
// 		$("#searchText").on("autocompleteselect", function(event, ui){});
		
	});
// 	$("ul li.ui-menu-item").trigger("select");
	
	
	var order = $('#listSize').val();
	order = $("#cpOOrder").val();
	if(typeof(order) =='undefined' || order == "") {
		order = 0;
	}
// 	console.log("order: " + order);
	//과정 추가
	$('#addProcess').on('click', function(){
		var keyword = $("#searchText").val();
		var pCode = keyword.split(" ",1);
		var pName = keyword.replace(pCode,'');
		
		if(!correctProcess(pCode, pName)) {
			alert('다시 검색해주세요.');
			return;
		}
		
		if ('' != keyword) {
			$.ajax({
		        url : "../admin/curation/modal/curationProcessAdd.do",
		        type : "post",
		        data : {searchText:keyword},
		        dataType : "json",
		        error :  function(e){
					alert(e.responseText);
				},
		        success : function(data){
		        	if(0 == data['resultCode']) {
			        	order++;
			        	var openStatus;
			        	var blockStatus='';

		        		var process = data['process'];
		        		
			        	if(process.public_Yn == 'Y') {
			        		openStatus = '공개';
			        	} else if(process.public_Yn == 'N') {
			        		openStatus = '비공개';
			        	}
			        	
			        	if(process.block_Yn == 'Y') {
			        		blockStatus = ' / 차단';
			        	}
		        		
		        		var dataHtml = '<tr><td>'+order+'</td><td><div align="left"><b>';
		        		dataHtml += process.process_Code + '</b> ' + process.process_Nm;
		        		dataHtml += '</div></td><td>';
		        		dataHtml += openStatus + blockStatus;
		        		dataHtml += '</td><td><button type="button" class="btn btn-default" onclick="moveUp(this)" style="display:inline-block;">▲</button><button type="button" class="btn btn-default" onclick="moveDown(this)" style="display:inline-block;">▼</button></td>';
			        	dataHtml += '<td><button type="button" class="btn btn-default" onclick="deleteProcess(this)" value="'+process.process_Code+'">삭제</button></td>'
			        	dataHtml += '<input type="hidden" name="Process_Code" value="'+process.process_Code+'"data-code="'+process.process_Code+'">';
			        	dataHtml += '<input type="hidden" name="Public_Yn" value="'+process.public_Yn+'"data-code="'+process.process_Code+'">'
			        	dataHtml += '<input type="hidden" name="Block_Yn" value="'+process.block_Yn+'"data-code="'+process.process_Code+'">'
			        	dataHtml += '<input type="hidden" name="Process_Nm" value="'+process.process_Nm+'"data-code="'+process.process_Code+'"></tr>';
			        	
		        		$('#curationProcessModal_tbody').append(dataHtml);
		        	}
		        	
		        	$("#searchText").val('');
		        }//success
		    });
		}//end if
	});// end 과정추가
	
	//save
	$('#curationProcessSave').on('click', function() {
		var openyn = "";
		$('input[name=Public_Yn]').each(function() {
			openyn += $(this).val();
			
		});
		var blockyn = "";
		$('input[name=Block_Yn]').each(function() {
			blockyn += $(this).val();
		});
		
		alert("openyn: " + openyn);
		
		if(0 <= openyn.indexOf("N") || 0 <= blockyn.indexOf('Y')) {
			alert('비공개 또는 차단 과정이 포함되어 있습니다. 재확인해주세요.');
		}
		
		$('form').submit();
	});	//save

});

function moveUp(el){
	var $tr = $(el).parent().parent(); // 클릭한 버튼이 속한 tr 요소
	$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
};

function moveDown(el){
	var $tr = $(el).parent().parent(); // 클릭한 버튼이 속한 tr 요소
	$tr.next().after($tr); // 현재 tr 의 다음 tr 뒤에 선택한 tr 넣기
};

//과정 추가할 때, 과정명과 과정코드가 실제로 맞는지 체크
function correctProcess(pCode, pName) {
	if(pCode == null || pName == null) {
		return false;
	} else {
		if(pCode[0].length < 10 || pName.length < 2) {
			return false;
		} else {
			return true;
		}
	}
};

function deleteProcess(obj) {
// 	console.log('delete Process_Code: ' + obj.value);
	var tr = $(obj).parent().parent();
    tr.remove();
    alert($(obj).val());
};

</script>
