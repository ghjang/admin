<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<div id="contentreviewModal">
	<input type="hidden" id="Process_Code" name="Process_Code"
		value="${processDTO.process_Code }" /> <input type="hidden"
		id="TakeLecture_Review_Count" name="TakeLecture_Review_Count"
		value="${processDTO.takeLecture_Review_Count }" />
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="15%">작성일</th>
				<th width="70%">수강 후기</th>
				<th width="15%">작성자</th>
			</tr>
		</thead>
		<tbody id="tbody">
			<c:choose>
				<c:when test="${reviewList ne null and fn:length(reviewList) > 0}">
					<c:forEach var="review" items="${reviewList}">
						<tr>
							<td>${ cfn:dateFmt2(review.regist_Date) }</td>
							<td>
								<div align="left">
									${review.content } <br />
									<c:forEach var="entry" items="${resultMap }" varStatus="status">
										<c:if test="${entry.key eq review.takeLecture_Review_Seq }">
											<c:forEach var="tail" items="${ entry.value}"
												varStatus="status">
										ㄴ${tail.tail_Content } (${tail.regist_Date }) <br />
											</c:forEach>
										</c:if>
									</c:forEach>
								</div>
							</td>
							<td>${review.nickname }</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="3">등록된 수강후기가 없습니다.</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>

	<c:if test="${reviewList ne null and fn:length(reviewList) > 0}">
		<div class="footer">
			<div class="pagination-centered">
				<input type="hidden" id="currentPage" value="${currentPage}" /> <input
					type="hidden" id="pageNumbers" value="${pageNumbers}" /> <input
					type="hidden" id="numOfPages" value="${numOfPages}" />
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a
						id="leftTen">&laquo;&laquo;</a></li>
					<li <c:if test="${currentPage-1 <= 0}">class="disabled"</c:if>><a
						id="left">&laquo;</a></li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active"><a class="current">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li><a class="current">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li
						<c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a
						id="right">&raquo;</a></li>
					<li
						<c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a
						id="rightTen">&raquo;&raquo;</a></li>
				</ul>
			</div>
		</div>
	</c:if>
</div>

<script type="text/javascript">
$(document).ready(function(){
// 	console.log('reviewDetail] pageNumbers:' + $('#pageNumbers').val() );
	var Process_Code = $('input[name=Process_Code]').val();
	var TakeLecture_Review_Count = $('input[name=TakeLecture_Review_Count]').val();
	var pageNumbers =  $('#pageNumbers').val();
	var currentPage = $('#currentPage').val();
	var numOfPages = $('#numOfPages').val();
	
	$('#left').on('click', function(event) {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		
		$.ajax({
			url: '../admin/process/modal/reviewDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) -1),
				Process_Code : Process_Code,
				TakeLecture_Review_Count : TakeLecture_Review_Count
			},
			success: function(data, status, xhr) {
				$('#contentreviewModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});

	
	$('.current').on('click', function() {
		var pn = $(this).html();
		$.ajax({
			url: '../admin/process/modal/reviewDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : pn,
				Process_Code : Process_Code,
				TakeLecture_Review_Count : TakeLecture_Review_Count
			},
			success: function(data, status, xhr) {
				$('#contentreviewModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});

	$('#right').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		
		$.ajax({
			url: '../admin/process/modal/reviewDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 1),
				Process_Code : Process_Code,
				TakeLecture_Review_Count : TakeLecture_Review_Count
			},
			success: function(data, status, xhr) {
				$('#contentreviewModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#rightTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/reviewDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 10),
				Process_Code : Process_Code,
				TakeLecture_Review_Count : TakeLecture_Review_Count
			},
			success: function(data, status, xhr) {
				$('#contentreviewModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#leftTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/reviewDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) - 10),
				Process_Code : Process_Code,
				TakeLecture_Review_Count : TakeLecture_Review_Count
			},
			success: function(data, status, xhr) {
				$('#contentreviewModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
});

</script>