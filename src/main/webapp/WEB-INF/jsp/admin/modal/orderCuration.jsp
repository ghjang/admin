<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<div id="orderCurationModal">
	<form action="../admin/curation/modal/orderCurationSave.do">
		<input type="hidden" id="listSize" name="listSize" value="${listSize}">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>순서</th>
					<th>큐레이션명</th>
					<th>사용여부</th>
					<th>순서변경</th>
				</tr>
			</thead>
			<tbody id="tbody">
				<c:choose>
					<c:when
						test="${curationList ne null and fn:length(curationList) > 0}">
						<c:forEach var="curation" items="${curationList }"
							varStatus="status">
							<tr id="${curation.curation_Seq }">
								<td>${curation.order }</td>
								<td><div align="left">${curation.curation_Nm }</div></td>
								<td><c:choose>
										<c:when test="${curation.use_Yn eq 'Y'}">
									사용
								</c:when>
										<c:otherwise>
									미사용
								</c:otherwise>
									</c:choose></td>
								<td>
									<button class="btn btn-default up" type="button"
										value="${ curation.order}">▲</button>
									<button class="btn btn-default down" type="button"
										value="${ curation.order}">▼</button> <input type="hidden"
									name="CurationOrder" value="${curation.curation_Seq }">
								</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="4">등록된 큐레이션이 없습니다.</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<div class="footer">
			<button class="btn btn-default" type="submit" id="orderSave">저장</button>
		</div>
	</form>
</div>

<script type="text/javascript">
$(function(){
	var listSize = $('#listSize').val();
	console.log("listSize: "+listSize);
	
	$('.up').on('click', function(event) {
		moveUp($(this));
	});
	
	$('.down').on('click', function() {
		moveDown($(this));
	});
	
	if(listSize > 0 ) {
		$('#orderSave').on('click', function() {
			listSize++;
			for(var i=1; i<listSize; i++){
				console.log($('#orderCurationModal tbody tr:nth-child('+i+')').attr('id'));
			}
			/*
			$('#orderSave').submit(function() {
				for(var i=1; i<listSize; i++){
					console.log($('#orderCurationModal tbody tr:nth-child('+i+')').attr('id'));
		// 			i : $('#orderCurationModal tbody tr:nth-child('+i+')').attr('id')
				}
			});
			*/
		});
	}

});

function moveUp(el){
	var $tr = $(el).parent().parent(); // 클릭한 버튼이 속한 tr 요소
	$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
}

function moveDown(el){
	var $tr = $(el).parent().parent(); // 클릭한 버튼이 속한 tr 요소
	$tr.next().after($tr); // 현재 tr 의 다음 tr 뒤에 선택한 tr 넣기
}
</script>