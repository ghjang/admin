<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>

<form action="../admin/sendEmail.do" method="POST" id="sendForm">
	<input type="hidden" name="Process_Code"
		value="${process.process_Code}"> <input type="hidden"
		name="Block_Yn" value="Y">
	<div>
		<p>이메일 주소</p>
		<input type="text" name="Email" style="width: 90%;"
			value="${process.email }" id="email" required />
	</div>
	<div>
		<p>메일 내용</p>
		<textarea rows="10" cols="10" style="width: 90%;" name="Content"
			id="mailContent" required></textarea>
	</div>
	<div class="footer">
		<input class="btn btn-default" id="send" style="width: 30%;"
			value="이메일 발송 및 차단 설정">
	</div>
</form>

<script type="text/javascript">
$(document).ready(function(){
	$('#mailContent').keyup(function (e){
	    chkword(this, 2000);
	});
	
	
	$('#send').on('click',function(event){
		if (false == validationCheck()) {
			event.preventDefault();
			return;
		}
		
		var result = confirm('해당 회원에게 이메일을 전송하시겠습니까?');
		if(result == true) {
			$('#sendForm').submit();
		}
	});
});

function chkword(obj, maxByte) {
    var strValue = obj.value;
    var strLen;
    if(strValue == null) {
    	strLen = 0;
    } else{
    	strLen = strValue.length;
    }
    var totalByte = 0;
    var len = 0;
    var oneChar = "";
    var str2 = "";

    for (var i = 0; i < strLen; i++) {
        oneChar = strValue.charAt(i);
        if (escape(oneChar).length > 4) {
            totalByte += 2;
        } else {
            totalByte++;
        }

        // 입력한 문자 길이보다 넘치면 잘라내기 위해 저장
        if (totalByte <= maxByte) {
            len = i + 1;
        }
    }

    // 넘어가는 글자는 자른다.
    if (totalByte > maxByte) {
        alert(maxByte + "Byte를 초과 입력 할 수 없습니다.");
        str2 = strValue.substr(0, len);
        obj.value = str2;
        chkword(obj, 2000);
    }
    
    return totalByte;
}	//chkword function


function validationCheck() {
	var email = $('#email').val();
	var content = $('#mailContent').val();
	console.log(content);
	if(email == null || email == "") {
		alert("이메일을 입력해주세요.");
		return false;
	} else if(content == null || content == "") {
		alert("메일 내용을 입력해주세요.");
		return false;
	} else {
		return true;
	}
};



</script>