<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>

<div>
	<p>
		<b>이메일 주소</b>
	</p>
	<p>${block.email }</p>
</div>
<div>
	<p>
		<b>메일 내용</b>
	</p>
	<pre>${block.content }</pre>
</div>