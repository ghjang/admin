<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />

<div id="takeLectureContent">
	<input type="hidden" id="Process_Code" name="Process_Code"
		value="${processDTO.process_Code }" />
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>신청일</th>
				<th>학습기간 종료일</th>
				<th>ID</th>
				<th>닉네임</th>
				<th>상태</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${lectureList ne null and fn:length(lectureList) > 0}">
					<c:forEach var="lecturePeople" items="${lectureList}">
						<tr>
							<td>${ cfn:dateFmt2(lecturePeople.takeLecture_Start_Date) }</td>
							<td id="endDate">${ cfn:dateFmt2(lecturePeople.takeLecture_End_Date) }</td>
							<td>${ lecturePeople.user_Id }</td>
							<td>${ lecturePeople.nickname }</td>
							<td><c:choose>
									<c:when test="${lecturePeople.takeLecture_End_Date < now }">
								기간 종료
							</c:when>
									<c:otherwise>
								진행중
							</c:otherwise>
								</c:choose></td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="5">수강자가 없습니다.</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>

	<c:if test="${lectureList ne null and fn:length(lectureList) > 0}">
		<input type="hidden" id="currentPage" value="${currentPage}" />
		<input type="hidden" id="pageNumbers" value="${pageNumbers}" />
		<input type="hidden" id="numOfPages" value="${numOfPages}" />
		<div class="pagination-centered footer">
			<ul class="pagination">
				<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a
					id="leftTen">&laquo;&laquo;</a></li>
				<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>><a
					id="left">&laquo;</a></li>
				<c:forEach var="pn" items="${pageNumbers}">
					<c:choose>
						<c:when test="${currentPage==pn}">
							<li class="active"><a class="current">${pn}</a></li>
						</c:when>
						<c:otherwise>
							<li><a class="current">${pn}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<li
					<c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a
					id="right">&raquo;</a></li>
				<li
					<c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a
					id="rightTen">&raquo;&raquo;</a></li>
			</ul>
		</div>
	</c:if>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var pageNumbers =  $('#pageNumbers').val();
	var currentPage = $('#currentPage').val();
	var numOfPages = $('#numOfPages').val();
	var currentPage = $('#currentPage').val();
	var Process_Code = $('#Process_Code').val();
	
	$('#leftTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/takeLecturePeople.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) -10),
				Process_Code : Process_Code
			},
			success: function(data, status, xhr) {
				$('#takeLectureContent').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	
	$('#left').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/takeLecturePeople.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) -1),
				Process_Code : Process_Code
			},
			success: function(data, status, xhr) {
				$('#takeLectureContent').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('.current').on('click', function() {
		var pn = $(this).html();
		$.ajax({
			url: '../admin/process/modal/takeLecturePeople.do',
			type: 'GET',
			async: false,
			data: {
				pn : pn,
				Process_Code : Process_Code
			},
			success: function(data, status, xhr) {
				$('#takeLectureContent').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});

	$('#right').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/takeLecturePeople.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 1),
				Process_Code : Process_Code
			},
			success: function(data, status, xhr) {
				$('#takeLectureContent').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#rightTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/process/modal/takeLecturePeople.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 10),
				Process_Code : Process_Code
			},
			success: function(data, status, xhr) {
				$('#takeLectureContent').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
});

</script>
