<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>

<c:forEach var="tagVO" items="${tagList}">
	<button type="button" class="btn btn-default" disabled>${ tagVO.tag }</button>
</c:forEach>
