<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<link rel="stylesheet" href="<c:url value='/assets/css/style.css'/>" />

<div id="makeLectureDetailModal">
	<input type="hidden" name="User_Id" value="${member.user_Id }">
	<input type="hidden" name=totalItems value="${totalItems }">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="15%">개설일</th>
				<th>상태</th>
				<th>과정코드</th>
				<th>과정명</th>
				<th>공개여부</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${processList ne null and fn:length(processList) > 0}">
					<c:forEach var="process" items="${processList }">
						<tr>
							<td>${cfn:dateFmt2(process.regist_Date) }</td>
							<td><c:choose>
									<c:when test="${process.open_Yn eq 'Y'}">
								오픈
							</c:when>
									<c:otherwise>
								제작중
							</c:otherwise>
								</c:choose></td>
							<td>${process.process_Code }</td>
							<td><div align="left">${process.process_Nm }</div></td>
							<td><c:choose>
									<c:when test="${process.public_Yn eq 'Y'}">
								공개
							</c:when>
									<c:when test="${process.public_Yn eq 'N'}">
								비공개
								<c:if test="${process.block_Yn eq 'Y'}">
								(차단)
								</c:if>
									</c:when>
								</c:choose></td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="5">개설 과정이 없습니다.</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<c:if test="${processList ne null and fn:length(processList) > 0}">
		<div class="footer">
			<input type="hidden" id="currentPage" value="${currentPage}" /> <input
				type="hidden" id="pageNumbers" value="${pageNumbers}" /> <input
				type="hidden" id="numOfPages" value="${numOfPages}" />
			<div class="pagination-centered">
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a
						id="leftTen">&laquo;&laquo;</a></li>
					<li <c:if test="${currentPage-1 <= 0}">class="disabled"</c:if>><a
						id="left">&laquo;</a></li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active"><a class="current">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li><a class="current">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li
						<c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a
						id="right">&raquo;</a></li>
					<li
						<c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a
						id="rightTen">&raquo;&raquo;</a></li>
				</ul>
			</div>
		</div>
	</c:if>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var User_Id  = $('input[name=User_Id]').val();
	var totalItems = $('input[name=totalItems]').val();
	var currentPage = $('#currentPage').val();
	var pageNumbers = $('#pageNumbers').val();
	var numOfPages = $('#numOfPages').val();
	
	$('.current').on('click', function() {
		var pn = $(this).html();
// 		console.log('누른 숫자: '+$(this).html());
		$.ajax({
			url: '../admin/member/modal/makeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : pn,
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#makeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#left').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/makeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (currentPage-1),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#makeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#right').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/makeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 1),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#makeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#rightTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/makeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 10),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#makeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#leftTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/makeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) - 10),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#makeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
});
</script>