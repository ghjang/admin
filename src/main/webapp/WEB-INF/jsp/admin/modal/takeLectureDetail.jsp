<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<div id="takeLectureDetailModal">
	<input type="hidden" name="User_Id" value="${member.user_Id }">
	<input type="hidden" name=totalItems value="${totalItems }">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="15%">구입일</th>
				<th>상태</th>
				<th width="15%">학습가능 기간</th>
				<th>과정코드</th>
				<th>과정명</th>
				<th>강사명</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when
					test="${takeLectureList ne null and fn:length(takeLectureList) > 0}">
					<c:forEach var="lecture" items="${takeLectureList }">
						<fmt:formatDate var="dateFormat" value="${lecture.takeLecture_End_Date}" pattern="yyyy-MM-dd"/>
						<fmt:formatDate var="dateNOWFormat" value="${now}" pattern="yyyy-MM-dd"/>
						<tr>
							<td>${cfn:dateFmt2(lecture.takeLecture_Start_Date )}</td>
							<td><c:choose>
								<c:when test="${lecture.takeLecture_End_Date < dateNOWFormat }">
									기간 종료
								</c:when>
								<c:otherwise>
									진행중
								</c:otherwise>
								</c:choose></td>
							<td><div align="left">${dateFormat }까지</div></td>
							<td>${lecture.process_Code }</td>
							<td><div align="left">${lecture.process_Nm }</div></td>
							<td>${lecture.nickname }</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="6">수강 과정이 없습니다.</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<c:if
		test="${takeLectureList ne null and fn:length(takeLectureList) > 0}">
		<div class="footer">
			<div class="pagination-centered">
				<input type="hidden" id="currentPage" value="${currentPage}" /> <input
					type="hidden" id="pageNumbers" value="${pageNumbers}" /> <input
					type="hidden" id="numOfPages" value="${numOfPages}" />
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a
						id="leftTen">&laquo;&laquo;</a></li>
					<li <c:if test="${currentPage-1 <= 0}">class="disabled"</c:if>><a
						id="left">&laquo;</a></li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active"><a class="current">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li><a class="current">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li
						<c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a
						id="right">&raquo;</a></li>
					<li
						<c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a
						id="rightTen">&raquo;&raquo;</a></li>
				</ul>
			</div>
		</div>
	</c:if>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var User_Id  = $('input[name=User_Id]').val();
	var totalItems = $('input[name=totalItems]').val();
	var currentPage = $('#currentPage').val();
	var pageNumbers = $('#pageNumbers').val();
	var numOfPages = $('#numOfPages').val();
	
	$('.current').on('click', function() {
		var pn = $(this).html();
// 		console.log('누른 숫자: '+pn);
		$.ajax({
			url: '../admin/member/modal/takeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : pn,
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#takeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#left').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/takeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) - 1),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#takeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#right').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/takeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 1),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#takeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#rightTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/takeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) + 10),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#takeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
	
	$('#leftTen').on('click', function() {
		if($(this).parent().prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
		$.ajax({
			url: '../admin/member/modal/takeLectureDetail.do',
			type: 'GET',
			async: false,
			data: {
				pn : (parseInt(currentPage) - 10),
				User_Id : User_Id,
				totalItems : totalItems
			},
			success: function(data, status, xhr) {
				$('#takeLectureDetailModal').html(data);
			},
			error: function(xhr, status, message) {
				console.log("status: " + status + ", error:" + message);
			}
		});
	});
});
</script>