<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>공지사항</title>
	<style type="text/css">
		#container img {
			width: 60%;
			height: auto;
		}
		

		.innerPopup {
		   z-index: 2222;
		}

	</style>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li class="active"><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>공지사항</h1>
			</div>
			<div>
				<input class="btn btn-default" type="button" value="글등록" data-toggle="modal" data-target="#register_article" style="margin-bottom:6px;">
			</div>
			<div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>등록일</th>
							<th>제목</th>
							<th>조회수</th>
							<th>공개여부</th>
							<th>관리</th>
						</tr>
					</thead>
					<tbody>
					<c:set var="count" value="${fn:length(noticeList) +1}" scope="page" />
					<c:forEach var="notice" items="${ noticeList }" varStatus="status">
						<c:set var="count" value="${count - 1}" scope="page" />
						<tr>
							<td>${(totalItems+1) - (status.count + (currentPage-1)*itemSize)} </td>
							<td>${cfn:dateFmt2(notice.regist_Date)}</td>
							<td>
							<div align="left">
								<a href="" id="course_detail_view_${notice.notice_Seq}" data-toggle="modal" data-target="#course_detail_${notice.notice_Seq}">
								${notice.title}</a>
							</div>
							
							<!-- start 제목'상세보기' modal -->
							<div class="modal fade" id="course_detail_${notice.notice_Seq}" tabindex="" role="dialog" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="header">
									        <button type="button" class="modal-close" data-dismiss="modal">X</button>
											<div class="modal-title header ">
												<h4>글 상세보기</h4>
											</div>
									    </div>
									    <div class="modal-body">
											<table class="table table-bordered">
												<tr>
													<th width="10%">제목</th>
													<td><div align="left">${notice.title}</div></td>
												</tr>
												<tr>
													<th width="10%">이미지</th>
													<td>
														<c:if test="${notice.notice_Image_Url ne null and notice.notice_Image_Url ne '' }">
															<img src="../${notice.notice_Image_Url}"/>
														</c:if>
													</td>
												</tr>
												<tr>
													<th width="10%">내용</th>
													<td class="text-left">
														${fn:replace(notice.content, cn, br)}
													</td>
												</tr>
											</table>
										</div>
									</div><!-- div modal-content -->
								</div>
							</div>
							<!-- end  제목'상세보기'  modal -->
							
							</td>
							<td>${notice.read_Count}</td>
							<td>
								<form action="../admin/updateOpenInfo.do" method="POST">
									<input type="hidden" name="Notice_Seq" value="${notice.notice_Seq }"/>
									<input type="hidden" name="Edit_Id" value="${notice.edit_Id }"/>
									<input type="hidden" name="Edit_Ip" value="${notice.edit_Ip }"/>
									<select name="Open_Yn">
										<c:choose>
											<c:when test="${ notice.open_Yn eq 'Y'}">
												<option value="Y" selected>공개</option>
												<option value="N">비공개</option>
											</c:when>
											<c:otherwise>
												<option value="Y">공개</option>
												<option value="N" selected>비공개</option>
											</c:otherwise>
										</c:choose>
									</select>
									<input class="btn btn-default" type="submit" value="저장">
								</form>
							</td>
							<td>
								<input class="btn btn-default" type="button" value="수정" data-toggle="modal" data-target="#edit_article_${notice.notice_Seq}">
								<input class="btn btn-default" type="button" value="삭제" data-toggle="modal" data-target="#confirm_delete_${notice.notice_Seq}">
									<!-- start '글 수정' modal -->
									<div id="edit_article_${notice.notice_Seq}" class="modal fade">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="header">
											        <button type="button" class="modal-close" data-dismiss="modal">X</button>
													<div class="modal-title header">
														<h4>글 작성/편집</h4>
													</div>
											    </div>
												<form action="../admin/updateNotice.do" method="post" enctype="multipart/form-data">
													<input type="hidden" name="Notice_Seq" value="${notice.notice_Seq }"/>
													<input type="hidden" name="Image" value="${notice.image }"/>
													<input type="hidden" name="Read_Count" value="${notice.read_Count }"/>
													<input type="hidden" name="Open_Yn" value="${notice.open_Yn }"/>
													<input type="hidden" name="Delete_Yn" value="${notice.delete_Yn }"/>
													<input type="hidden" name="redirect" value="${notice.redirect }"/>
													<input type="hidden" name="Regist_Id" value="${notice.regist_Id }"/>
													<input type="hidden" name="Regist_Ip" value="${notice.regist_Ip }"/>
<%-- 													<input type="hidden" name="Regist_Date" value="${notice.regist_Date }"/> --%>
													<input type="hidden" name="Edit_Id" value="${notice.edit_Id }"/>
													<input type="hidden" name="Edit_Ip" value="${notice.edit_Ip }"/>
<%-- 													<input type="hidden" name="Edit_Date" value="${notice.edit_Date }"/> --%>
													<table class="table table-bordered">
														<tr>
															<th>제목</th>
															<td><input type="text" name="title" id="title_edit" style="width:90%;" value="${ notice.title }" required/></td>
														</tr>
														<tr>
															<th>이미지</th>
															<td>
																<c:if test="${notice.notice_Image_Url ne null and notice.notice_Image_Url ne '' }">
<%-- 																		<a href="../${notice.notice_Image_Url}" target="_blank">이미지 새창</a> --%>
																	<a data-target="#attachedImageModal_${notice.notice_Seq }" href="javascript:void(0);"
																		class="attatchedNoticeImage" data-toggle="modal">첨부 이미지 확인</a>
																	<!-- 첨부 이미지 확인 Modal start -->
																	<div id="attachedImageModal_${notice.notice_Seq }" class="modal attachedImageModal" style=" z-index: 2222;">
																		<div class="modal-dialog">
																			<div class="modal-content">
																				<div class="header">
																			        <button type="button" class="modal-close" onclick="closeOneModal(${notice.notice_Seq })">X</button>
																					<div class="modal-title header" align="left">
																						<h4>첨부 이미지 확인</h4>
																					</div>
																					<div>
																						<div align="center"><img src="..${notice.notice_Image_Url}" style="width:100%; height:100%;"/></div>
																					</div>
																			    </div>
																			 </div>
																		</div>
																	</div>
																	<!-- 첨부 이미지 확인 Modal end -->
																</c:if>
																<input type="file" name="noticeImage"/> <!-- file -->
															</td>
														</tr>
														<tr>
															<th>내용</th>
															<td>
																<textarea rows="10" style="width:100%;" name="content" id="content_edit" required>${notice.content}</textarea>
															</td>
														</tr>
													</table>
													<div class="footer">
														<input class="btn btn-default" type="submit" value="완료">
													</div>
												</form>				
											</div><!-- div modal-content -->
										</div>
									</div>
									<!-- end '글 수정' modal -->
		
									
									<!-- start 관리 '삭제' modal -->
									<div id="confirm_delete_${notice.notice_Seq}" class="modal">
										<div class="modal-dialog">
											<div class="modal-content">
												<p class="text-center">해당 공지사항 글을 삭제하시겠습니까?</p>
												<div class="footer">
													<form action="../admin/deleteNotice.do" method="POST">
														<input type="hidden" name="Notice_Seq" value="${notice.notice_Seq }"/>
														<input type="hidden" name="Edit_Id" value="${notice.edit_Id }"/>
														<input type="hidden" name="Edit_Ip" value="${notice.edit_Ip }"/>
														<button type="submit" class="btn btn-primary">삭제</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
													</form>
												</div>
											</div><!-- div modal-content -->
										</div>
									</div>
									<!-- end 관리 '삭제' modal -->
							</td>
						</tr>
					</c:forEach>
					</tbody>	
				</table>
				<div class="pagination-centered" align="center">
					<ul class="pagination">
						<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a href="../admin/notice.do?pn=${currentPage-10}">이전 10개</a></li>
						<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>><a href="../admin/notice.do?pn=${currentPage-1}">&laquo;</a></li>
						<c:forEach var="pn" items="${pageNumbers}">
							<c:choose>
								<c:when test="${currentPage==pn}">
									<li class="active"><a
										href="../admin/notice.do?pn=${pn}">${pn}</a></li>
								</c:when>
								<c:otherwise>
									<li><a
										href="../admin/notice.do?pn=${pn}">${pn}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a href="../admin/notice.do?pn=${currentPage+1}">&raquo;</a></li>
						<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a href="../admin/notice.do?pn=${currentPage+10}">다음 10개</a></li>
					</ul>
				</div>
			</div>

		</div>
		<!-- /content -->
		
		<!-- start '글 등록' modal -->
		<div id="register_article" class="modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
				        <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4>글 작성/편집</h4>
						</div>
				    </div>
					<form action="../admin/registerArticle.do" method="post" enctype="multipart/form-data">
						<table class="table table-bordered">
							<tr>
								<th>제목</th>
								<td><input type="text" name="title" id="title" style="width:90%;" required/></td>
							</tr>
							<tr>
								<th>이미지</th>
								<td>
									<input type="file" name="noticeImage" id="noticeImage"/> <!-- file -->
								</td>
							</tr>
							<tr>
								<th>내용</th>
								<td>
									<textarea rows="10" style="width:90%;" name="content" id="content" required></textarea>
								</td>
							</tr>
						</table>
						<div class="footer">
							<input class="btn btn-default" type="submit" value="완료">
						</div>
					</form>				
				</div><!-- div modal-content -->
			</div>
		</div>
		<!-- end '글 등록' modal -->
	
		
	
	</div>
	<!-- /container -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />

<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker(); //  datepicker
	
	$('#change_open').on('click',function(){
		var url = "/admin/updateOpenInfo.do"; 
		$(location).attr('href',url);
	});
	
	$('#title').keyup(function (e){
	    chkword(this, 50);
	});
	$('#title').keyup();
	
	$('#content').keyup(function (e){
	    chkword(this, 5000);
	});
	$('#content').keyup();
	
	$('#title_edit').keyup(function (e){
	    chkword(this, 50);
	});
	$('#content_edit').keyup(function (e){
	    chkword(this, 5000);
	});
	/*
	$('.attatchedNoticeImage').on('click', function() {
// 		$("#attachedImageModal").dialog("open");
// 		$("#attachedImageModal").dialog({autoOpen:true});
		$('#attachedImageModal').modal({show:true});
	});
	
// 			$(".attachedImageModal").dialog({autoOpen:true});
	$('.attachedImageModal .modal-close').on('click', function() {
		//$(this).data('dismiss', 'modal');
		$(".attachedImageModal").modal("close");
// 		$('.modal-close').data('dismiss', 'modal');
// 		$(".attachedImageModal").dialog({autoOpen:false});
// 		$('#attachedImageModal').modal("hidden");
// 		$('#attachedImageModal').trigger('hide');
	});
	*/
	
	function chkword(obj, maxByte) {
	    var strValue = obj.value;
	    var strLen;
	    if(strValue == null) {
	    	strLen = 0;
	    } else{
	    	strLen = strValue.length;
	    }
	    var totalByte = 0;
	    var len = 0;
	    var oneChar = "";
	    var str2 = "";

	    for (var i = 0; i < strLen; i++) {
	        oneChar = strValue.charAt(i);
	        if (escape(oneChar).length > 4) {
	            totalByte += 2;
	        } else {
	            totalByte++;
	        }

	        // 입력한 문자 길이보다 넘치면 잘라내기 위해 저장
	        if (totalByte <= maxByte) {
	            len = i + 1;
	        }
	    }

	    // 넘어가는 글자는 자른다.
	    if (totalByte > maxByte) {
	        alert(maxByte + "byte를 초과 입력 할 수 없습니다.");
	        str2 = strValue.substr(0, len);
	        obj.value = str2;
	        chkword(obj, 4000);
	    }
	    
	    return totalByte;
	}	//chkword function
	
	$('.pagination li').on('click', function(event) {
		if(0 <= $(this).prop('class').indexOf("disabled")) {
			event.preventDefault();
			return;
		}
	});

	$("input[type=submit]").on('click', function(event) {
		var fileName = $("#noticeImage").val();
		if(fileName != "" && fileName != null) {
			var extName = fileName.slice(fileName.lastIndexOf(".") + 1).toLowerCase();
			if(extName != "jpg" && extName != "png" && extName != "jpeg") {
				alert("해당 확장자 파일은 업로드 할 수 없습니다.");
				event.preventDefault();
				return;
			}
		}
	});
});

function closeOneModal(noticeSeq) {
	var id = "#attachedImageModal_" + noticeSeq;
	console.log("click: " + id);
	$(id).css('display','none');
}

</script>
</body>
</html>