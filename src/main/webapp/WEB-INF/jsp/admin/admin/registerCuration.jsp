<%@page import="org.springframework.web.context.request.SessionScope"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>큐레이션 등록</title>
	<style type="text/css">
		.ui-menu {
			list-style:none;
			padding: 2px;
			margin: 0;
			display:block;
			float: left;
			font-size: 13px;
			
			max-height: 170px;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 10px;
		}
		.ui-menu .ui-menu {
			margin-top: -3px;
		}
		.ui-menu .ui-menu-item {
			margin:0;
			padding: 0;
			zoom: 1;
			float: left;
			clear: left;
			width: 100%;
		}
		.ui-menu .ui-menu-item a {
			
			text-decoration:none;
			display:block;
			padding:.2em .4em;
			line-height:1.5;
			zoom:1;
		}
		.ui-menu .ui-menu-item a.ui-state-hover,
		.ui-menu .ui-menu-item a.ui-state-active {
			
			font-weight: normal;
			margin: -1px;
		}
	</style>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li class="active"><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>큐레이션 등록/수정</h1>
			</div>
	
			<div class="col col-12">
			<form action="../admin/registerCuration.do" method="post">
				<input type="hidden" name="seq" id="seq" value="${curation.curation_Seq }">
				<table class="table table-bordered">
					<tr>
						<th>큐레이션명*</th>
						<td>
							<p class="text-right" id="nameSize"></p>
							<input type="text" name="Curation_Nm" style="width:90%;" id="Curation_Nm" value="${fn:replace(curation.curation_Nm,'<br>','/BR/') }" maxlength="50" required/>
<%-- 								<textarea name="Curation_Nm" style="width:90%;" id="Curation_Nm" maxlength="50" required>${curation.curation_Nm }</textarea> --%>
						</td>
					</tr>
					
					<tr>
						<th>한 줄 소개글*</th>
						<td>
							<p class="text-right" id="introSize"></p>
							<input type="text" name="Line_Introduction" style="width:90%;" id="Line_Introduction" value="${fn:replace(curation.line_Introduction,'<br>','/BR/')}" maxlength="40" required/>
<%-- 							<textarea name="Line_Introduction" style="width:90%;" id="Line_Introduction" maxlength="40" required>${curation.line_Introduction}</textarea> --%>
						</td>
					</tr>
					<tr>
						<th>포함 과정*</th>
						<td>
							<div align="left">
								<c:if test="${processList ne null and fn:length(processList) > 0}">
									<c:forEach var="process" items="${processList }" varStatus="status">
										<b>${status.count }: ${process.process_Code }</b> - ${process.process_Nm } <b>(
											<c:choose>
												<c:when test="${process.public_Yn eq 'Y'}">
													공개
												</c:when>
												<c:otherwise>
													비공개
												</c:otherwise>
											</c:choose>
											<c:if test="${process.block_Yn eq 'Y' }">
												 / 차단
											</c:if>
										)</b> <br />
										
										<input type="hidden" name="CurationProcessOrder" value="${status.count }">
										<input type="hidden" name="Process_Code" class="Process_Code" value="${process.process_Code }">
									</c:forEach>
								</c:if>
							</div>
							<button class="btn btn-default" type="button" id="curationProcessAddViewBtn" data-toggle="modal" data-target="#modal_template" value="${curation.curation_Seq }">과정 추가/편집</button>
						</td>
					</tr>
					<tr>
						<th>노출기간*</th>
						<td>
							<input type="text" class="datepicker" name="start_day" id="start_period" value="${cfn:dateFmt3(curation.expression_Start_Day)}" required/> ~ <input type="text" class="datepicker" name="end_day" id="end_period" value="${cfn:dateFmt3(curation.expression_CLOSE_Day)}" required/>
						</td>
					</tr>
					<tr>
						<th>사용여부</th>
						<td>
							<input type="hidden" id="UseYn" value="${curation.use_Yn }">
							<label><input type="radio" name="Use_Yn" value="Y" id="UseYnY"> 사용</label>
							<label><input type="radio" name="Use_Yn" value="N" id="UseYnN"> 미사용</label>
						</td>
					</tr>
				</table>
				<div align="center">
					<button class="btn btn-default" type="button" id="register_curation">등록</button>
					<button class="btn btn-default" type="button" onClick="javascript:self.location='curation.do';">목록으로</button>
				</div>
			</form>	<!-- end form -->
			</div>
		</div>
		<!-- /content -->
		
		<!-- start modal -->
		<div id="modal_template" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
					    <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4 id="title"></h4>
						</div>
					</div>
					<div class="content">
					
					</div>
				</div>
			</div>
		</div>
		<!-- end modal -->
		
	</div>
	<!-- /container -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />

<script type="text/javascript">
$(function() {
	$('.datepicker').datepicker(); //  datepicker
	
	$('#Curation_Nm').keyup(function (e){
	    $('#nameSize').html(chkword(this, 50) + ' / 50 Byte');
	});
	$('#Curation_Nm').keyup();
	
	$('#Line_Introduction').keyup(function (e){
	    $('#introSize').html(chkword(this, 40) + ' / 40 Byte');
	});
	$('#Line_Introduction').keyup();
	
	
	var chkUseYn = $('#UseYn').val();
	if(chkUseYn == 'N') {
		$('#UseYnN').prop('checked', true);
	} else {
		$('#UseYnY').prop('checked', true);
	}
	
	$('input[name=Use_Yn]').on('click', function() {
		$('#UseYn').val($(this).val());
	});
	
	var chkCpList = ""
	$('.Process_Code').each(function(i, element) {
		chkCpList += $(element).val() + "::";
	});
	
	var cpListLength = $('.Process_Code').length;
	console.log(cpListLength);
	
	
	//과정 추가/편집 모달
	$('#curationProcessAddViewBtn').on('click', function() {
		$("#modal_template div.content").load("../admin/curation/modal/curationProcessAddView.do", {
			Curation_Sequence : $(this).val(),
			chkCpList : chkCpList,
			Curation_Nm: $('#Curation_Nm').val(),
			Line_Introduction: $('#Line_Introduction').val(),
			start_period: $('#start_period').val(),
			end_period: $('#end_period').val(),
			Use_Yn: $('#UseYn').val()
// 			Process_Code :  $(this).val()
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			}
			$("#title").html("과정 추가/편집");
		});
	});
	
	
	$('#register_curation').on('click', function() {
		if (false == validationCheck(cpListLength)) {
			return;
		}
		alert($('#Curation_Nm').val());
		$('form').submit();
	});
	
});

function chkword(obj, maxByte) {
    var strValue = obj.value;
    var strLen = strValue.length;
    var totalByte = 0;
    var len = 0;
    var oneChar = "";
    var str2 = "";

    for (var i = 0; i < strLen; i++) {
        oneChar = strValue.charAt(i);
        if (escape(oneChar).length > 4) {
            totalByte += 2;
        } else {
            totalByte++;
        }

        // 입력한 문자 길이보다 넘치면 잘라내기 위해 저장
        if (totalByte <= maxByte) {
            len = i + 1;
        }
    }

    // 넘어가는 글자는 자른다.
    if (totalByte > maxByte) {
        alert(maxByte + "Byte를 초과 입력 할 수 없습니다.");
        str2 = strValue.substr(0, len);
        obj.value = str2;
        chkword(obj, 4000);
    }
    
    return totalByte;
}	//chkword function

function validationCheck(cpListLength) {
	var validformat = /^\d{4}\-\d{2}\-\d{2}$/; 
	
	if(null == $('#Curation_Nm').val() || "" == $('#Curation_Nm').val()) {
		alert("큐레이션명을 작성해 주세요.");
		$("#Curation_Nm").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if(null == $('#Line_Introduction').val() || "" == $('#Line_Introduction').val()) {
		alert("한 줄 소개글을 작성해 주세요.");
		$("#Line_Introduction").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	}else if (null == $('#start_period').val() || "" == $('#start_period').val() || !validformat.test($('#start_period').val())) {
		alert("기간을 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if (null == $('#end_period').val() || "" == $('#end_period').val() || !validformat.test($('#end_period').val())) {
		alert("기간을 선택해 주세요.");
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('#end_period').val() < $('#start_period').val()) {
		alert("기간이 잘못 선택되었습니다.\n다시 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if(cpListLength <= 0) {
		alert("포함 과정을 선택해주세요.");
		return false;
	} else {
		return true;
	}
	return false;
};
</script>
</body>
</html>