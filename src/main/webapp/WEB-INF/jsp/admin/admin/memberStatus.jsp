<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>회원현황</title>
	<style type="text/css">
		#container img {
			width: 40%;
			height: auto;
		}
	</style>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />

	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li class="active"><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			
			<div class="page-header">
				<h1 id="member_status">회원현황</h1>
			</div>
		
			<!-- start id="searchForm" -->
			<form id="searchForm" action="../admin/memberStatus.do" method="get" >
			<input type="hidden" name="search" id="search" value="${search }"/>
			<div class="col col-12 col-md-12" style="margin-bottom:20px;">
				<h5>현재까지 총 회원 수: <b>${memberTotCnt }명</b> (신규: ${memJoinNewCnt} / 유입: ${memJoinOldCnt}) </h5>
				
				<table class="table table-bordered">
					<tr>
						<th>가입기간</th>
						<td>
							<input type="text" class="datepicker" name="start_period" id="start_period" required/> ~ <input type="text" class="datepicker" name="end_period" id="end_period" required/>
							<button type="button" class="btn btn-default search_period" value="1">전체</button>
							<button type="button" class="btn btn-default search_period" value="2">오늘</button>
							<button type="button" class="btn btn-default search_period" value="3">1주일</button>
							<button type="button" class="btn btn-default search_period" value="4">1개월</button>
							<button type="button" class="btn btn-default search_period" value="5">3개월</button>
							<button type="button" class="btn btn-default search_period" value="6">6개월</button>
							<button type="button" class="btn btn-default search_period" value="7">1년</button>
						</td>
					</tr>
					<tr>
						<th>회원구분</th>
						<td>
							<label><input type="checkbox" name="member_classification" class="member_classification" value="41" />신규회원</label>
							<label><input type="checkbox" name="member_classification" class="member_classification" value="42" />(휴넷)유입회원</label>
						</td>
					</tr>
					<tr>
						<th>수강과정 유무</th>
						<td>
							<label><input type="checkbox" name="course_progress" class="course_progress" value="not_progress" />없음</label>
							<label><input type="checkbox" name="course_progress" class="course_progress" value="progressing" />진행중</label>
							<label><input type="checkbox" name="course_progress" class="course_progress" value="end_progress" />기간종료</label>
						</td>
					</tr>
					<tr>
						<th>과정개설 유무</th>
						<td>
							<label><input type="checkbox" name="process_opening" class="process_opening" value="not_opening" />없음</label>
							<label><input type="checkbox" name="process_opening" class="process_opening" value="processing" />제작중</label>
							<label><input type="checkbox" name="process_opening" class="process_opening" value="opening" />오픈</label>
						</td>
					</tr>
				</table>

				<div>				
					<button type="submit" class="btn btn-default col-3" id="searchBtn" style="float: left;">검색하기</button>
					&nbsp;검색결과: ${searchCnt }명
					
					<input type="hidden" name="excel" value="" id="excel" />
					<div style="float: right;">
<!-- 						<a href="../memberDownload.do" id="downloadBtn" class="btn btn-default" style="margin-right:1px;">엑셀문서 다운로드</a> -->
						<button id="downloadBtn" type="button" class="btn btn-default" style="margin-right:1px;">엑셀문서 다운로드</button>
					</div>
				</div>
			</div>
			
			<div>
				<div style="float: left; margin-bottom:6px;">
					<select name="searchCategory">
						<option value="Nickname" <c:if test="${chk_searchCategory eq 'Nickname'}">selected="selected"</c:if>>닉네임</option>
						<option value="User_Id" <c:if test="${chk_searchCategory eq 'User_Id'}">selected="selected"</c:if>>ID</option>
						<option value="Email" <c:if test="${chk_searchCategory eq 'Email'}">selected="selected"</c:if>>이메일</option>
						<option value="Mobile_No" <c:if test="${chk_searchCategory eq 'Mobile_No'}">selected="selected"</c:if>>휴대폰 번호</option>
					</select>
					<input type="text" style="width:200px;" name="content" <c:if test="${searchText ne null and searchText ne ''}">value="${searchText}"</c:if>> <button class="btn btn-default" type="submit" id="textSearchBtn">검색</button>
				</div>
				
				<div style="float: right; margin-bottom:6px;">
					정렬기준 &nbsp;
					<select name="ordering" id="ordering">
						<option value="Regist_Date" <c:if test="${chk_ordering eq 'Regist_Date'}">selected="selected"</c:if>>최근가입일 순</option>
						<option value="TakeLecture_OnGoing_Count" <c:if test="${chk_ordering eq 'TakeLecture_OnGoing_Count'}">selected="selected"</c:if>>많은 수강과정 순</option>
						<option value="Process_Count" <c:if test="${chk_ordering eq 'Process_Count'}">selected="selected"</c:if>>많은 개설과정 순</option>
					</select>
				</div>
			</div>
			
			</form>
			<!-- end id="searchForm" -->
			
			<div class="memberList">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>회원번호</th>
							<th>회원구분</th>
							<th>가입일</th>
							<th>ID/<br/>닉네임</th>
							<th>프로필 이미지</th>
							<th>이메일</th>
							<th>휴대폰 번호</th>
							<th>수강 과정수</th>
							<th>개설 과정수</th>
							<th>이메일<br/>수신 동의</th>
							<th>SMS<br/>수신 동의</th>
							<th>가입 경로</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${memberList ne null and fn:length(memberList) > 0}">
								<c:forEach var="member" items="${memberList }">
									<tr>
										<td>${member.user_Seq }</td>
										<td>
											<c:choose>
												<c:when test="${member.join_Path_Code eq 41}">신규</c:when>
												<c:when test="${member.join_Path_Code eq 42}">유입</c:when>
											</c:choose>
										</td>
										<td>${cfn:dateFmt2(member.regist_Date) }</td>
										<td><a href="https://admin.hunet.co.kr/Hunet/Member/View.aspx?flag=&type=list&userId=${member.user_Id }">${member.user_Id }</a> /<br/>${member.nickname }</td>
										<td>
											<c:choose>
												<c:when test="${member.profile_Image == null || member.profile_Image == ''}">
													미등록
												</c:when>
												<c:otherwise>
<%-- 													<input type="hidden" name="Regist_Profile_Img_Url_${member.user_Code }" value="${member.regist_Profile_Img_Url }"> --%>
													<button type="button" class="btn btn-default profileBtn" value="${member.user_Seq }" data-url="${member.regist_Profile_Img_Url }" data-toggle="modal" data-target="#modal_template">보기</button>
													
												</c:otherwise>
											</c:choose>
										</td>
										<td>${member.email}</td>
										<td>${member.mobile}</td>
										<td>
											<div align="left">
												진행중: ${member.takeLecture_Studying_Count } <br /> <input type="hidden" id="studyingCnt_${member.user_Id }" value="${member.takeLecture_Studying_Count }" />
												종료: ${member.takeLecture_Expire_Count } <br /> <input type="hidden" id="expireCnt_${member.user_Id }" value="${member.takeLecture_Expire_Count }" />
											</div>
												<button type="button" class="btn btn-default takeLectureDetailBtn" value="${member.user_Id }" data-toggle="modal" data-target="#modal_template">보기</button>
										</td>
										<td>
											<div align="left">
												제작중: ${member.making_Process_Count } <br /> <input type="hidden" id="makingCnt_${member.user_Id }" value="${member.making_Process_Count }" />
												오픈: ${member.open_Process_Count } <br /> <input type="hidden" id="openCnt_${member.user_Id }" value="${member.open_Process_Count }" />
											</div>
												<button type="button" class="btn btn-default makeLectureDetailBtn" value="${member.user_Id }" data-toggle="modal" data-target="#modal_template">보기</button>
										</td>
										<td>${member.email_Yn }</td>
										<td>${member.sms_Yn }</td>
										<td>
											<c:choose>
												<c:when test="${fn:length(member.join_Path_Url) > 30}">
													${fn:substring(member.join_Path_Url,0, 30)}...
												</c:when>
												<c:otherwise>
													${fn:substring(member.join_Path_Url,0, 30)}
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="12">데이터가 존재하지 않습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>	
				</table>
			</div>
			<!-- end div class="memberList" -->
			<!-- start pagination -->
			<c:if test="${memberList ne null and fn:length(memberList) > 0}">
				<div class="pagination-centered" align="center">
					<ul class="pagination">
						<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a href="../admin/memberStatus.do" data-pn="${currentPage-10}">이전 10개</a></li>
						<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>><a href="../admin/memberStatus.do" data-pn="${currentPage-1}">&laquo;</a></li>
						<c:forEach var="pn" items="${pageNumbers}">
							<c:choose>
								<c:when test="${currentPage==pn}">
									<li class="active"><a
										href="../admin/memberStatus.do" data-pn="${pn}">${pn}</a></li>
								</c:when>
								<c:otherwise>
									<li><a
										href="../admin/memberStatus.do" data-pn="${pn}">${pn}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a href="../admin/memberStatus.do" data-pn="${currentPage+1}">&raquo;</a></li>
						<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>><a href="../admin/memberStatus.do" data-pn="${currentPage+10}">다음 10개</a></li>
					</ul>
				</div>
			</c:if>
			<!-- end pagination -->
		</div>
		<!-- /content -->
		
		<!-- start modal -->
		<div id="modal_template" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
					    <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4 id="title"></h4>
						</div>
					</div>
					<div class="content">
					
					</div>
				</div>
			</div>
		</div>
		<!-- end modal -->
		
		
	</div>
	<!-- /container -->
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />
	
<script type="text/javascript">
$(function(){
	$('.datepicker').datepicker(); //  datepicker
	var search = $('#search').val();
	
	//기본 검색 체크값 세팅
	var now = new Date();
	if(search == "Y") {	//검색한 후 
		$('input:checkbox').prop("checked", false);
		$('#start_period').val('${chk_start}');
		$('#end_period').val('${chk_end}');
		
		<c:forEach var="item" items="${chk_memClass}">
			if('${item}' != null && '${item}' != '') {
				$("input:checkbox[value="+'${item}'+"].member_classification").prop("checked", true);
			}
		</c:forEach>
	
		<c:forEach var="item" items="${chk_courseProgress}">
			if('${item}' != null && '${item}' != '') {
				$("input:checkbox[value="+'${item}'+"].course_progress").prop("checked", true);
			}
		</c:forEach>
		
		<c:forEach var="item" items="${chk_processOpening}">
			if('${item}' != null && '${item}' != '') {
				$("input:checkbox[value="+'${item}'+"].process_opening").prop("checked", true);
			}
		</c:forEach>
		
		if("${chk_ordering}" != null && "${chk_ordering}" != '') {
			$('select[name=ordering] option[value='+"${chk_ordering}"+']').prop("selected", true);
		}
		
	} else {	//기본
		$('input:checkbox').prop("checked", true);
		$('#start_period').datepicker("setDate", '2016-01-01');
		$('#end_period').datepicker("setDate", now.getFullYear() +"-"+(now.getMonth() +1) +"-"+now.getDate() +" "+now.getHours() + ":" +now.getMinutes() + ":"+now.getSeconds() +"." +now.getMilliseconds() );
	}
	
	//프로필 보기 버튼 눌렀을 때
	$('.profileBtn').on('click', function(event) {
		var value= $(this).val();
		var url = $(this).data("url");
		$("#modal_template div.content").load("../admin/member/modal/profileImage.do", {
			User_Seq : value,
			Regist_Profile_Img_Url : url
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("프로필 이미지");
		});
	});
	
	//수강과정 수 보기 눌렀을 때 
	$('.takeLectureDetailBtn').on('click', function(event) {
		var value= $(this).val();
		$("#modal_template div.content").load("../admin/member/modal/takeLectureDetail.do", {
			User_Id : value,
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("수강 과정<small> 진행 중: "+$('#studyingCnt_'+value).val()+" / 종료: "+$('#expireCnt_'+value).val()+"</small>");
		});
	});
	
	//개설과정 수 보기 눌렀을 때 
	$('.makeLectureDetailBtn').on('click', function(event) {
		var value= $(this).val();
		$("#modal_template div.content").load("../admin/member/modal/makeLectureDetail.do", {
			User_Id : value,
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("개설 과정<small> 제작 중: "+$('#makingCnt_'+value).val()+" / 오픈: "+$('#openCnt_'+value).val()+"</small>");
		});
	});

	var qStr = window.location.search;
	
	//엑셀 다운로드 버튼 눌렀을 때 
	$('#downloadBtn').on('click', function(evnet) {
		$('#excel').val('Y');
		$('form').submit();			
	});
	
	//submit 버튼 누르면, search 값 보내기
	$('#searchBtn').on('click', function() {
		if (false == validationCheck()) {
			event.preventDefault();
			return;
		}
		
		$('#excel').val('N');
		$('#search').val('Y');
		
		$('#searchForm').submit();
	});
	
	//글자로 검색
	$('#textSearchBtn').on('click', function() {
		if (false == validationCheck()) {
			event.preventDefault();
			return;
		}
		$('#excel').val('N');
		$('#search').val('Y');
		$('#searchForm').submit();
	});
	
	//정렬기준 값 바뀌면 searchForm submit
	$('#ordering').on('change', function(event) {
		if (false == validationCheck()) {
			event.preventDefault();
			return;
		}
// 		$(this).closest('form').trigger('submit');
		$('#excel').val('N');
		$('#searchForm').submit();
	});
	
	$('.pagination a').on('click', function(event) {
		if(0 <= $(this).parent().prop('class').indexOf("disabled")) {
			event.preventDefault();
			return;
		}
		var pn =$(this).data("pn");
		
		if(search == "Y") {
			$('#search').val('Y');
			if(0 <= qStr.indexOf("pn")) {
		 		qStr = qStr.substr(0,qStr.indexOf("&pn="));
			}
			$(this).attr("href", $(this).attr("href") +qStr + "&pn="+pn);
			$(this).unbind("click");
		} else {
			$('#ordering').val($('#ordering').val());
			$(this).attr("href", $(this).attr("href") + "?pn="+pn+"&ordering="+$('#ordering').val());
			$(this).unbind("click");
		}
	});
	
	$('.datepicker').on('click', function() {
		$('.search_period').removeClass('active');
	});
	
	$('.search_period').on('click',function(event){
		$('.search_period').removeClass('active');
		$(this).addClass('active');
		var code = $(this).val()
		
// 		var dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		var d = new Date();
		
		var lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
		if(code == '1') {	//전체
			$( "#start_period" ).datepicker("setDate", new Date("2016/01/01"));
		} else if(code == '2') {	//오늘
			$( "#start_period" ).datepicker("setDate", new Date() );
		} else if(code == '3') {	//1주일
			var date = d.getDate() - 7;
			d.setDate(date);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '4') {	//1개월
			lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -1;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '5') {	//3개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 3), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -3;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '6') {	//6개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 6), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -6;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '7') {	//1년
			var year = d.getFullYear() -1;
			d.setFullYear(year);
			$( "#start_period" ).datepicker("setDate", d );
		}
		
		d = new Date();
		$( "#end_period" ).datepicker("setDate", d.getFullYear() +"-"+(d.getMonth() +1) +"-"+d.getDate() +" "+d.getHours() + ":" +d.getMinutes() + ":"+d.getSeconds() +"." +d.getMilliseconds() );
    });
});

function validationCheck() {
	var validformat = /^\d{4}\-\d{2}\-\d{2}$/; 
	if (null == $('#start_period').val() || "" == $('#start_period').val() || !validformat.test($('#start_period').val())) {
		alert("접수일을 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if (null == $('#end_period').val() || "" == $('#end_period').val() || !validformat.test($('#end_period').val())) {
		alert("접수일 선택해 주세요.");
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('#end_period').val() < $('#start_period').val()) {
		alert("접수일이 잘못 선택되었습니다.\n다시 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('input[name=member_classification]:checked').length == 0) {
		alert("회원구분을 선택해주세요.");
		return false;
	} else if ($('input[name=course_progress]:checked').length == 0) {
		alert("수강과정 유무를 선택해주세요.");
		return false;
	} else if ($('input[name=process_opening]:checked').length == 0) {
		alert("과정개설 유무를 선택해주세요.");
		return false;
	} else {
		true;
	}
};
</script>
</body>
</html>