<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>고객센터</title>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li class="active"><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>고객센터</h1>
			</div>
			
			<form action="../admin/customerService.do" method="post" id="searchForm">
			<input type="hidden" name="chkSearch" id="chkSearch" value=${chkSearch eq 'Y'.toString() ? 'Y' : 'N'}>
			<div class="col col-12 col-md-12">
				<h5>총 문의글: <b>${totalItems}개</b>  (답변대기: ${applyNCount} / 답변완료: ${applyYCount}) </h5>
				
				<table class="table table-bordered">
					<tr>
						<th>접수일</th>
						<td>
							<input type="text" class="datepicker" name="start_period" id="start_period" required <c:if test="${start_period ne null and start_period ne ''}" >value="${start_period}"</c:if>/> ~ <input type="text" class="datepicker" name="end_period" id="end_period" required <c:if test="${end_period ne null and end_period ne ''}" >value="${end_period}"</c:if>/>
							<button type="button" class="btn btn-default search_period" value="1">전체</button>
							<button type="button" class="btn btn-default search_period" value="2">오늘</button>
							<button type="button" class="btn btn-default search_period" value="3">1주일</button>
							<button type="button" class="btn btn-default search_period" value="4">1개월</button>
							<button type="button" class="btn btn-default search_period" value="5">3개월</button>
							<button type="button" class="btn btn-default search_period" value="6">6개월</button>
							<button type="button" class="btn btn-default search_period" value="7">1년</button>
						</td>
					</tr>
					<tr>
						<th>답변상태</th>
						<td>
							<label><input type="checkbox" id="answer_delay" name="answer_delay" value="true" <c:if test="${answerDelay == true}">checked </c:if>/>답변대기</label>
							<label><input type="checkbox" id="answer_complete" name="answer_complete" value="true" <c:if test="${answerComplete == true}">checked</c:if>/>답변완료</label>
						</td>
					</tr>
				</table>
				
				<button type="button" class="btn btn-default col-3" id="searchButton" onclick="searchClick();">검색하기</button>
				<c:if test="${searchCount ne null and searchCount > 0}">
					검색결과: ${searchCount}명
				</c:if>
			</div>
			</form>
			
			
				<div>
					<div id="switching" style="float:left; width:50%; padding:25px 0px 8px 0px; text-align:left;">
						<select id="searchCategory" name="searchCategory">
							<option value="N" <c:if test="${searchCategory eq 'N'.toString()}">selected="selected"</c:if>>닉네임</option>
							<option value="I" <c:if test="${searchCategory eq 'I'.toString()}">selected="selected"</c:if>>ID</option>
							<option value="T" <c:if test="${searchCategory eq 'T'.toString()}">selected="selected"</c:if>>제목</option>
						</select>
						<input type="text" id="searchText" name="searchText" style="width:200px;" <c:if test="${searchText ne null and searchText ne ''}">value="${searchText}"</c:if>> <input class="btn btn-default" type="button" value="검색" onclick="searchClick();">
					</div>
				</div>
				<div style="float: right; width:50%; padding:25px 0px 8px 0px; text-align: right;">
					<div class="col-md-offset-8">
					정렬기준 &nbsp;
						<select id="orderBy" name="orderBy" onchange="orderChangeSearch()">
							<option value="D" <c:if test="${orderBy eq 'D'}">selected="selected"</c:if>>최근접수일 순</option>
							<option value="H" <c:if test="${orderBy eq 'H'}">selected="selected"</c:if>>말머리순</option>
							<option value="A" <c:if test="${orderBy eq 'A'}">selected="selected"</c:if>>답변상태순</option>
						</select>
					</div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>구분</th>
							<th>ID/닉네임</th>
							<th>연락처</th>
							<th>이메일 주소</th>
							<th>제목</th>
							<th>답변 상태</th>
							<th>접수일시</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${customerList ne null and fn:length(customerList) > 0}">
								<c:forEach var="cusList" items="${customerList}" varStatus="status">
									<tr>
										<td>${(searchCount+1) - (status.count + (currentPage-1)*itemSize)} </td>
										<td>
											<c:forEach var="code" items="${ category }">
												<c:if test="${ cusList.cust_Center_Category eq code.code}">
													${code.code_Nm }
												</c:if>
											</c:forEach>
										</td>
										<td>${cusList.regist_Id} / ${cusList.nickname}</td>
										<td>${cusList.mobile}</td>
										<td>${cusList.email}</td>
										<td>
											<div align="left">
											<a href="#" class="qnaDetailBtn" data-toggle="modal" data-target="#modal_template" value="${cusList.cust_Center_Seq }">
												${cusList.title }
											</a>
											</div>
										</td>
										<c:choose>
											<c:when test="${cusList.apply_Yn eq 'Y'}">
												<td>답변완료</td>
											</c:when>
											<c:otherwise>
												<td style="color: red;">답변대기</td>
											</c:otherwise>
										</c:choose>
										<td>${cfn:dateFmt2(cusList.regist_Date)}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="8">데이터가 존재하지 않습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- /content -->
			
			<!-- 페이징 -->
			<c:if test="${customerList ne null and fn:length(customerList) > 0}">
			<div class="pagination-centered" align="center">
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>>
						<c:choose>
							<c:when test="${currentPage-10 <= 0}">
								<a href="#">이전 10개</a>
							</c:when>
							<c:otherwise>
								<a href="../admin/customerService.do?pn=${currentPage-10}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">이전 10개</a>
							</c:otherwise>
						</c:choose>
					</li>
					<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>>
						<c:choose>
							<c:when test="${currentPage-1 == 0}">
								<a href="#">&laquo;</a>
							</c:when>
							<c:otherwise>
								<a href="../admin/customerService.do?pn=${currentPage-1}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">&laquo;</a>
							</c:otherwise>
						</c:choose>
					</li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active">
								<a href="../admin/customerService.do?pn=${pn}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li>
								<a href="../admin/customerService.do?pn=${pn}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>>
					<c:choose>
						<c:when test="${currentPage+1 > numOfPages}">
							<a href="#">&raquo;</a>
						</c:when>
						<c:otherwise>
							<a href="../admin/customerService.do?pn=${currentPage+1}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">&raquo;</a>
						</c:otherwise>
					</c:choose>
					</li>
					<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>>
					<c:choose>
						<c:when test="${currentPage+10 > numOfPages}">
							<a href="#">다음 10개</a>
						</c:when>
						<c:otherwise>
							<a href="../admin/customerService.do?pn=${currentPage+10}&chkSearch=${chkSearch}&start_period=${start_period}&end_period=${end_period}&answer_delay=${answerDelay}&answer_complete=${answerComplete}&searchText=${searchText}&searchCategory=${searchCategory}&orderBy=${orderBy}">다음 10개</a>
						</c:otherwise>
					</c:choose>
					</li>
				</ul>
			</div>
			</c:if>
			<!-- /페이징 -->
		</div>
		<!-- /container -->
		
		
		<!-- start modal -->
		<div id="modal_template" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
					    <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4 id="title"></h4>
						</div>
					</div>
					<div class="content">
					
					</div>
				</div>
			</div>
		</div>
		<!-- end modal -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />
	
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker(); 			// datepicker
	$('[name="Apply_Ip"]').val(ip());	 	// 현재 접속한 PC의 IP 세팅
	$('[name="Apply_Id"]').val();			// 접속한 아이디 입력
	
	$('.qnaDetailBtn').on('click', function() {
// 		console.log('Cust_Center_Seq : '+ $(this).attr('value'));
		$("#modal_template div.content").load("../admin/custService/modal/qnaDetail.do", {
			Cust_Center_Seq :  $(this).attr('value')
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			}
			$("#title").html("내용보기");
		});
	});
	
	$('.pagination li').on('click', function(event) {
		if($(this).prop('class').indexOf("disabled") >= 0) {
			event.preventDefault();
			return;
		}
	});
	
	if($('#chkSearch').val() != 'Y') {
		var d = new Date();
		$( "#start_period" ).datepicker("setDate", new Date("2016/01/01"));
		$( "#end_period" ).datepicker("setDate", dateFormatChange(d));
	}
	
	$('.datepicker').on('click', function() {
		$('.search_period').removeClass('active');
	});
	
	$('.search_period').on('click',function(event){
		$('.search_period').removeClass('active');
		$(this).addClass('active');
		var code = $(this).val();
		var d = new Date();
		var lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
		
		if(code == '1') {	//전체
			$( "#start_period" ).datepicker("setDate", new Date("2016/01/01"));
		} else if(code == '2') {	//오늘
			$( "#start_period" ).datepicker("setDate", new Date() );
		} else if(code == '3') {	//1주일
			var date = d.getDate() - 7;
			d.setDate(date);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '4') {	//1개월
			lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -1;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '5') {	//3개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 3), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -3;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '6') {	//6개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 6), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -6;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '7') {	//1년
			var year = d.getFullYear() -1;
			d.setFullYear(year);
			$( "#start_period" ).datepicker("setDate", d );
		}
		
		d = new Date();
		$( "#end_period" ).datepicker("setDate", dateFormatChange(d));
    });
});


function searchClick() {
	if (false == validationCheck()) 
		return;
	
	var form = document.getElementById("searchForm");
	$('#chkSearch').val('Y');
	
	form.appendChild(document.getElementById("searchCategory"));	
	form.appendChild(document.getElementById("searchText"));
	form.appendChild(document.getElementById("orderBy"));
	document.body.appendChild(form);
	var startDate = $('#start_period').val();
	var endDate = $('#end_period').val();
	
	form.submit();
};

function orderChangeSearch() {	
	var form = document.getElementById("searchForm");
	
	if ('' != $('#searchText').val()) {
		form.appendChild(document.getElementById("searchText"))
		form.appendChild(document.getElementById("searchCategory"))
	}
	form.appendChild(document.getElementById("orderBy"))
	document.body.appendChild(form);
	form.submit();
};

function dateFormatChange(date){
    function pad(num) {
        num = num + '';
        return num.length < 2 ? '0' + num : num;
    }    
    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate()) + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
};

function validationCheck() {
	var validformat = /^\d{4}\-\d{2}\-\d{2}$/; 
	if (null == $('#start_period').val() || "" == $('#start_period').val() || !validformat.test($('#start_period').val())) {
		alert("가입기간을 다시 설정해주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if (null == $('#end_period').val() || "" == $('#end_period').val() || !validformat.test($('#end_period').val())) {
		alert("가입기간을 다시 설정해주세요.");
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('#end_period').val() < $('#start_period').val()) {
		alert("가입기간을 다시 설정해주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if (!$("#answer_delay").is(":checked") && !$("#answer_complete").is(":checked")) {
		alert("답변 상태 조건을 선택해 주세요.");
		return false;
	} else {
		true;
	}
};

</script>
</body>
</html>