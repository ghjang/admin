<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>큐레이션</title>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li class="active"><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>큐레이션</h1>
			</div>
			
			<div style="padding-top:3px; padding-bottom: 35px;">
				<div id="switching" style="float: left; width: 50%; text-align: left;">
					<input class="btn btn-default" type="button" id="orderCurationBtn" value="순서변경" data-toggle="modal" data-target="#modal_template">
				</div>
				<div style="float: right; width: 50%; text-align: right;">
					<input class="btn btn-default" type="button" value="큐레이션 등록" onClick="javascript:self.location='registerCurationView.do';">
				</div>
			</div>
			
			<div class="col col-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>순서</th>
							<th>큐레이션명</th>
							<th>한 줄 소개글</th>
							<th>포함 과정명</th>
							<th>사용여부</th>
							<th>노출기간</th>
							<th>비고</th>
						</tr>
					</thead>
					<tbody>
					<c:choose>
						<c:when test="${curationList ne null and fn:length(curationList) > 0}">
						<c:forEach var="curation" items="${curationList}" varStatus="status">
							<tr>
								<td style="vertical-align: top;">${curation.order}</td>
								<td style="vertical-align: top;"><div align="left">${fn:replace(curation.curation_Nm,'<br>','/BR/')}</div></td>
								<td style="vertical-align: top;"><div align="left">${fn:replace(curation.line_Introduction,'<br>','/BR/')}</div></td>
								<td style="vertical-align: top;">
									<div align="left">
									<c:if test="${cpList != null}">
										<c:forEach var="cp" items="${cpList }" varStatus="status">
											<c:if test="${cp.curation_Seq eq curation.curation_Seq}">
												<b>${cp.curation_Process_Order } : ${cp.process_Code }</b> - ${cp.process_Nm } <b> (
													<c:choose>
														<c:when test="${cp.public_Yn eq 'Y'}">
															공개
														</c:when>
														<c:otherwise>
															비공개
														</c:otherwise>
													</c:choose>
													<c:if test="${cp.block_Yn eq 'Y' }">
														 / 차단
													</c:if>
												)</b> <br />
											</c:if>
										</c:forEach>
									</c:if>
									</div>
								</td>
								<td style="vertical-align: top;">
									<c:choose>
										<c:when test="${curation.use_Yn eq 'Y'}">
											사용
										</c:when>
										<c:otherwise>
											미사용
										</c:otherwise>
									</c:choose>
								</td>
								<td style="vertical-align: top;"><fmt:formatDate value="${curation.expression_Start_Day}" pattern="yyyy.MM.dd" /> ~ <fmt:formatDate value="${curation.expression_CLOSE_Day}" pattern="yyyy.MM.dd"/></td>
								<td style="vertical-align: top;">
									<button type="button" class="btn btn-default" onClick="javascript:self.location='registerCurationView.do?Curation_Seq=${curation.curation_Seq}&edit=Y';">수정</button>
								</td>
							</tr>
						</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="7">등록된 큐레이션이 없습니다.</td>
							</tr>
						</c:otherwise>
					</c:choose>
					</tbody>	
				</table>
				<c:if test="${curationList ne null and fn:length(curationList) > 0}">
					<div class="pagination-centered" align="center">
						<ul class="pagination">
							<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>>
								<c:choose>
									<c:when test="${currentPage-10 <= 0}">
										<a href="#">이전 10개</a>
									</c:when>
									<c:otherwise>
										<a href="../admin/curation.do?pn=${currentPage-10}">다음 10개</a>
									</c:otherwise>
								</c:choose>
							</li>
							<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>><a href="../admin/curation.do?pn=${currentPage-1}">&laquo;</a></li>
							<c:forEach var="pn" items="${pageNumbers}">
								<c:choose>
									<c:when test="${currentPage==pn}">
										<li class="active"><a
											href="../admin/curation.do?pn=${pn}">${pn}</a></li>
									</c:when>
									<c:otherwise>
										<li><a
											href="../admin/curation.do?pn=${pn}">${pn}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a
								href="../admin/curation.do?pn=${currentPage+1}">&raquo;</a></li>
							<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>>
								<c:choose>
									<c:when test="${currentPage+10 > numOfPages}">
										<a href="#">다음 10개</a>
									</c:when>
									<c:otherwise>
										<a href="../admin/curation.do?pn=${currentPage+10}">다음 10개</a>
									</c:otherwise>
								</c:choose>
							</li>
						</ul>
					</div> <!-- pagination-centered -->
				</c:if>
			</div>
		</div>
		<!-- /content -->
		
		<!-- start modal -->
		<div id="modal_template" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
					    <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4 id="title"></h4>
						</div>
					</div>
					<div class="content">
					
					</div>
				</div>
			</div>
		</div>
		<!-- end modal -->
		
	</div>
	<!-- /container -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />

<script type="text/javascript">
$(document).ready(function(){
	//순서변경 버튼 눌렀을 때
	$('#orderCurationBtn').on('click', function(event) {
// 		var value= $(this).val();
		$("#modal_template div.content").load("../admin/curation/modal/orderCuration.do", {

		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("순서변경");
		});
	});
	
	$('.pagination li').on('click', function(event) {
		if(0 <= $(this).prop('class').indexOf("disabled")) {
			event.preventDefault();
			return;
		}
	});
});

function orderChangeClick() {
	$('[name="orderNo"]').css('visibility', 'visible');
	var c_html = '<input class="btn btn-default" type="button" value="저장" onclick="orderSaveClick()">&nbsp;<input class="btn btn-default" type="button" value="취소" onclick="orderCancelClick()">';
	$("#switching").html(c_html);
};

function orderCancelClick() {
	$('[name="orderNo"]').css('visibility', 'hidden');
	var c_html = '<input class="btn btn-default" type="button" value="순서변경" onclick="orderChangeClick()">';
	$("#switching").html(c_html);
};

function orderSaveClick() {
	
};
</script>
</body>
</html>