<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>운영자관리</title>
	<style type="text/css">
	.ui-menu {
			list-style:none;
			padding: 2px;
			margin: 0;
			display:block;
			float: left;
		}
		.ui-menu .ui-menu {
			margin-top: -3px;
		}
		.ui-menu .ui-menu-item {
			margin:0;
			padding: 0;
			zoom: 1;
			float: left;
			clear: left;
			width: 100%;
		}
		.ui-menu .ui-menu-item a {
			text-decoration:none;
			display:block;
			padding:.2em .4em;
			line-height:1.5;
			zoom:1;
		}
		.ui-menu .ui-menu-item a.ui-state-hover,
		.ui-menu .ui-menu-item a.ui-state-active {
			font-weight: normal;
			margin: -1px;
		}
		</style>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li class="active"><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>운영자 관리</h1>
			</div>
			
			<div class="col col-12 col-md-12" style="padding-top: 5px;">
				<h5>운영자 추가</h5>
			</div>
			<div class="col col-10" style="padding-bottom: 10px; padding-top: 5px;">
				<select id="searchType">
					<option value="N">닉네임</option>
					<option value="I">ID</option>
				</select>
<!-- 					<input type="text" id="searchText" autocomplete="off" multiple="multiple" list="searchList"/> <input class="btn btn-default" type="button" value="추가" onclick="addUser()">&nbsp;<label style="font-size: 10px; font-style: color:#e8e8e8"> ※ 단어 입력후 Enter 키 입력시 사용자 검색</label> -->
				<input type="text" id="searchText" name="searchText" autocomplete="off"/> <input class="btn btn-default" type="button" value="추가" onclick="addUser()">
			</div>
			<div id="selectList">
			</div>
			<div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<td>No</td>
							<td>ID</td>
							<td>닉네임</td>
							<td>Q&amp;A 게시판</td>
							<td>비고</td>
						</tr>
					</thead>
					<tbody>
					<c:choose>
						<c:when test="${operatorList ne null and fn:length(operatorList) > 0}">					
							<c:forEach var="oList" items="${operatorList}" varStatus="status">
								<tr>
									<td>${status.count}</td>
									<td>${oList.user_Id}</td>
									<td>${oList.nickname}</td>
									<td>
										<c:if test="${oList.open_Yn eq 'Y' or oList.open_Yn eq 'y'}">
											공개
										</c:if>
										<c:if test="${oList.open_Yn eq 'N' or oList.open_Yn eq 'n'}">
											비공개
										</c:if>
									</td>
									<td>
										<input class="btn btn-default" type="button" value="설정" data-toggle="modal" data-target="#setting_${status.count}">
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="5">데이터가 존재하지 않습니다.</td>
							</tr>
						</c:otherwise>
					</c:choose>
					</tbody>
				</table>
			</div>
			<c:if test="${operatorList ne null and fn:length(operatorList) > 0}">
			<div class="pagination-centered" align="center">
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a href="../admin/operatorManagement.do?pn=${currentPage-10}">이전 10개</a></li>
					<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>>
					<c:choose>
						<c:when test="${currentPage-1 == 0}">
							<a href="#">&laquo;</a>
						</c:when>
						<c:otherwise>
							<a href="../admin/operatorManagement.do?pn=${currentPage-1}">&laquo;</a>
						</c:otherwise>
					</c:choose>
					</li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active"><a
									href="../admin/operatorManagement.do?pn=${pn}">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li><a
									href="../admin/operatorManagement.do?pn=${pn}">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>>
					<c:choose>
						<c:when test="${currentPage+1 > numOfPages}">
							<a href="#">&raquo;</a>
						</c:when>
						<c:otherwise>
							<a href="../admin/operatorManagement.do?pn=${currentPage+1}">&raquo;</a>
						</c:otherwise>
					</c:choose>
					</li>
					<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>>
					<c:choose>
						<c:when test="${currentPage+1 > numOfPages}">
							<a href="#">다음 10개</a>
						</c:when>
						<c:otherwise>
							<a href="../admin/operatorManagement.do?pn=${currentPage+10}">다음 10개</a>
						</c:otherwise>
					</c:choose>
					</li>
				</ul>
			</div>
			</c:if>
		</div>
		<!-- /content -->
		
		<c:forEach var="oListModal" items="${operatorList}" varStatus="status">
			<!-- start 비고 '설정' modal -->
			<div id="setting_${status.count}" class="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="header">
						    <button type="button" class="modal-close" data-dismiss="modal">X</button>
							<div class="modal-title header ">
								<h4 id="title">기능 설정</h4>
							</div>
						</div>

						<form action="../admin/updateOperatorOpenStatus.do" method="post">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>기능</th>
										<th>설정</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Q%A 게시판 제공여부</td>
										<td>
											<input type="hidden" name="Admin_Seq" value="${oListModal.admin_Seq}"/>
											<select name="Open_Yn">
												<c:choose>
													<c:when test="${oListModal.open_Yn eq 'Y'}">
														<option value="Y">공개</option>
														<option value="N">비공개</option>
													</c:when>
													<c:otherwise>
														<option value="N">비공개</option>
														<option value="Y">공개</option>
													</c:otherwise>
												</c:choose>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							<div align="center">
								<button type="submit" class="btn btn-default">저장</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- end 비고 '설정' modal -->
		</c:forEach>
	</div>
	<!-- /container -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />
</body>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#searchText").on("textchange", function() {
    	var availableTags = [];
		var keyword = $("#searchText").val();
		var type = $("#searchType").val();
		if ('' != keyword) {
			$.ajax({
		        url : "../admin/searchUserInfo.do",
		        type : "post",
		        data : {searchText:keyword, searchType:type},
		        dataType : "json",
		        error :  function(e){
					alert(e.responseText);
				},
		        success : function(data){
		        	if (0 == data['resultCode']) {
		        		var uList = data["userList"];
		        		
		        		if (null != uList) {
							var data_html = '<datalist id="searchList">';
							
			        		for (i=0; i < uList.length; i++) {
			        			var emp = "";
			        			emp = uList[i].user_Id+' / '+uList[i].nickname;
			        			availableTags.push(emp);
			        		}
		        		} else {
		        			availableTags.push('검색결과가 없습니다.');
		        		}
		        		
		        		$( "#searchText" ).autocomplete({
		        			source: availableTags
		            	});
		        	}
		        }
		    });
		}
	});
	
	$('.pagination li').on('click', function(event) {
		if(0 <= $(this).prop('class').indexOf("disabled")) {
			event.preventDefault();
			return;
		}
	});
    
});

function addUser() {
	var keyword = $("#searchText").val();
	var pCode = keyword.split("/",1);
	var pName = keyword.replace(pCode,'');
	
	if(!correctuser(pCode, pName)) {
		alert('다시 검색해주세요.');
		return;
	}
	
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "../admin/insertOperatorInAdminPage.do");

	form.appendChild(document.getElementById("searchText"));
	
	var iField = document.createElement("input");
	iField.setAttribute("type", "hidden");
	iField.setAttribute("name", "registIP");
	iField.setAttribute("value", ip());
	form.appendChild(iField);
	
	document.body.appendChild(form);
	form.submit();
};

//유저를 추가할 때, 실제 유저 id, 닉네임이 맞는지 확인
function correctuser(pCode, pName) {
	if(pCode == null || pName == null) {
		return false;
	} else {
		if(pCode[0].length < 2 || pName.length < 2) {
			return false;
		} else {
			return true;
		}
	}
};

</script>
</html>