<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/commons/include/commonTagLibrary.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<jsp:include page="../inc/head.jsp" flush="false" />
	<title>과정현황</title>
	<style type="text/css">
		td img {
			max-height:12px;
			width:auto;
		}
	</style>
</head>
<body>
	<jsp:include page="../inc/header.jsp" flush="false" />
	<jsp:useBean id="now" class="java.util.Date" />
	
	<!-- container -->
	<div id="container">
		<!-- sidebar -->
		<div id="sidebar">
			<h3>해피칼리지</h3>
			<!-- nav -->
			<div class="nav">
				<ul>
					<li><a href="../admin/memberStatus.do">회원현황</a></li>
					<li class="active"><a href="../admin/processStatus.do">과정현황</a></li>
					<li><a href="../admin/curation.do">큐레이션</a></li>
					<li><a href="../admin/notice.do">공지사항</a></li>
					<li><a href="../admin/customerService.do">고객센터</a></li>
					<li><a href="../admin/operatorManagement.do">운영자관리</a></li>
				</ul>
			</div>
			<!-- /nav -->
		</div>
		<!-- /sidebar -->
		
		<!-- content -->
		<div id="content">
			<div class="page-header">
				<h1>과정현황</h1>
			</div>
			
			<form action="../admin/processStatus.do" method="get" id="searchForm">
			<input type="hidden" name="search" id="search" value="${search }"/>
			<div class="col col-12 col-md-12" style="margin-bottom:20px;">
				<h5>총 오픈 과정 수: <b>${processDefaultCnt } 과정</b></h5>
					<table class="table table-bordered">
						<tr>
							<th>개설 기간</th>
							<td>
								<input type="text" class="datepicker" name="start_period" id="start_period" required/> ~ <input type="text" class="datepicker" name="end_period" id="end_period" required/>
								<button type="button" class="btn btn-default search_period" value="1">전체</button>
								<button type="button" class="btn btn-default search_period" value="2">오늘</button>
								<button type="button" class="btn btn-default search_period" value="3">1주일</button>
								<button type="button" class="btn btn-default search_period" value="4">1개월</button>
								<button type="button" class="btn btn-default search_period" value="5">3개월</button>
								<button type="button" class="btn btn-default search_period" value="6">6개월</button>
								<button type="button" class="btn btn-default search_period" value="7">1년</button>
							</td>
						</tr>
						<tr>
							<th>평가 등급</th>
							<td>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="5"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="4.5"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="4"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="3.5"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="3"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="2.5"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="2"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="1.5"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="1"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="0.5"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
								<label><input type="checkbox" name="Process_Total_Grade" class="Process_Total_Grade" value="0"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></label>
							</td>
						</tr>
						<tr>
							<th>카테고리</th>
							<td>
								<c:forEach var="code" items="${field_code}" varStatus="status">
								   	<label><input type="checkbox" name="Field_Code" class="Field_Code" value="${code.code}">${code.code_Nm}</label>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<th>공개설정</th>
							<td>
							<label><input type="checkbox" name="Open_Yn" class="Open_Yn" value="Y">공개</label>
							<label><input type="checkbox" name="Open_Yn" class="Open_Yn" value="N">비공개</label>
							</td>
						</tr>
						<tr>
							<th>차단설정</th>
							<td>
							<label><input type="checkbox" name="Block_Yn" class="Block_Yn" value="Y">차단됨</label>
							<label><input type="checkbox" name="Block_Yn" class="Block_Yn" value="N">차단안됨</label>
							</td>
						</tr>
						<tr>
							<th>판매가</th>
							<td>
								<label><input type="checkbox" name="Free_Yn" class="Free_Yn" value="Y"/>무료</label>
								<label><input type="checkbox" name="Free_Yn" class="Free_Yn" value="N"/>유료</label>
							</td>
						</tr>
					</table>
					
					<div>
						<button type="submit" class="btn btn-default col-3" id="searchBtn" style="float: left;">검색하기</button> 
						&nbsp;검색결과: ${ searchCnt }건
						
						<input type="hidden" name="excel" id="excel" value="N" />
						<div style="float: right;">
							<button type="button" class="btn btn-default" id="downloadBtn" style="margin-right:1px;">엑셀문서 다운로드</button>
						</div>
					</div>
<!-- 				</form> -->
			</div>
			 <!--end 검색조건 div -->
			 
			
			<div style="float: left; margin-bottom:6px;"> 
<!-- 				<form action="../admin/processStatus.do" method=POST id="textSearchForm"> -->
				<select name="searchCategory">
					<option value="Process_Nm" <c:if test="${chk_searchCategory eq 'Process_Nm'}">selected="selected"</c:if>>과정명</option>
					<option value="Process_Code" <c:if test="${chk_searchCategory eq 'Process_Code'}">selected="selected"</c:if>>과정코드</option>
					<option value="Tag" <c:if test="${chk_searchCategory eq 'Tag'}">selected="selected"</c:if>>태그명</option>
				</select>
				<input type="text" style="width:200px;" name="content" id="textContent" <c:if test="${searchText ne null and searchText ne ''}">value="${searchText}"</c:if>> <input class="btn btn-default" type="submit" id="textSearchFormBtn" value="검색">
			</div>
			<div  style="float: right; margin-bottom:6px;">
				정렬기준 &nbsp;
				<select name="ordering" id="ordering">
					<option value="Regist_Date" <c:if test="${chk_Order eq 'Regist_Date'}">selected="selected"</c:if>>최근 개설일 순</option>
					<option value="Process_Total_Grade" <c:if test="${chk_Order eq 'Process_Total_Grade'}">selected="selected"</c:if>>높은 별점 순</option>
					<option value="TakeLecture_Review_Count" <c:if test="${chk_Order eq 'TakeLecture_Review_Count'}">selected="selected"</c:if>>많은 수강후기 순</option>
					<option value="TakeLecture_Studying_Count" <c:if test="${chk_Order eq 'TakeLecture_Studying_Count'}">selected="selected"</c:if>>높은 학습중 수강자수 순</option>
					<option value="TakeLecture_OnGoing_Count" <c:if test="${chk_Order eq 'TakeLecture_OnGoing_Count'}">selected="selected"</c:if>>높은 전체수강자수 순</option>
					<option value="Price_high" <c:if test="${chk_Order eq 'Price_high'}">selected="selected"</c:if>>높은 판매금액 순</option>
					<option value="Price_low" <c:if test="${chk_Order eq 'Price_low'}">selected="selected"</c:if>>낮은 판매금액 순</option>
				</select>
			</div>
			
			</form>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th rowspan="2">개설일</th>
						<th rowspan="2">카테고리</th>
						<th rowspan="2">과정코드</th>
						<th rowspan="2">과정명</th>
						<th rowspan="2">ID/닉네임</th>
						<th rowspan="2">가격</th>
						<th rowspan="2">태그명</th>
						<th rowspan="2">평가등급</th>
						<th rowspan="2">수강 후기수</th>
						<th rowspan="2">Q&amp;A</th>
						<th rowspan="2">전체 수강자수</th>
						<th colspan="4">공유 수</th>
						<th rowspan="2">공개설정</th>
						<th rowspan="2">차단 사유</th>
					</tr>
					<tr>
						<th>페북</th>
						<th>카톡</th>
						<th>문자</th>
						<th>메일</th>
					</tr>
				</thead>
				<!-- 내용  -->
				<tbody>
					<c:choose>
						<c:when test="${ processList ne null and fn:length(processList) > 0}">
							<c:forEach var="process" items="${ processList }">
								<tr>
									<td><fmt:formatDate var="dateFormat" value="${process.regist_Date}" pattern="yyyy-MM-dd"/>${dateFormat }​</td>
									<td>
										<c:forEach var="code" items="${ field_code }">
											<c:if test="${ process.field_Code eq code.code}">
												${code.code_Nm }
											</c:if>
										</c:forEach>
									</td>
									<td>${ process.process_Code }</td>
									<td>${ process.process_Nm }</td>
									<td>${ process.regist_Id } / ${process.nickname}</td>
									<td>
									<c:choose>
										<c:when test="${process.free_Yn eq 'Y' }">
											무료
										</c:when>
										<c:when test="${process.free_Yn eq 'N' }">
											유료
										</c:when>
									</c:choose>
									</td>
									<td>
										<c:forEach var="entry" items="${resultMap }" varStatus="status">
											<c:if test="${entry.key eq process.process_Code }">
												<c:forEach var="tag" items="${ entry.value}" varStatus="status">
													#${tag.tag } <br />
												</c:forEach>
											</c:if>
										</c:forEach>
										<c:if test="${process.tag_Count >= 5 }">
											<button type="button" class="btn btn-default tagDetailbtn" data-toggle="modal" data-target="#modal_template" data-backdrop="static" value="${process.process_Code }">전체보기</button>
										</c:if>
									</td>
									<td>
										<c:choose>
											<c:when test="${0.0 <= process.grade and process.grade < 0.5}"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${0.5 <= process.grade and process.grade < 1.0}"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${1.0 <= process.grade and process.grade < 1.5}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${1.5 <= process.grade and process.grade < 2.0}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${2.0 <= process.grade and process.grade < 2.5}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${2.5 <= process.grade and process.grade < 3.0}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${3.0 <= process.grade and process.grade < 3.5}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${3.5 <= process.grade and process.grade < 4.0}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${4.0 <= process.grade and process.grade < 4.5}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star2_m.png"></c:when>
											<c:when test="${4.5 <= process.grade and process.grade < 5.0}"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star3_m.png"></c:when>
											<c:when test="${process.grade eq 5.0 }"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"><img src="../assets/img/star1_m.png"></c:when>
										</c:choose>
										<br/>(${process.process_Evalationer_Count })
									</td>
									<td>
										${process.takeLecture_Review_Count }
										<br />
										<input type="hidden" id = "review_Count_${process.process_Code }" value="${process.takeLecture_Review_Count }"/>
										<button class="btn btn-default reviewDetailBtn" type="button" data-toggle="modal" data-target="#modal_template" value="${process.process_Code}">보기</button>
										
									</td>
									<td>
										${ process.process_QnA_Count }
									</td>
									<td>
									<div align="left">
										전체: ${process.takeLecture_OnGoing_Count }	<br /> <input type="hidden" id="onGoing_count_${process.process_Code }" value="${process.takeLecture_OnGoing_Count }"/>
										진행중: ${process.takeLecture_Studying_Count }<br /> <input type="hidden" id="studying_count_${process.process_Code }" value="${process.takeLecture_Studying_Count }"/>
										기간종료: ${process.takeLecture_Expire_Count }	<br /> <input type="hidden" id="expire_count_${process.process_Code }" value="${process.takeLecture_Expire_Count }"/>
									</div>
									<button class="btn btn-default takeLecturePeopleBtn" type="button" data-toggle="modal" data-target="#modal_template" value="${process.process_Code }">보기</button>
									</td>
									<td>${ process.facebook_Share_Count }</td>
									<td>${ process.kakaotalk_Share_Count }</td>
									<td>${ process.letters_Share_Count }</td>
									<td>${ process.email_Share_Count }</td>
									<td>
										<select name="Open_Yn_set_${process.process_Code }" class="Open_Yn_Setting">
											<option value="Y" <c:if test="${process.open_Yn eq 'Y'}">selected="selected"</c:if>>공개</option>
											<option value="N" <c:if test="${process.open_Yn eq 'N'}">selected="selected"</c:if>>비공개</option>
											<option value="B" <c:if test="${process.block_Yn eq 'Y'}">selected="selected"</c:if>>차단</option>
										</select>
										<input type="hidden" id="Email_${process.process_Code }" value = "${process.email }">
										<input type="hidden" id="Open_Yn_${process.process_Code }" value = "${process.open_Yn }">
										<input type="hidden" id="Block_Yn_${process.process_Code }" value = "${process.block_Yn }">
										<br/>
										<button class="btn btn-default openFormBtn" data-toggle="modal" type="button" value="${process.process_Code }" >변경</button>
									</td>
									<td>
										<c:forEach var="entry" items="${blockMap }" varStatus="status">
											<c:if test="${entry.key eq process.process_Code }">
												<c:forEach var="block" items="${ entry.value}" varStatus="status">
													${cfn:dateFmt2(block.regist_Date) } <br />
													<button class="btn btn-default blockReasonBtn" data-toggle="modal" data-target="#modal_template" type="button" value="${block.process_Block_Email_Seq }" >보기</button>
													<br />
												</c:forEach>
											</c:if>
										</c:forEach>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="17">데이터가 존재하지 않습니다.</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>	
			</table>
			<div class="pagination-centered" align="center">
				<ul class="pagination">
					<li <c:if test="${currentPage-10 <= 0}">class="disabled"</c:if>><a href="../admin/processStatus.do" data-pn="${ currentPage-10}">이전 10개</a></li>
					<li <c:if test="${currentPage-1 == 0}">class="disabled"</c:if>><a href="../admin/processStatus.do" data-pn="${ currentPage-1}">&laquo;</a></li>
					<c:forEach var="pn" items="${pageNumbers}">
						<c:choose>
							<c:when test="${currentPage==pn}">
								<li class="active"><a href="../admin/processStatus.do" data-pn="${pn }">${pn}</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="../admin/processStatus.do" data-pn="${pn }">${pn}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<li <c:if test="${currentPage+1 > numOfPages}">class="disabled"</c:if>><a href="../admin/processStatus.do" data-pn="${currentPage+1 }">&raquo;</a></li>
					<li <c:if test="${currentPage+10 > numOfPages}">class="disabled"</c:if>>
						<c:choose>
							<c:when test="${currentPage+10 > numOfPages}">
								<a href="">다음 10개</a>
							</c:when>
							<c:otherwise>
								<a href="../admin/processStatus.do" value="${currentPage+10}">다음 10개</a>
							</c:otherwise>
						</c:choose>
					</li>
				</ul>
			</div><!-- end pagination -->
			
		</div>
		<!-- /content -->
		
		
		<!-- start modal -->
		<div id="modal_template" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="header">
					    <button type="button" class="modal-close" data-dismiss="modal">X</button>
						<div class="modal-title header ">
							<h4 id="title"></h4>
						</div>
					</div>
					<div class="content">
					
					</div>
				</div>
			</div>
		</div>
		<!-- end modal -->
		
		<!-- start confirm modal -->
		<div id="modal_confirm" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="content">
						<p class="text-center">해당 과정의<br/>공개 설정값을 변경하시겠습니까?</p>
					</div>
					<div class="footer">
<!-- 						<form action="../admin/updateProcessOpenStatus.do" method="POST" id="confirmForm"> -->
							<input type="hidden" name="Process_Code" >
							<input type="hidden" name="Open_Yn" >
							<button type="button" class="btn btn-primary confirm_change">변경</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
<!-- 						</form> -->
					</div>
				</div>
			</div>
		</div>
		<!-- end confirm modal -->
	
		<!-- start confirm result modal -->
		<div id="modal_result" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="content">
						<p class="text-center">설정값이<br/>서버에 반영되었습니다.</p>
					</div>
					<div class="footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">확인</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end confirm result modal -->
		
	</div>
	<!-- /container -->
	
	<jsp:include page="../inc/footer.jsp" flush="false" />
	<jsp:include page="../inc/foot.jsp" flush="false" />

<script type="text/javascript">
$(function(){
	$('.datepicker').datepicker(); //  datepicker
	var search = $('#search').val();
	var block = $('#Block_Yn').val();
	
	var now = new Date();
	if(search == 'Y') {
		$('input:checkbox').prop("checked", false);
		$('#start_period').val('${chk_start}');
		$('#end_period').val('${chk_end}');
	} else {
		$('input:checkbox').prop("checked", true);
		$('#start_period').datepicker("setDate", '2016-01-01');
		$('#end_period').datepicker("setDate", now.getFullYear() +"-"+(now.getMonth() +1) +"-"+now.getDate() +" "+now.getHours() + ":" +now.getMinutes() + ":"+now.getSeconds() +"." +now.getMilliseconds() );
	}
	

	<c:forEach var="item" items="${chk_Grade}">
		if('${item}' != null && '${item}' != '') {
			$("input:checkbox[value=\""+'${item}'+"\"].Process_Total_Grade").prop("checked", true);
		}
	</c:forEach>
	
	<c:forEach var="item" items="${chk_Field}">
		if('${item}' != null && '${item}' != '') {
			$("input:checkbox[value="+'${item}'+"].Field_Code").prop("checked", true);
		}
	</c:forEach>
	
	<c:forEach var="item" items="${chk_Open}">
		if('${item}' != null && '${item}' != '') {
			$("input:checkbox[value="+'${item}'+"].Open_Yn").prop("checked", true);
		}
	</c:forEach>
	
	<c:forEach var="item" items="${chk_Free}">
		if('${item}' != null && '${item}' != '') {
			$("input:checkbox[value="+'${item}'+"].Free_Yn").prop("checked", true);
		}
	</c:forEach>
	
	<c:forEach var="item" items="${chk_Block}">
	if('${item}' != null && '${item}' != '') {
		$("input:checkbox[value="+'${item}'+"].Block_Yn").prop("checked", true);
	}
	</c:forEach>
	
	
	if("${chk_Order}" != null && "${chk_Order}" != '') {
		$('select[name=ordering] option[value='+"${chk_Order}"+']').prop("selected", true);
	}
	
	//태그 전체보기 모달
	$('.tagDetailbtn').on('click', function() {
		
		$("#modal_template div.content").load("../admin/process/modal/TagDetail.do", {
			Process_Code :  $(this).val()
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			}
			$("#title").html("태그 전체보기");
		});
	});
	
	//수강후기 모달
	$('.reviewDetailBtn').on('click', function() {
		var value= $(this).val();
		$("#modal_template div.content").load("../admin/process/modal/reviewDetail.do", {
			Process_Code : value,
			TakeLecture_Review_Count : $('#review_Count_'+value).val()
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("수강 후기");
		});
	});
	
	//전체수강자수 모달
	$('.takeLecturePeopleBtn').on('click', function() {
		var value= $(this).val();
		$("#modal_template div.content").load("../admin/process/modal/takeLecturePeople.do", {
			Process_Code :  $(this).val()
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			}
			$("#title").html("전체 수강자 <small>전체: "+ $('#onGoing_count_' + value).val()+" / 진행 중: "+$('#studying_count_'+value).val()+" / 기간 종료 : "+$('#expire_count_'+value).val()+"</small>");
		});
	});
	
	//차단사유 모달
	$('.blockReasonBtn').on('click', function() {
		var value= $(this).val();
		$("#modal_template div.content").load("../admin/process/modal/blockReason.do", {
			Process_Block_Email_Seq: value
			
		}, function(response, status, xhr) {
			if(status == "error") {
				console.log("error: " + xhr.status + " " + xhr.statusText);
			} 
			$("#title").html("차단사유");
		});
	});
	
	//공개설정 변경
	$('.openFormBtn').on('click', function(event) {
		var processCode = $(this).val();
		var email = $('#Email_'+processCode).val();
		var open = $('#Open_Yn_'+processCode).val();
		var block =  $('#Block_Yn_' + processCode).val();
		
		var selectValue = $('select[name=Open_Yn_set_'+processCode+'] option:selected').val();
		
		if(selectValue == 'B') {
			$("#modal_template").modal({show:true});
			$("#modal_template div.content").load("../admin/process/modal/sendEmailBlocking.do", {
				Process_Code : processCode,
				Email : email,
			}, function(response, status, xhr) {
				if(status == "error") {
					console.log("error: " + xhr.status + " " + xhr.statusText);
				} 
				$("#title").html("차단사유 이메일 전송");
			});
			
		} else {
			$("#modal_confirm").modal({show:true});
			
			$('.confirm_change').on('click', function() {
				$.ajax({
					url: '../admin/updateProcessOpenStatus.do',
					type: 'POST',
					dataType:'json',
					data: {
						Process_Code: processCode,
						Public_Yn: selectValue
					},
					success: function(data, status, xhr) {
						var result = data["updateOpenResult"];
						
						if(result > 0) {
							$("#modal_result").modal({show:true});
						} else {
							$("#modal_result .content .text-center").html("실패");
							$("#modal_result").modal({show:true});
						}
						
					},
					error: function(xhr, status, message) {
						$("#modal_result .content .text-center").html("실패");
						$("#modal_result").modal({show:true});
					}
				});
			});
		}
	});
	
	
	$('#textSearchFormBtn').on('click',function() {
// 		if($('input[name=Block_Yn_hidden]').prop("checked") == true) {
// 			$('#Block_Yn').val('Y');
// 		} else {
// 			$('#Block_Yn').val('N');
// 		}
		$('#excel').val('N');
		$('#search').val('Y');
		$('#searchForm').submit();
	});
	
	
	
	$('#ordering').change(function() {
// 		$('#search').val('Y');
		$('#excel').val('N');
// 		if($('input[name=Block_Yn_hidden]').prop("checked") == true) {
// 			$('#Block_Yn').val('Y');
// 		} else {
// 			$('#Block_Yn').val('N');
// 		}
		
		if(search == "Y") {
			$('#search').val('Y');
			$('#searchForm').submit();
		} else {
			$('#search').val('N');
			$('#searchForm').submit();
		}
	});
	
	//엑셀 다운로드 버튼 눌렀을 때 
	$('#downloadBtn').on('click', function(evnet) {
// 		if($('input[name=Block_Yn_hidden]').prop("checked") == true) {
// 			$('#Block_Yn').val('Y');
// 		} else {
// 			$('#Block_Yn').val('N');
// 		}
		
		$('#excel').val('Y');
		$('form').submit();			
	});
	
	$('#searchBtn').on('click', function(evnet) {
		if (false == validationCheck()) {
			event.preventDefault();
			return;
		}
		
		$('#excel').val('N');
		$('#search').val('Y');
// 		if($('input[name=Block_Yn_hidden]').prop("checked") == true) {
// 			$('#Block_Yn').val('Y');
// 		} else {
// 			$('#Block_Yn').val('N');
// 		}
		
		//검색하기 버튼 눌렀을 때
		$('#searchForm').submit();	//search button submit
	});
	
	
	$('.datepicker').on('click', function() {
		$('.search_period').removeClass('active');
	});
	
	$('.search_period').on('click',function(event){
		$('.search_period').removeClass('active');
		$(this).addClass('active');
		var code = $(this).val();
// 		var dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		var d = new Date();

		var lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
		if(code == '1') {	//전체
			$( "#start_period" ).datepicker("setDate", new Date("2016/01/01"));
		} else if(code == '2') {	//오늘
			$( "#start_period" ).datepicker("setDate", new Date() );
		} else if(code == '3') {	//1주일
			var date = d.getDate() - 7;
			d.setDate(date);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '4') {	//1개월
			lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -1;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '5') {	//3개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 3), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -3;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '6') {	//6개월
			lastDayofLastMonth = ( new Date(d.getYear(), ((d.getMonth()+1)- 6), 0) ).getDate();
			if(d.getDate() > lastDayofLastMonth) {
				d.setDate(lastDayofLastMonth);
			}
			var month = d.getMonth() -6;
			d.setMonth(month);
			$( "#start_period" ).datepicker("setDate", d );
		} else if(code == '7') {	//1년
			var year = d.getFullYear() -1;
			d.setFullYear(year);
			$( "#start_period" ).datepicker("setDate", d );
		}
		
		d = new Date();
		$('#end_period').datepicker("setDate", now.getFullYear() +"-"+(now.getMonth() +1) +"-"+now.getDate() +" "+now.getHours() + ":" +now.getMinutes() + ":"+now.getSeconds() +"." +now.getMilliseconds() );
    });
	var qStr = window.location.search;
	
	$('.pagination a').on('click', function(event) {
		if(0 <= $(this).parent().prop('class').indexOf("disabled")) {
			event.preventDefault();
			return;
		}
		var pn = $(this).data('pn');
		
		if(search == "Y") {
			$('#search').val('Y');
			
			if(0 <= qStr.indexOf("pn")) {
		 		qStr = qStr.substr(0,qStr.indexOf("&pn="));
			}
			
			$(this).attr("href", $(this).attr("href") +qStr + "&pn="+pn);
			$(this).unbind("click");
		} else {
			$('#ordering').val($('#ordering').val());
			$(this).attr("href", $(this).attr("href") + "?pn="+pn+"&ordering="+$('#ordering').val());
			$(this).unbind("click");
		}
	});
	
});

function validationCheck() {
	var validformat = /^\d{4}\-\d{2}\-\d{2}$/; 
	if (null == $('#start_period').val() || "" == $('#start_period').val() || !validformat.test($('#start_period').val())) {
		alert("접수일을 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if (null == $('#end_period').val() || "" == $('#end_period').val() || !validformat.test($('#end_period').val())) {
		alert("접수일 선택해 주세요.");
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('#end_period').val() < $('#start_period').val()) {
		alert("접수일이 잘못 선택되었습니다.\n다시 선택해 주세요.");
		$("#start_period").fadeOut().fadeIn().fadeOut().fadeIn();
		$("#end_period").fadeOut().fadeIn().fadeOut().fadeIn();
		return false;
	} else if ($('input[name=Field_Code]:checked').length == 0) {
		alert("카테고리를 선택해주세요.");
		return false;
	} else if ($('input[name=Free_Yn]:checked').length == 0) {
		alert("판매가를 선택해주세요.");
		return false;
	} else if ($('input[name=Process_Total_Grade]:checked').length == 0) {
		alert("평가등급을 선택해주세요.");
		return false;
	} else {
		true;
	}
};
</script>
</body>
</html>